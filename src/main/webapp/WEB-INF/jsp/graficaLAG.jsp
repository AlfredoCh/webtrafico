<!doctype html>
<html lang="en">
<head>
<title>Web de Tr�fico - Colas</title>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
	integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
	crossorigin="anonymous">
<link rel="stylesheet" href="resources/css/general.css">
</head>
<body ng-app="traficoApp">
	<div ng-controller="graficaCtrl">
		<div class="background-image"></div>
		<div class="content">
			<div class="card">
				<div class="card-header">
					Azteca Comunicaciones Per� <a ng-click="regresar()"
					class="float-right">Regresar</a>
				</div>
				<div class="card-body">
					<div class="form-row">
						<div class="form-group col-md-4" align="center">
							<label> <b>Fecha Inicio</b></label> <input ng-model="dtini"
								class="form-control" type="date" id="dtini" required> <input
								ng-model="tmini" class="form-control" type="time" id="tmini">
						</div>
						<div class="form-group col-md-4" align="center">
							<label> <b>Fecha Fin</b> </label> <input ng-model="dtfin"
								class="form-control" type="date" id="dtfin" required> <input
								ng-model="tmfin" class="form-control" type="time" id="tmfin">
						</div>
						<div class="form-group col-md-4" align="center">
							<br>
							<div class="row">
								<div class="col-md-4">
								</div>
								<div class="col-md-4">
									<div class="row">
										<label>Ver Drops</label>
										<label class="switch">
										  <input type="checkbox" ng-click="verDrops()">
										  <span class="slider round"></span>
										</label>
									</div>
								</div>
								<div class="col-md-4">
								</div>
							</div>
							<div>
								<button type="button" class="btn btn-primary btn-lg btn-block"
								ng-click="estadisticas()"> Buscar </button>
							</div>
						</div>
					</div>
					<div ng-hide="loader" align="center" style="margin-top: 5em">
						<div class="loader"></div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<div style="height: 30em; width:55em" id="grafica1" ng-hide="graf1"></div>
							<div style="height: 30em; width:55em" id="graficadrops1" ng-hide="grafdrop1"></div>
						</div>
						<div class="col-md-4">
							<table class="table table-sm" ng-hide="tabla1" style="font-size: 12px;">
								<thead>
									<tr>
										<th scope="col">Cola</th>
										<th scope="col">Min</th>
										<th scope="col">Avg</th>
										<th scope="col">Max</th>
										<th scope="col">StdDev</th>
									</tr>
								</thead>
								<tbody>
									<tr bgcolor="#757EFF">
										<th scope="row">DEFAULT</th>
										<th>{{out_col1_min}}</th>
										<th>{{out_col1_avg}}</th>
										<th>{{out_col1_max}}</th>
										<th>{{out_col1_stddev}}</th>
									</tr>
									<tr bgcolor="#FFB138">
										<th scope="row">VIDEO</th>
										<th>{{out_col2_min}}</th>
										<th>{{out_col2_avg}}</th>
										<th>{{out_col2_max}}</th>
										<th>{{out_col2_stddev}}</th>
									</tr>
									<tr bgcolor="#5DC456">
										<th scope="row">GESTI�N</th>
										<th>{{out_col3_min}}</th>
										<th>{{out_col3_avg}}</th>
										<th>{{out_col3_max}}</th>
										<th>{{out_col3_stddev}}</th>
									</tr>
									<tr bgcolor="#FF4C3B">
										<th scope="row">CLIENTES</th>
										<th>{{out_col4_min}}</th>
										<th>{{out_col4_avg}}</th>
										<th>{{out_col4_max}}</th>
										<th>{{out_col4_stddev}}</th>
									</tr>
									<tr bgcolor="#BC5DFF">
										<th scope="row">CLIENTES</th>
										<th>{{out_col5_min}}</th>
										<th>{{out_col5_avg}}</th>
										<th>{{out_col5_max}}</th>
										<th>{{out_col5_stddev}}</th>
									</tr>
									<tr bgcolor="#B57B1D">
										<th scope="row">CLIENTES</th>
										<th>{{out_col6_min}}</th>
										<th>{{out_col6_avg}}</th>
										<th>{{out_col6_max}}</th>
										<th>{{out_col6_stddev}}</th>
									</tr>
									<tr bgcolor="#FF7DD8">
										<th scope="row">VOIP</th>
										<th>{{out_col7_min}}</th>
										<th>{{out_col7_avg}}</th>
										<th>{{out_col7_max}}</th>
										<th>{{out_col7_stddev}}</th>
									</tr>
									<tr bgcolor="#C2C4B8">
										<th scope="row">ROUTING CONTROL</th>
										<th>{{out_col8_min}}</th>
										<th>{{out_col8_avg}}</th>
										<th>{{out_col8_max}}</th>
										<th>{{out_col8_stddev}}</th>
									</tr>
								</tbody>
							</table>
							<table class="table table-sm" ng-hide="tabladrop1" style="font-size: 12px;">
								<thead>
									<tr>
										<th scope="col">Cola</th>
										<th scope="col">Min</th>
										<th scope="col">Avg</th>
										<th scope="col">Max</th>
										<th scope="col">StdDev</th>
									</tr>
								</thead>
								<tbody>
									<tr bgcolor="#757EFF">
										<th scope="row">DEFAULT</th>
										<th>{{out_drop1_min}}</th>
										<th>{{out_drop1_avg}}</th>
										<th>{{out_drop1_max}}</th>
										<th>{{out_drop1_stddev}}</th>
									</tr>
									<tr bgcolor="#FFB138">
										<th scope="row">VIDEO</th>
										<th>{{out_drop2_min}}</th>
										<th>{{out_drop2_avg}}</th>
										<th>{{out_drop2_max}}</th>
										<th>{{out_drop2_stddev}}</th>
									</tr>
									<tr bgcolor="#5DC456">
										<th scope="row">GESTI�N</th>
										<th>{{out_drop3_min}}</th>
										<th>{{out_drop3_avg}}</th>
										<th>{{out_drop3_max}}</th>
										<th>{{out_drop3_stddev}}</th>
									</tr>
									<tr bgcolor="#FF4C3B">
										<th scope="row">CLIENTES</th>
										<th>{{out_drop4_min}}</th>
										<th>{{out_drop4_avg}}</th>
										<th>{{out_drop4_max}}</th>
										<th>{{out_drop4_stddev}}</th>
									</tr>
									<tr bgcolor="#BC5DFF">
										<th scope="row">CLIENTES</th>
										<th>{{out_drop5_min}}</th>
										<th>{{out_drop5_avg}}</th>
										<th>{{out_drop5_max}}</th>
										<th>{{out_drop5_stddev}}</th>
									</tr>
									<tr bgcolor="#B57B1D">
										<th scope="row">CLIENTES</th>
										<th>{{out_drop6_min}}</th>
										<th>{{out_drop6_avg}}</th>
										<th>{{out_drop6_max}}</th>
										<th>{{out_drop6_stddev}}</th>
									</tr>
									<tr bgcolor="#FF7DD8">
										<th scope="row">VOIP</th>
										<th>{{out_drop7_min}}</th>
										<th>{{out_drop7_avg}}</th>
										<th>{{out_drop7_max}}</th>
										<th>{{out_drop7_stddev}}</th>
									</tr>
									<tr bgcolor="#C2C4B8">
										<th scope="row">ROUTING CONTROL</th>
										<th>{{out_drop8_min}}</th>
										<th>{{out_drop8_avg}}</th>
										<th>{{out_drop8_max}}</th>
										<th>{{out_drop8_stddev}}</th>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<hr style="width:85%;">
					<div class="row">
						<div class="col-md-8">
							<div style="height: 30em; width:55em" id="grafica2" ng-hide="graf2"></div>
							<div style="height: 30em; width:55em" id="graficadrops2" ng-hide="grafdrop2"></div>
						</div>
						<div class="col-md-4" >
							<table class="table table-sm" ng-hide="tabla2" style="font-size: 12px;">
								<thead>
									<tr>
										<th scope="col">Cola</th>
										<th scope="col">Min</th>
										<th scope="col">Avg</th>
										<th scope="col">Max</th>
										<th scope="col">StdDev</th>
									</tr>
								</thead>
								<tbody>
									<tr bgcolor="#757EFF">
										<th scope="row">DEFAULT</th>
										<th>{{in_col1_min}}</th>
										<th>{{in_col1_avg}}</th>
										<th>{{in_col1_max}}</th>
										<th>{{in_col1_stddev}}</th>
									</tr>
									<tr bgcolor="#FFB138">
										<th scope="row">VIDEO</th>
										<th>{{in_col2_min}}</th>
										<th>{{in_col2_avg}}</th>
										<th>{{in_col2_max}}</th>
										<th>{{in_col2_stddev}}</th>
									</tr>
									<tr bgcolor="#5DC456">
										<th scope="row">GESTI�N</th>
										<th>{{in_col3_min}}</th>
										<th>{{in_col3_avg}}</th>
										<th>{{in_col3_max}}</th>
										<th>{{in_col3_stddev}}</th>
									</tr>
									<tr bgcolor="#FF4C3B">
										<th scope="row">CLIENTES</th>
										<th>{{in_col4_min}}</th>
										<th>{{in_col4_avg}}</th>
										<th>{{in_col4_max}}</th>
										<th>{{in_col4_stddev}}</th>
									</tr>
									<tr bgcolor="#BC5DFF">
										<th scope="row">CLIENTES</th>
										<th>{{in_col5_min}}</th>
										<th>{{in_col5_avg}}</th>
										<th>{{in_col5_max}}</th>
										<th>{{in_col5_stddev}}</th>
									</tr>
									<tr bgcolor="#B57B1D">
										<th scope="row">CLIENTES</th>
										<th>{{in_col6_min}}</th>
										<th>{{in_col6_avg}}</th>
										<th>{{in_col6_max}}</th>
										<th>{{in_col6_stddev}}</th>
									</tr>
									<tr bgcolor="#FF7DD8">
										<th scope="row">VOIP</th>
										<th>{{in_col7_min}}</th>
										<th>{{in_col7_avg}}</th>
										<th>{{in_col7_max}}</th>
										<th>{{in_col7_stddev}}</th>
									</tr>
									<tr bgcolor="#C2C4B8">
										<th scope="row">ROUTING CONTROL</th>
										<th>{{in_col8_min}}</th>
										<th>{{in_col8_avg}}</th>
										<th>{{in_col8_max}}</th>
										<th>{{in_col8_stddev}}</th>
									</tr>
								</tbody>
							</table>
							<table class="table table-sm" ng-hide="tabladrop2" style="font-size: 12px;">
								<thead>
									<tr>
										<th scope="col">Cola</th>
										<th scope="col">Min</th>
										<th scope="col">Avg</th>
										<th scope="col">Max</th>
										<th scope="col">StdDev</th>
									</tr>
								</thead>
								<tbody>
									<tr bgcolor="#757EFF">
										<th scope="row">DEFAULT</th>
										<th>{{in_drop1_min}}</th>
										<th>{{in_drop1_avg}}</th>
										<th>{{in_drop1_max}}</th>
										<th>{{in_drop1_stddev}}</th>
									</tr>
									<tr bgcolor="#FFB138">
										<th scope="row">VIDEO</th>
										<th>{{in_drop2_min}}</th>
										<th>{{in_drop2_avg}}</th>
										<th>{{in_drop2_max}}</th>
										<th>{{in_drop2_stddev}}</th>
									</tr>
									<tr bgcolor="#5DC456">
										<th scope="row">GESTI�N</th>
										<th>{{in_drop3_min}}</th>
										<th>{{in_drop3_avg}}</th>
										<th>{{in_drop3_max}}</th>
										<th>{{in_drop3_stddev}}</th>
									</tr>
									<tr bgcolor="#FF4C3B">
										<th scope="row">CLIENTES</th>
										<th>{{in_drop4_min}}</th>
										<th>{{in_drop4_avg}}</th>
										<th>{{in_drop4_max}}</th>
										<th>{{in_drop4_stddev}}</th>
									</tr>
									<tr bgcolor="#BC5DFF">
										<th scope="row">CLIENTES</th>
										<th>{{in_drop5_min}}</th>
										<th>{{in_drop5_avg}}</th>
										<th>{{in_drop5_max}}</th>
										<th>{{in_drop5_stddev}}</th>
									</tr>
									<tr bgcolor="#B57B1D">
										<th scope="row">CLIENTES</th>
										<th>{{in_drop6_min}}</th>
										<th>{{in_drop6_avg}}</th>
										<th>{{in_drop6_max}}</th>
										<th>{{in_drop6_stddev}}</th>
									</tr>
									<tr bgcolor="#FF7DD8">
										<th scope="row">VOIP</th>
										<th>{{in_drop7_min}}</th>
										<th>{{in_drop7_avg}}</th>
										<th>{{in_drop7_max}}</th>
										<th>{{in_drop7_stddev}}</th>
									</tr>
									<tr bgcolor="#C2C4B8">
										<th scope="row">ROUTING CONTROL</th>
										<th>{{in_drop8_min}}</th>
										<th>{{in_drop8_avg}}</th>
										<th>{{in_drop8_max}}</th>
										<th>{{in_drop8_stddev}}</th>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script
		src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular.min.js">
		
	</script>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
		integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
		integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
		crossorigin="anonymous"></script>
	<script src="resources/js/graficaLAG_controller.js"></script>
	<script src="resources/js/plotly-latest.min.js"></script>
</body>
</html>