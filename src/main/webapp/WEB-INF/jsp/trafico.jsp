<!doctype html>
<html lang="en">
<head>
<title>Web de Tr�fico - Trafico</title>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
	integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
	crossorigin="anonymous">
<link rel="stylesheet" href="resources/css/general.css">
</head>
<body ng-app="traficoApp">
	<div ng-controller="graficaCtrl">
		<div class="background-image"></div>
		<div class="content">
			<div class="card">
				<div class="card-header">
					Azteca Comunicaciones Per� <a ng-click="regresar()"
						class="float-right">Regresar</a>
				</div>
				<div class="card-body">
					<div class="form-row">
						<div class="form-group col-md-4" align="center">
							<label> <b>Fecha Inicio</b></label> <input ng-model="dtini"
								class="form-control" type="date" id="dtini" required> <input
								ng-model="tmini" class="form-control" type="time" id="tmini">
						</div>
						<div class="form-group col-md-4" align="center">
							<label> <b>Fecha Fin</b>
							</label> <input ng-model="dtfin" class="form-control" type="date"
								id="dtfin" required> <input ng-model="tmfin"
								class="form-control" type="time" id="tmfin">
						</div>
						<div class="form-group col-md-4" align="center">
							<br>
							<div class="row">
								<div class="col-md-4">
								</div>
								<div class="col-md-4">
									<div class="row">
									</div>
								</div>
								<div class="col-md-4">
								</div>
							</div>
							<div>
								<button type="button" class="btn btn-primary btn-lg btn-block"
								ng-click="estadisticas()"> Buscar </button>
							</div>
						</div>
					</div>
					<div ng-hide="loader" align="center" style="margin-top: 5em">
						<div class="loader"></div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<div style="height: 30em; width: 55em" id="grafica1"
								ng-hide="graf1"></div>
						</div>
						<div class="col-md-4" ng-hide="tabla1">
							<table class="table table-sm" style="font-size: 12px;">
								<thead>
									<tr>
										<th scope="col">*</th>
										<th scope="col">Min</th>
										<th scope="col">Avg</th>
										<th scope="col">Max</th>
										<th scope="col">StdDev</th>
									</tr>
								</thead>
								<tbody>
									<tr bgcolor="#757EFF">
										<th scope="row">OUTPUT</th>
										<th>{{min}}</th>
										<th>{{avg}}</th>
										<th>{{max}}</th>
										<th>{{stddev}}</th>
									</tr>
									<tr bgcolor="#FFB138">
										<th scope="row">INPUT</th>
										<th>{{min_in}}</th>
										<th>{{avg_in}}</th>
										<th>{{max_in}}</th>
										<th>{{stddev_in}}</th>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<script
		src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular.min.js">
		
	</script>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
		integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
		integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
		crossorigin="anonymous"></script>
	<script src="resources/js/trafico_controller.js"></script>
	<script src="resources/js/plotly-latest.min.js"></script>
</body>
</html>