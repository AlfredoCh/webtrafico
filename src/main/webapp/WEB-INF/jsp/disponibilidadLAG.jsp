<!doctype html>
<html lang="en">
<head>
<title>Web de Tr�fico - Disponibilidad</title>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
	integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
	crossorigin="anonymous">
<link rel="stylesheet" href="resources/css/general.css">
</head>
<body ng-app="traficoApp">
	<div ng-controller="disponibilidadCtrl">
		<div class="background-image"></div>
		<div class="content">
			<div class="card">
				<div class="card-header">
					Azteca Comunicaciones Per� <a ng-click="regresar()"
						class="float-right">Regresar</a>
				</div>

				<div class="card-body">
					<div align="center">
						<h4>[ {{ tipo }} ] {{ localidad }} - Lag Group {{ lag_group
							}}</h4>
					</div>
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item"><a class="nav-link active" id="home-tab"
							data-toggle="tab" href="#home" role="tab" aria-controls="home"
							aria-selected="true">Sem/Mes/A�o</a></li>
						<li class="nav-item"><a class="nav-link" id="profile-tab"
							data-toggle="tab" href="#profile" role="tab"
							aria-controls="profile" aria-selected="false">Intervalos</a></li>
					</ul>
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active" id="home" role="tabpanel"
							aria-labelledby="home-tab">
							<table class="table">
								<thead>
									<tr>
										<th>#</th>
										<th>Semana</th>
										<th>Mes</th>
										<th>A�o</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th>Disponibilidad</th>
										<th>
											<div ng-if="semana > 97">
												<button type="button" class="btn btn-outline-success">{{semana}}%</button>
											</div>
											<div ng-if="semana > 95">
												<div ng-if="semana < 97">
													<button type="button" class="btn btn-outline-warning">{{semana}}%</button>
												</div>
											</div>
											<div ng-if="semana < 95">
												<button type="button" class="btn btn-outline-danger">{{semana}}%</button>
											</div>
										</th>
										<th>
											<div ng-if="mes > 97">
												<button type="button" class="btn btn-outline-success">{{mes}}%</button>
											</div>
											<div ng-if="mes > 95">
												<div ng-if="mes < 97">
													<button type="button" class="btn btn-outline-warning">{{mes}}%</button>
												</div>
											</div>
											<div ng-if="mes < 95">
												<button type="button" class="btn btn-outline-danger">{{mes}}%</button>
											</div>
										</th>
										<th>
											<div ng-if="anio > 97">
												<button type="button" class="btn btn-outline-success">{{anio}}%</button>
											</div>
											<div ng-if="anio > 95">
												<div ng-if="anio < 97">
													<button type="button" class="btn btn-outline-warning">{{anio}}%</button>
												</div>
											</div>
											<div ng-if="anio < 95">
												<button type="button" class="btn btn-outline-danger">{{anio}}%</button>
											</div>
										</th>
									</tr>
									<tr>
										<th>Tiempo acumulado</th>
										<td>{{t_sem}}</td>
										<td>{{t_mes}}</td>
										<td>{{t_ani}}</td>
									</tr>
								</tbody>
							</table>
							<table class="table table-bordered">
								<thead>
									<tr>
										<th scope="col">#</th>
										<th scope="col">Fecha Inicio</th>
										<th scope="col">Fecha Fin</th>
										<th scope="col">Tiempo Total</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="evento in eventos">
										<td>{{$index + 1}}</td>
										<td>{{evento.inicio}}</td>
										<td>{{evento.fin}}</td>
										<td>{{evento.duracion}}</td>
									</tr>
								</tbody>
							</table>
							<div ng-hide="loader" align="center" style="margin-top: 5em">
								<div class="loader"></div>
							</div>
						</div>
						<div class="tab-pane fade" id="profile" role="tabpanel"
							aria-labelledby="profile-tab">
							<br>
							<div class="form-row" ng-hide="filtro">
								<div class="form-group col-md-4">
									<label for="inputInicio">Fecha Inicio</label> <input
										ng-model="dtini" class="form-control" type="date" id="dtini"
										required> <input ng-model="tmini" class="form-control"
										type="time" id="tmini" required>
								</div>
								<div class="form-group col-md-4">
									<label for="inputFin">Fecha Fin</label> <input ng-model="dtfin"
										class="form-control" type="date" id="dtfin"> <input
										ng-model="tmfin" class="form-control" type="time" id="tmfin"
										required>
								</div>
								<div class="form-group col-md-4" align="center">
									<br> <br>
									<button type="button" class="btn btn-outline-primary"
										ng-click="dispoInterval()">Buscar</button>
								</div>
							</div>
							<div align="center">
								<div class="row">
									<div class="col-md-4"></div>
									<div class="col-md-4">
										<label><b>Disponibilidad:</b></label>
										<div align="center">
											<div ng-if=" dispo > 97 ">
												<button type="button" class="btn btn-outline-success">{{dispo}}%</button>
											</div>
											<div ng-if=" dispo > 95 ">
												<div ng-if=" dispo < 97 ">
													<button type="button" class="btn btn-outline-warning">{{dispo}}%</button>
												</div>
											</div>
											<div ng-if=" dispo < 95 ">
												<button type="button" class="btn btn-outline-danger">{{dispo}}%</button>
											</div>
										</div>
									</div>
									<div class="col-md-4" ng-hide="btn_filtro">
										<button class="btn" ng-click="regresarFiltro()">Filtros</button>
									</div>
								</div>
								<br> <label><b>Tiempo Acumulado:</b>
									{{tiempo_acum}}</label>
							</div>
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>Evento</th>
										<th>Fecha Inicio</th>
										<th>Fecha Fin</th>
										<th>Tiempo Total</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="ev in events">
										<th>{{$index + 1}}</th>
										<td>{{ev.inicio}}</td>
										<td>{{ev.fin}}</td>
										<td>{{ev.duracion}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script
		src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular.min.js">
		
	</script>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
		integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
		integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
		crossorigin="anonymous"></script>
	<script src="resources/js/disponibilidadLAG_controller.js"></script>
</body>
</html>