<!doctype html>
<html lang="en">
<head>
<title>Web de Tr�fico - Login</title>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
	integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
	crossorigin="anonymous">
<link rel="stylesheet" href="resources/css/general.css">
</head>
<body ng-app="traficoApp">
	<div ng-controller="loginCtrl">
		<div class="background-image"></div>
		<div class="content">
			<div class="card " style="width: 25rem;">
				<img class="card-img-top" src="resources/img/security.jpg"
					alt="Card image cap">
				<div class="card-body">
					<h4 class="card-title text-center">Web de Tr�fico</h4>
					<h6 class="card-subtitle mb-2 text-muted text-center">Azteca
						Comunicaciones Per�</h6>
					<div class="form-group">
						<label for="exampleInputEmail1">Usuario</label> <input type="text"
							class="form-control" ng-model="user"
							placeholder="Ingrese usuario">
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Contrase�a</label> <input
							type="password" class="form-control" ng-model="pass"
							placeholder="Ingrese contrase�a">
					</div>
					<div align="center">
						<button type="submit" class="btn btn-primary" ng-click="login()">Ingresar</button>
					</div>
				</div>
				<div class="alert alert-danger" ng-hide="alerta" role="alert">Credenciales
					incorrectas</div>
			</div>

		</div>

	</div>

	<script
		src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular.min.js">
		
	</script>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
		integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
		integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
		crossorigin="anonymous"></script>
	<script src="resources/js/login_controller.js"></script>
	<script type="text/javascript">
		var _contextPath = "${pageContext.request.contextPath}";
	</script>
</body>
</html>