<!doctype html>
<html lang="en">
<head>
<title>Web de Tr�fico - Home</title>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
	integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
	crossorigin="anonymous">
<link rel="stylesheet" href="resources/css/general.css">
<link rel="stylesheet" href="resources/css/tableexport.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body ng-app="traficoApp">
	<div ng-controller="homeCtrl">
		<div class="background-image"></div>
		<div class="content">
			<div class="card">
				<div class="card-header">
					Azteca Comunicaciones Per� <a
						href="http://10.143.16.163:8080/WebTrafico/login"
						class="float-right">Salir</a>
				</div>
				<div class="card-body">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item"><a class="nav-link active" id="home-tab"
							data-toggle="tab" href="#home" role="tab" aria-controls="home"
							aria-selected="true">Enlace</a></li>
						<li class="nav-item"><a class="nav-link" id="profile-tab"
							data-toggle="tab" href="#profile" role="tab"
							aria-controls="profile" aria-selected="false">Lag</a></li>
						<li class="nav-item"><a class="nav-link" id="equipo-tab"
							data-toggle="tab" href="#equipo" role="tab"
							aria-controls="profile" aria-selected="false">Equipo</a></li>
						<li class="nav-item"><a class="nav-link" id="contact-tab"
							data-toggle="tab" href="#contact" role="tab"
							aria-controls="contact" aria-selected="false">Dashboard</a></li>
						<li class="nav-item"><a class="nav-link" id="reportes-tab"
							data-toggle="tab" href="#reportes" role="tab"
							aria-controls="reportes_enlaces" aria-selected="false">Reportes Enlaces</a></li>
						<li class="nav-item"><a class="nav-link" id="reportes_lag-tab"
							data-toggle="tab" href="#reportes_lag" role="tab"
							aria-controls="reportes_lag" aria-selected="false">Reportes Lags</a></li>
						<li class="nav-item"><a class="nav-link" id="reportes_equipos-tab"
							data-toggle="tab" href="#reportes_equipos" role="tab"
							aria-controls="reportes_equipos" aria-selected="false">Reportes Equipos</a></li>
					</ul>
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane fade show active" id="home" role="tabpanel"
							aria-labelledby="home-tab">
							<div class="row card-header">
								<div class="col-1" align="center">ID</div>
								<div class="col-sm" align="center">REGI�N</div>
								<div class="col-sm" align="center">LOCALIDAD</div>
								<div class="col-sm" align="center">IP</div>
								<div class="col-sm" align="center">TIPO</div>
							</div>
							<div class="row card-header">
								<div class="col-1">
									<input class="form-control" ng-model="search.ID" type="text">
								</div>
								<div class="col-sm">
									<input class="form-control" ng-model="search.REGION"
										type="text">
								</div>
								<div class="col-sm">
									<input class="form-control" ng-model="search.LOCALIDAD"
										type="text">
								</div>
								<div class="col-sm">
									<input class="form-control" ng-model="search.IP" type="text">
								</div>
								<div class="col-sm">
									<input class="form-control" ng-model="search.TIPO" type="text">
								</div>
							</div>
							<div ng-hide="loader" align="center" style="margin-top: 5em">
								<div class="loader"></div>
							</div>
							<div id="accordion" role="tablist">
								<div ng-repeat="e in equipos | filter:search:strict"
									class="card">
									<div class="row card-header collapsed" role="tab"
										id="headingTwo" ng-click="getEnlaces(e.ID)"
										data-toggle="collapse" href="#collapse{{e.ID}}"
										aria-expanded="false" aria-controls="collapseTwo"
										style="cursor: pointer">
										<div class="col-1" align="center">{{e.ID}}</div>
										<div class="col-sm" align="center">{{e.REGION}}</div>
										<div class="col-sm" align="center">{{e.LOCALIDAD}}</div>
										<div class="col-sm" align="center">{{e.IP}}</div>
										<div class="col-sm" align="center">{{e.TIPO}}</div>
									</div>
									<div id="collapse{{e.ID}}" class="collapse" role="tabpanel"
										aria-labelledby="headingTwo" data-parent="#accordion">
										<div ng-hide="enLoader" class="bar-container"
											style="margin-top: 2em">
											<div class="bar"></div>
											<div class="bar"></div>
											<div class="bar"></div>
											<div class="bar"></div>
										</div>
										<div ng-hide="enData">
											<table class="table table-striped">
												<thead>
													<tr>
														<th>ID Enlace</th>
														<th>Interfaz Origen</th>
														<th>Destino</th>
														<th>Interfaz Destino</th>
														<th>LAG</th>
														<th>Disponibilidad</th>
														<th>Tr�fico</th>
														<th>Tr�fico Colas</th>
													</tr>
												</thead>
												<tbody>
													<tr ng-repeat="enlace in enlaces">
														<td>{{enlace.id_enlace}}</td>
														<td>{{enlace.intf_org}}</td>
														<td>{{enlace.destino}}</td>
														<td>{{enlace.intf_dst}}</td>
														<td>{{enlace.lag_group}}</td>
														<td>
															<div ng-if="enlace.indisponibilidad < 95">
																<button class="btn btn-danger btn-sm" type="button"
																	style="cursor: pointer"
																	ng-click="irIndisponibilidad(enlace.id_enlace, e.ID, 'enlace')">{{enlace.indisponibilidad}}%</button>
															</div>
															<div ng-if="enlace.indisponibilidad > 95">
																<div ng-if="enlace.indisponibilidad < 97">
																	<button class="btn btn-warning btn-sm" type="button"
																		style="cursor: pointer"
																		ng-click="irIndisponibilidad(enlace.id_enlace, e.ID, 'enlace')">{{enlace.indisponibilidad}}%</button>
																</div>
																<div ng-if="enlace.indisponibilidad > 97">
																	<button class="btn btn-success btn-sm" type="button"
																		style="cursor: pointer"
																		ng-click="irIndisponibilidad(enlace.id_enlace, e.ID, 'enlace')">{{enlace.indisponibilidad}}%</button>
																</div>
															</div>
														</td>
														<td>
															<button type="button" class="btn btn-sm"
																ng-click="irTrafico(enlace.id_enlace, e.ID, 'enlaces')"
																style="cursor: pointer">
																<img height="13" width="22"
																	src="resources/css/open-iconic-master/svg/graph.svg"
																	alt="icon name">
															</button>
														</td>
														<td>
															<button type="button" class="btn btn-sm"
																ng-click="irGrafica(enlace.id_enlace, e.ID, 'enlaces')"
																style="cursor: pointer">
																<img height="13" width="22"
																	src="resources/css/open-iconic-master/svg/graph.svg"
																	alt="icon name">
															</button>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="profile" role="tabpanel"
							aria-labelledby="profile-tab">
							<div class="row card-header">
								<div class="col-1" align="center">ID</div>
								<div class="col-sm" align="center">REGI�N</div>
								<div class="col-sm" align="center">LOCALIDAD</div>
								<div class="col-sm" align="center">IP</div>
								<div class="col-sm" align="center">TIPO</div>
							</div>
							<div class="row card-header">
								<div class="col-1">
									<input class="form-control" ng-model="search2.ID" type="text">
								</div>
								<div class="col-sm">
									<input class="form-control" ng-model="search2.REGION"
										type="text">
								</div>
								<div class="col-sm">
									<input class="form-control" ng-model="search2.LOCALIDAD"
										type="text">
								</div>
								<div class="col-sm">
									<input class="form-control" ng-model="search2.IP" type="text">
								</div>
								<div class="col-sm">
									<input class="form-control" ng-model="search2.TIPO" type="text">
								</div>
							</div>
							<div id="accordion2" role="tablist">
								<div ng-repeat="e in equipos | filter:search2:strict"
									class="card">
									<div class="row collapsed card-header" role="tab"
										id="headingTwo" ng-click="getLags(e.ID)"
										data-toggle="collapse" href="#collapse88{{e.ID}}"
										aria-expanded="false" aria-controls="collapseTwo"
										style="cursor: pointer">
										<div class="col-1" align="center">{{e.ID}}</div>
										<div class="col-sm" align="center">{{e.REGION}}</div>
										<div class="col-sm" align="center">{{e.LOCALIDAD}}</div>
										<div class="col-sm" align="center">{{e.IP}}</div>
										<div class="col-sm" align="center">{{e.TIPO}}</div>
									</div>
									<div id="collapse88{{e.ID}}" class="collapse" role="tabpanel"
										aria-labelledby="headingTwo" data-parent="#accordion2">
										<div ng-hide="enLoader" class="bar-container"
											style="margin-top: 2em">
											<div class="bar"></div>
											<div class="bar"></div>
											<div class="bar"></div>
											<div class="bar"></div>
										</div>
										<div ng-hide="enData">
											<table class="table table-striped">
												<thead>
													<tr>
														<th>ID Lag</th>
														<th>LAG</th>
														<th>Disponibilidad</th>
														<th>Tr�fico</th>
														<th>Tr�fico Colas</th>
													</tr>
												</thead>
												<tbody>
													<tr ng-repeat="l in lags">
														<td>{{l.id_lag}}</td>
														<td>Lag Group {{l.lag_group}}</td>
														<td>
															<div ng-if="l.disponibilidad < 95">
																<button class="btn btn-danger btn-sm" type="button"
																	style="cursor: pointer"
																	ng-click="irIndisponibilidadLag(l.lag_group, e.ID, l.id_lag, 'lag')">{{l.disponibilidad}}%</button>
															</div>
															<div ng-if="l.disponibilidad > 95">
																<div ng-if="l.disponibilidad < 97">
																	<button class="btn btn-warning btn-sm" type="button"
																		style="cursor: pointer"
																		ng-click="irIndisponibilidadLag(l.lag_group, e.ID, l.id_lag, 'lag')">{{l.disponibilidad}}%</button>
																</div>
																<div ng-if="l.disponibilidad > 97">
																	<button class="btn btn-success btn-sm" type="button"
																		style="cursor: pointer"
																		ng-click="irIndisponibilidadLag(l.lag_group, e.ID, l.id_lag, 'lag')">{{l.disponibilidad}}%</button>
																</div>
															</div>
														</td>
														<td>
															<button type="button" class="btn btn-sm"
																style="cursor: pointer"
																ng-click="irTraficoLag(l.lag_group, e.ID, l.id_lag, 'lag')">
																<img height="13" width="22"
																	src="resources/css/open-iconic-master/svg/graph.svg"
																	alt="icon name">
															</button>

														</td>
														<td>
															<button type="button" class="btn btn-sm"
																style="cursor: pointer"
																ng-click="irGraficaLag(l.lag_group, e.ID, l.id_lag, 'lag')">
																<img height="13" width="22"
																	src="resources/css/open-iconic-master/svg/graph.svg"
																	alt="icon name">
															</button>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="equipo" role="tabpanel"
							aria-labelledby="equipo">
							<div class="row card-header">
								<div class="col-1" align="center">ID</div>
								<div class="col-sm" align="center">REGI�N</div>
								<div class="col-sm" align="center">LOCALIDAD</div>
								<div class="col-sm" align="center">IP</div>
								<div class="col-sm" align="center">TIPO</div>
								<div class="col-sm" align="center">DISPONIBILIDAD</div>
							</div>
							<div class="row card-header">
								<div class="col-1">
									<input class="form-control" ng-model="search3.ID" type="text">
								</div>
								<div class="col-sm">
									<input class="form-control" ng-model="search3.REGION"
										type="text">
								</div>
								<div class="col-sm">
									<input class="form-control" ng-model="search3.LOCALIDAD"
										type="text">
								</div>
								<div class="col-sm">
									<input class="form-control" ng-model="search3.IP" type="text">
								</div>
								<div class="col-sm">
									<input class="form-control" ng-model="search3.TIPO" type="text">
								</div>
								<div class="col-sm"></div>
							</div>
							<div ng-hide="loader" align="center" style="margin-top: 5em">
								<div class="loader"></div>
							</div>
							<div id="accordion2" role="tablist">
								<div ng-repeat="e in equipos2 | filter:search3:strict"
									class="card">
									<div class="row collapsed card-header" role="tab"
										id="headingTwo" data-toggle="collapse"
										href="#collapse77{{e.ID}}" aria-expanded="false"
										aria-controls="collapseTwo" style="cursor: pointer">
										<div class="col-1" align="center">{{e.ID}}</div>
										<div class="col-sm" align="center">{{e.REGION}}</div>
										<div class="col-sm" align="center">{{e.LOCALIDAD}}</div>
										<div class="col-sm" align="center">{{e.IP}}</div>
										<div class="col-sm" align="center">{{e.TIPO}}</div>
										<div class="col-sm" align="center">
											<div>
												<button class="ng-class: e.COLOR" type="button"
												style="cursor: pointer" ng-click="irIndisponibilidadEquipo(e.ID, 'equipo')">
												{{e.DISPONIBILIDAD}}%</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="contact" role="tabpanel"
							aria-labelledby="contact-tab">
							<div class="form-row">
								<div class="form-group col-md-4" align="center">
									<label> <b>Fecha Inicio</b></label> <input ng-model="dtini"
										class="form-control" type="date" id="dtini" required>
									<input ng-model="tmini" class="form-control" type="time"
										id="tmini">
								</div>
								<div class="form-group col-md-4" align="center">
									<label> <b>Fecha Fin</b>
									</label> <input ng-model="dtfin" class="form-control" type="date"
										id="dtfin" required> <input ng-model="tmfin"
										class="form-control" type="time" id="tmfin">
								</div>
								<div class="form-group col-md-4" align="center">
									<br>
									<div class="row">
										<div class="col-md-4">
											
										</div>
										<div class="col-md-4">
											
										</div>
										<div class="col-md-4">
											
										</div>
									</div>
									<div>
										<button type="button" class="btn btn-primary btn-lg btn-block"
											ng-click="getDashboardFechas()">Buscar</button>
									</div>
								</div>
							</div>
							<div>
								<div class="row">
									<div class="col-md-6">
										Disponibilidad Equipos
										<table class="table table-sm">
											<thead>
												<tr>
													<th>Jerarqu�a</th>
													<th>Dispo</th>
													<th>Minutos</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<th><font size="2">Conexi�n</font></th>
													<td style="background-color:{{c_conexion}};"><font size="2">{{conexion}}%</font></td>
													<td><font size="2">{{c_minutos_conexion}}</font></td>
												</tr>
												<tr>
													<th><font size="2">Distribuidor</font></th>
													<td style="background-color:{{c_distribuidor}};"><font size="2">{{distribuidor}}%</font></td>
													<td><font size="2">{{c_minutos_distribuidor}}</font></td>
												</tr>
												<tr>
													<th><font size="2">Distribuidor SR</font></th>
													<td style="background-color:{{c_distribuidor_sr}};"><font size="2">{{distribuidor_sr}}%</font></td>
													<td><font size="2">{{c_minutos_distribuidor_sr}}</font></td>
												</tr>
												<tr>
													<th><font size="2">Distribuidor RR</font></th>
													<td style="background-color:{{c_distribuidor_rr}};"><font size="2">{{distribuidor_rr}}%</font></td>
													<td><font size="2">{{c_minutos_distribuidor_rr}}</font></td>
												</tr>
												<tr>
													<th><font size="2">Agregador SR</font></th>
													<td style="background-color:{{c_agregador_sr}};"><font size="2">{{agregador_sr}}%</font></td>
													<td><font size="2">{{c_minutos_agregador_sr}}</font></td>
												</tr>
												<tr>
													<th><font size="2">Agregador RR</font></th>
													<td style="background-color:{{c_agregador_rr}};"><font size="2">{{agregador_rr}}%</font></td>
													<td><font size="2">{{c_minutos_agregador_rr}}</font></td>
												</tr>
												<tr>
													<th><font size="2">Core</font></th>
													<td style="background-color:{{c_core}};"><font size="2">{{core}}%</font></td>
													<td><font size="2">{{c_minutos_core}}</font></td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="col-md-6">Top 7 - Indisponibilidad Enlaces
										<table class="table table-sm">
											<thead>
												<tr>
													<th>ID</th>
													<th>Enlace</th>
													<th>Dispo</th>
													<th>Minutos</th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="ed in enla_dash">
													<td><font size="2">{{ed.id_enlace}}</font></td>
													<td><font size="2">{{ed.enlace}}</font></td>
													<td style="background-color:{{ed.color}};">
													<font size="2">{{ed.dispo}}%</font></td>
													<td><font size="2">{{ed.minutos}}</font></td>
												</tr>
											</tbody>
										</table>
									</div>
									<!--  
									<div class="col-md-5">Top 7 - Indisponibilidad Lag
										<table class="table table-sm">
											<thead>
												<tr>
													<th>ID</th>
													<th>Lag</th>
													<th>Dispo</th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="ld in lag_dash">
													<td><font size="2">{{ld.id_lag}}</font></td>
													<td><font size="2">{{ld.lag}}</font></td>
													<td style="background-color:{{ld.color}};">
													<font size="2">{{ld.dispo}}%</font></td>
												</tr>
											</tbody>
										</table>
									</div>
									-->
								</div>
								<div class="row">
									<div class="col-md-6">Top 7 - Utilizaci�n de Tr�fico Core
										<table class="table table-sm">
											<thead>
												<tr>
													<th>ID</th>
													<th>Enlace</th>
													<th>Util%</th>
													<th>Avg</th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="tc in traf_core_dash">
													<td><font size="2">{{tc.id_enlace}}</font></td>
													<td><font size="2">{{tc.enlace}}</font></td>
													<td style="background-color:{{tc.color}};">
														<font size="2">{{tc.utilizacion}} %</font>
													</td>
													<td><font size="2">{{tc.trafico}}</font></td>
												</tr>
											</tbody>
										</table>
									</div>
									<div class="col-md-6">Top 7 - Utilizaci�n de Tr�fico Agregador
										<table class="table table-sm">
											<thead>
												<tr>
													<th>ID</th>
													<th>Enlace</th>
													<th>Util%</th>
													<th>Avg</th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="ta in traf_agrega_dash">
													<td><font size="2">{{ta.id_enlace}}</font></td>
													<td><font size="2">{{ta.enlace}}</font></td>
													<td style="background-color:{{ta.color}};">
														<font size="2">{{ta.utilizacion}}%</font>
													</td>
													<td><font size="2">{{ta.trafico}}</font></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane fade" id="reportes" role="tabpanel"
							aria-labelledby="reportes">
							<div class="form-row">
								<div class="form-group col-md-4" align="center">
									<label> <b>Fecha Inicio</b></label> <input ng-model="dtini"
										class="form-control" type="date" id="dtini" required>
									<input ng-model="tmini" class="form-control" type="time"
										id="tmini">
								</div>
								<div class="form-group col-md-4" align="center">
									<label> <b>Fecha Fin</b>
									</label> <input ng-model="dtfin" class="form-control" type="date"
										id="dtfin" required> <input ng-model="tmfin"
										class="form-control" type="time" id="tmfin">
								</div>
								<div class="form-group col-md-4" align="center">
									<br>
									<div class="row">
										<div class="col-md-4">
											<input class="form-check-input" type="checkbox" ng-model="unidades" checked="false"><span>Mbps</span>
										</div>
										<div class="col-md-4">
											<div class="row">
												<label>Input Traffic</label> <label class="switch">
													<input type="checkbox" ng-click="inputTrafic()"> <span
													class="slider round"></span>
												</label>
											</div>
										</div>
										<div class="col-md-4">
											<button class="btn btn-block" ng-click="exportTableTrafico(reporte_trafico_output)"
												id="XLSButton"><i class="fa fa-file-excel-o" style="font-size:22px"></i> Exportar </button>
										</div>
									</div>
									<div>
										<button type="button" class="btn btn-primary btn-lg btn-block"
											ng-click="getReporteTraficoEnlaceFechas()">Buscar</button>
									</div>
								</div>
							</div>
							<div align="center" ng-hide="reporte_trafico_output">
								<h3>Reporte Tr�fico Output de Enlaces</h3>

								<table class="table table-sm table-hover"
									style="font-size: 12px;" id="reporte_trafico_output">
									<tbody>
										<tr>
											<th>
												<button ng-click="sortBy('id_enlace')"
													class="btn btn-primary btn-sm btn-block">ID</button> <span
												class="sortorder" ng-show="propertyName === 'id_enlace'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('tipo')"
													class="btn btn-primary btn-sm btn-block">TIPO</button> <span
												class="sortorder" ng-show="propertyName === 'tipo'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('region_org')"
													class="btn btn-primary btn-sm btn-block">REGION
													ORG</button> <span class="sortorder"
												ng-show="propertyName === 'region_org'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('localidad_org')"
													class="btn btn-primary btn-sm btn-block">LOCALIDAD
													ORG</button> <span class="sortorder"
												ng-show="propertyName === 'localidad_org'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('interfaz_org')"
													class="btn btn-primary btn-sm btn-block">INTF ORG</button> <span class="sortorder"
												ng-show="propertyName === 'interfaz_org'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('lag')"
													class="btn btn-primary btn-sm btn-block">LAG</button><span
												class="sortorder" ng-show="propertyName === 'lag'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('localidad_dst')"
													class="btn btn-primary btn-sm btn-block">LOCALIDAD
													DST</button> <span class="sortorder"
												ng-show="propertyName === 'localidad_dst'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('interfaz_dst')"
													class="btn btn-primary btn-sm btn-block">INTF DST</button> <span class="sortorder"
												ng-show="propertyName === 'interfaz_dst'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_dispo')"
													class="btn btn-primary btn-sm btn-block">DISPO</button> <span
												class="sortorder" ng-show="propertyName === 'raw_dispo'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_min')"
													class="btn btn-primary btn-sm btn-block">MIN</button> <span
												class="sortorder" ng-show="propertyName === 'raw_min'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_avg')"
													class="btn btn-primary btn-sm btn-block">AVG</button> <span
												class="sortorder" ng-show="propertyName === 'raw_avg'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_max')"
													class="btn btn-primary btn-sm btn-block">MAX</button> <span
												class="sortorder" ng-show="propertyName === 'raw_max'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_stddev')"
													class="btn btn-primary btn-sm btn-block">STDDEV</button> <span
												class="sortorder" ng-show="propertyName === 'raw_stddev'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('capacidad')"
													class="btn btn-primary btn-sm btn-block">CAPA</button> <span
												class="sortorder" ng-show="propertyName === 'raw_capacidad'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_utilizacion')"
													class="btn btn-primary btn-sm btn-block">UTILIZ</button> <span
												class="sortorder" ng-show="propertyName === 'raw_utilizacion'"
												ng-class="{reverse: reverse}"></span>
											</th>
										</tr>
										<tr>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search4.id_enlace"></th>
											<th><input type="text"
												class="form-control form-control-sm" ng-model="search4.tipo">
											</th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search4.region_org"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search4.localidad_org"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search4.interfaz_org"></th>
											<th><input type="text"
												class="form-control form-control-sm" ng-model="search4.lag">
											</th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search4.localidad_dst"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search4.interfaz_dst"></th>
											<th><input type="text" class="form-control form-control-sm"
												ng-model="search4.dispo"></th>
											<th><input type="text"
												class="form-control form-control-sm" ng-model="search4.min">
											</th>
											<th><input type="text"
												class="form-control form-control-sm" ng-model="search4.avg">
											</th>
											<th><input type="text"
												class="form-control form-control-sm" ng-model="search4.max">
											</th>
											<th><input type="text" class="form-control form-control-sm"
												ng-model="search4.stddev"></th>
											<th><input type="text" class="form-control form-control-sm"
												ng-model="search4.capacidad"></th>
											<th><input type="text" class="form-control form-control-sm"
												ng-model="search4.utilizacion"></th>
										</tr>
										<tr
											ng-repeat="er in enlaces_reportes | orderBy:propertyName:reverse | filter:search4:strict"
											ng-click="irTrafico(er.id_enlace, er.id_equipo, 'reporte_enlace')"
											style="cursor: pointer">
											<td>{{er.id_enlace}}</td>
											<td>{{er.tipo}}</td>
											<td>{{er.region_org}}</td>
											<td>{{er.localidad_org}}</td>
											<td>{{er.interfaz_org}}</td>
											<td>{{er.lag}}</td>
											<td>{{er.localidad_dst}}</td>
											<td>{{er.interfaz_dst}}</td>
											<td>{{er.dispo}}</td>
											<td ng-if="unidades == false">{{er.min}}</td>
											<td ng-if="unidades == false">{{er.avg}}</td>
											<td ng-if="unidades == false">{{er.max}}</td>
											<td ng-if="unidades == false">{{er.stddev}}</td>
											<td ng-if="unidades == true">{{er.raw_min}}</td>
											<td ng-if="unidades == true">{{er.raw_avg}}</td>
											<td ng-if="unidades == true">{{er.raw_max}}</td>
											<td ng-if="unidades == true">{{er.raw_stddev}}</td>
											<td>{{er.capacidad}}</td>
											<td>{{er.utilizacion}}</td>
										</tr>
									</tbody>
								</table>
							</div>
							
							<div align="center" ng-hide="reporte_trafico_input">
								<h3>Reporte Tr�fico Input de Enlaces</h3>

								<table class="table table-sm table-hover"
									style="font-size: 12px;" id="reporte_trafico_input">
									<tbody>
										<tr>
											<th>
												<button ng-click="sortBy('id_enlace')"
													class="btn btn-primary btn-sm btn-block">ID</button> <span
												class="sortorder" ng-show="propertyName === 'id_enlace'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('tipo')"
													class="btn btn-primary btn-sm btn-block">TIPO</button> <span
												class="sortorder" ng-show="propertyName === 'tipo'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('region_org')"
													class="btn btn-primary btn-sm btn-block">REGION
													ORG</button> <span class="sortorder"
												ng-show="propertyName === 'region_org'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('localidad_org')"
													class="btn btn-primary btn-sm btn-block">LOCALIDAD
													ORG</button> <span class="sortorder"
												ng-show="propertyName === 'localidad_org'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('interfaz_org')"
													class="btn btn-primary btn-sm btn-block">INTF ORG</button> <span class="sortorder"
												ng-show="propertyName === 'interfaz_org'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('lag')"
													class="btn btn-primary btn-sm btn-block">LAG</button> <span
												class="sortorder" ng-show="propertyName === 'lag'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('localidad_dst')"
													class="btn btn-primary btn-sm btn-block">LOCALIDAD
													DST</button> <span class="sortorder"
												ng-show="propertyName === 'localidad_dst'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('interfaz_dst')"
													class="btn btn-primary btn-sm btn-block">INTF DST</button> <span class="sortorder"
												ng-show="propertyName === 'interfaz_dst'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_dispo')"
													class="btn btn-primary btn-sm btn-block">DISPO</button> <span
												class="sortorder" ng-show="propertyName === 'raw_dispo'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_min2')"
													class="btn btn-primary btn-sm btn-block">MIN</button> <span
												class="sortorder" ng-show="propertyName === 'raw_min2'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_avg2')"
													class="btn btn-primary btn-sm btn-block">AVG</button> <span
												class="sortorder" ng-show="propertyName === 'raw_avg2'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_max2')"
													class="btn btn-primary btn-sm btn-block">MAX</button> <span
												class="sortorder" ng-show="propertyName === 'raw_max2'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_stddev2')"
													class="btn btn-primary btn-sm btn-block">STDDEV</button> <span
												class="sortorder" ng-show="propertyName === 'raw_stddev2'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_capacidad')"
													class="btn btn-primary btn-sm btn-block">CAPA</button> <span
												class="sortorder" ng-show="propertyName === 'raw_capacidad'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_utilizacion2')"
													class="btn btn-primary btn-sm btn-block">UTILIZ</button> <span
												class="sortorder" ng-show="propertyName === 'raw_utilizacion2'"
												ng-class="{reverse: reverse}"></span>
											</th>
										</tr>
										<tr>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search4.id_enlace"></th>
											<th><input type="text"
												class="form-control form-control-sm" ng-model="search4.tipo">
											</th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search4.region_org"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search4.localidad_org"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search4.interfaz_org"></th>
											<th><input type="text"
												class="form-control form-control-sm" ng-model="search4.lag">
											</th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search4.localidad_dst"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search4.interfaz_dst"></th>
											<th><input type="text"
												class="form-control form-control-sm" ng-model="search4.dispo">
											</th>
											<th><input type="text"
												class="form-control form-control-sm" ng-model="search4.min">
											</th>
											<th><input type="text"
												class="form-control form-control-sm" ng-model="search4.avg">
											</th>
											<th><input type="text"
												class="form-control form-control-sm" ng-model="search4.max">
											</th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search4.stddev"></th>
											<th><input type="text"
												class="form-control form-control-sm" ng-model="search4.capacidad">
											</th>
											<th><input type="text"
												class="form-control form-control-sm" ng-model="search4.utilizacion2">
											</th>
										</tr>
										<tr
											ng-repeat="er in enlaces_reportes | orderBy:propertyName:reverse | filter:search4:strict"
											ng-click="irTrafico(er.id_enlace, er.id_equipo)"
											style="cursor: pointer">
											<td>{{er.id_enlace}}</td>
											<td>{{er.tipo}}</td>
											<td>{{er.region_org}}</td>
											<td>{{er.localidad_org}}</td>
											<td>{{er.interfaz_org}}</td>
											<td>{{er.lag}}</td>
											<td>{{er.localidad_dst}}</td>
											<td>{{er.interfaz_dst}}</td>
											<td>{{er.dispo}}</td>
											<td ng-if="unidades == false">{{er.min2}}</td>
											<td ng-if="unidades == false">{{er.avg2}}</td>
											<td ng-if="unidades == false">{{er.max2}}</td>
											<td ng-if="unidades == false">{{er.stddev2}}</td>
											<td ng-if="unidades == true">{{er.raw_min2}}</td>
											<td ng-if="unidades == true">{{er.raw_avg2}}</td>
											<td ng-if="unidades == true">{{er.raw_max2}}</td>
											<td ng-if="unidades == true">{{er.raw_stddev2}}</td>
											<td>{{er.capacidad}}</td>
											<td>{{er.utilizacion2}}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="tab-pane fade" id="reportes_lag" role="tabpanel"
							aria-labelledby="reportes">
							<div class="form-row">
								<div class="form-group col-md-4" align="center">
									<label> <b>Fecha Inicio</b></label> <input ng-model="dtini"
										class="form-control" type="date" id="dtini" required>
									<input ng-model="tmini" class="form-control" type="time"
										id="tmini">
								</div>
								<div class="form-group col-md-4" align="center">
									<label> <b>Fecha Fin</b>
									</label> <input ng-model="dtfin" class="form-control" type="date"
										id="dtfin" required> <input ng-model="tmfin"
										class="form-control" type="time" id="tmfin">
								</div>
								<div class="form-group col-md-4" align="center">
									<br>
									<div class="row">
										<div class="col-md-4">
											<input class="form-check-input" type="checkbox" ng-model="unidades" checked="false"><span>Mbps</span>
										</div>
										<div class="col-md-4">
											<div class="row">
												<label>Input Traffic</label> <label class="switch">
													<input type="checkbox" ng-click="inputLag()"> <span
													class="slider round"></span>
												</label>
											</div>
										</div>
										<div class="col-md-4">
											<button class="btn btn-block" ng-click="exportTableLag(reporte_lag_output)"
												id="XLSButton"><i class="fa fa-file-excel-o" style="font-size:22px"></i> Exportar </button>
										</div>
									</div>
									<div>
										<button type="button" class="btn btn-primary btn-lg btn-block"
											ng-click="getReporteTraficoLagFechas()">Buscar</button>
									</div>
								</div>
							</div>
							<div align="center" ng-hide="reporte_lag_output">
								<h3>Reporte Tr�fico Output de Lags</h3>

								<table class="table table-sm table-hover"
									style="font-size: 12px;" id="reporte_lag_output">
									<tbody>
										<tr>
											<th>
												<button ng-click="sortBy('id_lag')"
													class="btn btn-primary btn-sm btn-block">ID</button> <span
												class="sortorder" ng-show="propertyName === 'id_lag'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('tipo')"
													class="btn btn-primary btn-sm btn-block">TIPO</button> <span
												class="sortorder" ng-show="propertyName === 'tipo'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('region_org')"
													class="btn btn-primary btn-sm btn-block">REGION
													ORG</button> <span class="sortorder"
												ng-show="propertyName === 'region_org'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('localidad_org')"
													class="btn btn-primary btn-sm btn-block">LOCALIDAD
													ORG</button> <span class="sortorder"
												ng-show="propertyName === 'localidad_org'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('lag')"
													class="btn btn-primary btn-sm btn-block">LAG</button><span
												class="sortorder" ng-show="propertyName === 'lag'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_dispo')"
													class="btn btn-primary btn-sm btn-block">DISPO</button> <span
												class="sortorder" ng-show="propertyName === 'raw_dispo'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_min')"
													class="btn btn-primary btn-sm btn-block">MIN</button> <span
												class="sortorder" ng-show="propertyName === 'raw_min'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_avg')"
													class="btn btn-primary btn-sm btn-block">AVG</button> <span
												class="sortorder" ng-show="propertyName === 'raw_avg'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_max')"
													class="btn btn-primary btn-sm btn-block">MAX</button> <span
												class="sortorder" ng-show="propertyName === 'raw_max'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_stddev')"
													class="btn btn-primary btn-sm btn-block">STDDEV</button> <span
												class="sortorder" ng-show="propertyName === 'raw_stddev'"
												ng-class="{reverse: reverse}"></span>
											</th>
										</tr>
										<tr>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search6.id_lag"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search6.tipo"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search6.region_org"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search6.localidad_org"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search6.lag"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search6.dispo"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search6.min"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search6.avg"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search6.max"></th>
											<th><input type="text" class="form-control form-control-sm"
												ng-model="search6.stddev"></th>
										</tr>
										<tr
											ng-repeat="lr in lags_reportes | orderBy:propertyName:reverse | filter:search6:strict"
											ng-click="irTraficoLag(lr.lag_group, lr.id_equipo, lr.id_lag, 'reporte_lag')"
											style="cursor: pointer">
											<td>{{lr.id_lag}}</td>
											<td>{{lr.tipo}}</td>
											<td>{{lr.region_org}}</td>
											<td>{{lr.localidad_org}}</td>
											<td>{{lr.lag_group}}</td>
											<td>{{lr.dispo}}</td>
											<td ng-if="unidades == false">{{lr.min}}</td>
											<td ng-if="unidades == false">{{lr.avg}}</td>
											<td ng-if="unidades == false">{{lr.max}}</td>
											<td ng-if="unidades == false">{{lr.stddev}}</td>
											<td ng-if="unidades == true">{{lr.raw_min}}</td>
											<td ng-if="unidades == true">{{lr.raw_avg}}</td>
											<td ng-if="unidades == true">{{lr.raw_max}}</td>
											<td ng-if="unidades == true">{{lr.raw_stddev}}</td>
										</tr>
									</tbody>
								</table>
							</div>
							
							<div align="center" ng-hide="reporte_lag_input">
								<h3>Reporte Tr�fico Input de Lags</h3>

								<table class="table table-sm table-hover"
									style="font-size: 12px;" id="reporte_lag_input">
									<tbody>
										<tr>
											<th>
												<button ng-click="sortBy('id_lag')"
													class="btn btn-primary btn-sm btn-block">ID</button> <span
												class="sortorder" ng-show="propertyName === 'id_lag'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('tipo')"
													class="btn btn-primary btn-sm btn-block">TIPO</button> <span
												class="sortorder" ng-show="propertyName === 'tipo'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('region_org')"
													class="btn btn-primary btn-sm btn-block">REGION
													ORG</button> <span class="sortorder"
												ng-show="propertyName === 'region_org'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('localidad_org')"
													class="btn btn-primary btn-sm btn-block">LOCALIDAD
													ORG</button> <span class="sortorder"
												ng-show="propertyName === 'localidad_org'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('lag')"
													class="btn btn-primary btn-sm btn-block">LAG</button> <span
												class="sortorder" ng-show="propertyName === 'lag'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_dispo')"
													class="btn btn-primary btn-sm btn-block">DISPO</button> <span
												class="sortorder" ng-show="propertyName === 'raw_dispo'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_min2')"
													class="btn btn-primary btn-sm btn-block">MIN</button> <span
												class="sortorder" ng-show="propertyName === 'raw_min2'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_avg2')"
													class="btn btn-primary btn-sm btn-block">AVG</button> <span
												class="sortorder" ng-show="propertyName === 'raw_avg2'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_max2')"
													class="btn btn-primary btn-sm btn-block">MAX</button> <span
												class="sortorder" ng-show="propertyName === 'raw_max2'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_stddev2')"
													class="btn btn-primary btn-sm btn-block">STDDEV</button> <span
												class="sortorder" ng-show="propertyName === 'raw_stddev2'"
												ng-class="{reverse: reverse}"></span>
											</th>
										</tr>
										<tr>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search6.id_lag"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search6.tipo"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search6.region_org"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search6.localidad_org"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search6.lag"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search6.dispo"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search6.min"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search6.avg"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search6.max"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search6.stddev"></th>
										</tr>
										<tr
											ng-repeat="lr in lags_reportes | orderBy:propertyName:reverse | filter:search6:strict"
											ng-click="irTraficoLag(lr.lag_group, lr.id_equipo, lr.id_lag, 'reporte_lag')"
											style="cursor: pointer">
											<td>{{lr.id_lag}}</td>
											<td>{{lr.tipo}}</td>
											<td>{{lr.region_org}}</td>
											<td>{{lr.localidad_org}}</td>
											<td>{{lr.lag_group}}</td>
											<td>{{lr.dispo}}</td>
											<td ng-if="unidades == false">{{lr.min2}}</td>
											<td ng-if="unidades == false">{{lr.avg2}}</td>
											<td ng-if="unidades == false">{{lr.max2}}</td>
											<td ng-if="unidades == false">{{lr.stddev2}}</td>
											<td ng-if="unidades == true">{{lr.raw_min2}}</td>
											<td ng-if="unidades == true">{{lr.raw_avg2}}</td>
											<td ng-if="unidades == true">{{lr.raw_max2}}</td>
											<td ng-if="unidades == true">{{lr.raw_stddev2}}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="tab-pane fade" id="reportes_equipos" role="tabpanel"
							aria-labelledby="reportes">
							<div class="form-row">
								<div class="form-group col-md-4" align="center">
									<label> <b>Fecha Inicio</b></label> <input ng-model="dtini"
										class="form-control" type="date" id="dtini" required>
									<input ng-model="tmini" class="form-control" type="time"
										id="tmini">
								</div>
								<div class="form-group col-md-4" align="center">
									<label> <b>Fecha Fin</b>
									</label> <input ng-model="dtfin" class="form-control" type="date"
										id="dtfin" required> <input ng-model="tmfin"
										class="form-control" type="time" id="tmfin">
								</div>
								<div class="form-group col-md-4" align="center">
									<br>
									<div class="row">
										<div class="col-md-1"></div>
										<div class="col-md-6">
											<div class="row">
												
											</div>
										</div>
										<div class="col-md-5">
											<button class="btn btn-block" ng-click="exportTableEquipos()"
												id="XLSButton"><i class="fa fa-file-excel-o" style="font-size:22px"></i> Exportar </button>
										</div>
									</div>
									<div>
										<button type="button" class="btn btn-primary btn-lg btn-block"
											ng-click="getReporteEquiposFechas()">Buscar</button>
									</div>
								</div>
							</div>
							<div align="center" ng-hide="reporte_equipos">
								<h3>Reporte Disponibilidad Equipos</h3>
								<table class="table table-sm table-hover"
									style="font-size: 12px;" id="reporte_equipos">
									<tbody>
										<tr>
											<th>
												<button ng-click="sortBy('id_equipo')"
													class="btn btn-primary btn-sm btn-block">ID</button> <span
												class="sortorder" ng-show="propertyName === 'id_equipo'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('tipo')"
													class="btn btn-primary btn-sm btn-block">TIPO</button> <span
												class="sortorder" ng-show="propertyName === 'tipo'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('region')"
													class="btn btn-primary btn-sm btn-block">REGION
													</button> <span class="sortorder"
												ng-show="propertyName === 'region'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('localidad')"
													class="btn btn-primary btn-sm btn-block">LOCALIDAD
													</button> <span class="sortorder"
												ng-show="propertyName === 'localidad'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('hostname')"
													class="btn btn-primary btn-sm btn-block">HOSTNAME
													</button> <span class="sortorder"
												ng-show="propertyName === 'hostname'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('ip')"
													class="btn btn-primary btn-sm btn-block">IP</button><span
												class="sortorder" ng-show="propertyName === 'ip'"
												ng-class="{reverse: reverse}"></span>
											</th>
											<th>
												<button ng-click="sortBy('raw_dispo')"
													class="btn btn-primary btn-sm btn-block">DISPO</button> <span
												class="sortorder" ng-show="propertyName === 'raw_dispo'"
												ng-class="{reverse: reverse}"></span>
											</th>
										</tr>
										<tr>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search5.id_equipo"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search5.tipo"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search5.region"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search5.localidad"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search5.hostname"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search5.ip"></th>
											<th><input type="text"
												class="form-control form-control-sm"
												ng-model="search5.dispo"></th>
										</tr>
										<tr
											ng-repeat="eqr in equipos_reportes | orderBy:propertyName:reverse | filter:search5:strict"
											ng-click="irIndisponibilidadEquipo(eqr.id_equipo, 'reporte_equipo')"
											style="cursor: pointer">
											<td>{{eqr.id_equipo}}</td>
											<td>{{eqr.tipo}}</td>
											<td>{{eqr.region}}</td>
											<td>{{eqr.localidad}}</td>
											<td>{{eqr.hostname}}</td>
											<td>{{eqr.ip}}</td>
											<td>{{eqr.dispo}}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.6/angular.min.js">
		
	</script>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
		integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
		integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
		crossorigin="anonymous"></script>
	<script src="resources/js/home_controller.js">
		
	</script>
	<script src="resources/js/xlsx.core.min.js"></script>
	<script src="resources/js/FileSaver.min.js"></script>
	<script src="resources/js/tableexport.min.js"></script>
</body>
</html>