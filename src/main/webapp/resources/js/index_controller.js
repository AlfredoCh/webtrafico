var app = angular.module("clientesApp", []);
var url = "http://localhost:8080/springrest"
app.controller("indexCtrl", function($scope, $http) {
	var data = {
		texto : "Hola"
	}
	$http({
		method : 'GET',
		url : url + "/getData",
		data : data
	}).then(function(response) {
		var datos = response.data;
		var x = new Array
		var y = new Array
		for (var i = 0; i < datos.length; i++) {
			var registro = datos[i];
			x[i] = registro["fecha"]
			y[i] = registro["avgrt"]

		}

		$scope.x = x
		$scope.y = y

		var trace1 = {
			type : "scatter",
			mode : "lines",
			name : 'LIMA',
			x : $scope.x,
			y : $scope.y,
			line : {
				color : '#17BECF'
			}
		}

		var data = [ trace1 ];

		var layout = {
			title : 'DEMO WEB DE CLIENTES',
			xaxis : {
				autorange : true,
				range : [ '2017-11-01', '2017-11-15' ],
				rangeselector : {
					buttons : [ {
						count : 2,
						label : '2m',
						step : 'month',
						stepmode : 'backward'
					}, {
						count : 4,
						label : '4m',
						step : 'month',
						stepmode : 'backward'
					}, {
						step : 'all'
					} ]
				},
				rangeslider : {
					range : [ '2017-11-01', '2017-11-15' ]
				},
				type : 'date'
			},
			yaxis : {
				autorange : true,
				range : [ 0, 10 ],
				type : 'linear'
			}
		};

		Plotly.newPlot('myDiv', data, layout);
	})
});