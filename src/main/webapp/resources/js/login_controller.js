var app = angular.module("traficoApp", []);
var url = "/WebTrafico"
app.controller("loginCtrl", function($scope, $http) {
	$scope.alerta = true;
	$scope.login = function() {
		var data = {
			user : $scope.user,
			pass : $scope.pass
		}
		$scope.alerta = true;
		$http({
			method : 'POST',
			url : url + "/login",
			data : data
		}).then(function(response) {
			var x = response.data;
			if (x['respuesta'] != 'ok') {
				$scope.alerta = false;
			} else {
				window.location = url + "/home"
			}
		})
	}
});