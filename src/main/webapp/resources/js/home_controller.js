var app = angular.module("traficoApp", []);
var url = "/WebTrafico"
app.controller("homeCtrl", function($scope, $http) {
	$scope.loader = true;
	$scope.enLoader = true;
	$scope.enData = true;
	$scope.loader = false;
	$scope.reporte_trafico_input = true;
	$scope.reporte_trafico_output = false;
	$scope.reporte_lag_input = true;
	$scope.reporte_lag_output = false;
	$scope.unidades = false;
	var tab = sessionStorage.getItem("desde")
	if (tab != null) {
		if (tab == "lag") {
			$('#myTab a[href="#profile"]').tab('show')
		} else if (tab == "equipo"){
			$('#myTab a[href="#equipo"]').tab('show')
		} else if (tab == "dashboard"){
			$('#myTab a[href="#contact"]').tab('show')
		} else if (tab == "reporte_enlace") {
			$('#myTab a[href="#reportes"]').tab('show')
		} else if (tab == "reporte_equipo") {
			$('#myTab a[href="#reportes_equipos"]').tab('show')
		} else if (tab == "reporte_lag") {
			$('#myTab a[href="#reportes_lag"]').tab('show')
		} else {
			$('#myTab a[href="#home"]').tab('show')
		}
	}
	var data = {
		busqueda : null
	}
	
	function guardarFiltro() {
		var idnum = $scope.search.ID
		var region = $scope.search.REGION
		var loca = $scope.search.LOCALIDAD
		var ip = $scope.search.IP
		var tipo = $scope.search.TIPO
		
		var idnum2 = $scope.search2.ID
		var region2 = $scope.search2.REGION
		var loca2 = $scope.search2.LOCALIDAD
		var ip2 = $scope.search2.IP
		var tipo2 = $scope.search2.TIPO
		
		var idnum3 = $scope.search3.ID
		var region3 = $scope.search3.REGION
		var loca3 = $scope.search3.LOCALIDAD
		var ip3 = $scope.search3.IP
		var tipo3 = $scope.search3.TIPO
		
		var id_enlace4 = $scope.search4.id_enlace
		var tipo4 = $scope.search4.tipo
		var region_org4 = $scope.search4.region_org
		var localidad_org4 = $scope.search4.localidad_org
		var interfaz_org4 = $scope.search4.interfaz_org
		var lag4 = $scope.search4.lag
		var localidad_dst4 = $scope.search4.localidad_dst
		var interfaz_dst4 = $scope.search4.interfaz_dst
		var dispo4 = $scope.search4.dispo
		var min4 = $scope.search4.min
		var avg4 = $scope.search4.avg
		var max4 = $scope.search4.max
		var stddev4 = $scope.search4.stddev
		var capacidad4 = $scope.search4.capacidad
		var utilizacion24 = $scope.search4.utilizacion2
		
		var id_equipo5 = $scope.search5.id_equipo
		var tipo5 = $scope.search5.tipo
		var region5 = $scope.search5.region
		var loca5 = $scope.search5.localidad
		var hostname5 = $scope.search5.hostname
		var ip5 = $scope.search5.ip
		var dispo5 = $scope.search5.dispo

		sessionStorage.setItem("idnum", idnum)
		sessionStorage.setItem("region", region)
		sessionStorage.setItem("loca", loca)
		sessionStorage.setItem("ip", ip)
		sessionStorage.setItem("tipo", tipo)
		
		sessionStorage.setItem("idnum2", idnum2)
		sessionStorage.setItem("region2", region2)
		sessionStorage.setItem("loca2", loca2)
		sessionStorage.setItem("ip2", ip2)
		sessionStorage.setItem("tipo2", tipo2)
		
		sessionStorage.setItem("idnum3", idnum3)
		sessionStorage.setItem("region3", region3)
		sessionStorage.setItem("loca3", loca3)
		sessionStorage.setItem("ip3", ip3)
		sessionStorage.setItem("tipo3", tipo3)
		
		sessionStorage.setItem("id_enlace4", id_enlace4)
		sessionStorage.setItem("tipo4", tipo4)
		sessionStorage.setItem("region_org4", region_org4)
		sessionStorage.setItem("localidad_org4", localidad_org4)
		sessionStorage.setItem("interfaz_dst4", interfaz_dst4)
		sessionStorage.setItem("lag4", lag4)
		sessionStorage.setItem("localidad_dst4", localidad_dst4)
		sessionStorage.setItem("interfaz_dst4", interfaz_dst4)
		sessionStorage.setItem("dispo4", dispo4)
		sessionStorage.setItem("min4", min4)
		sessionStorage.setItem("avg4", avg4)
		sessionStorage.setItem("max4", max4)
		sessionStorage.setItem("stddev4", stddev4)
		sessionStorage.setItem("capacidad4", capacidad4)
		sessionStorage.setItem("utilizacion24", utilizacion24)

		sessionStorage.setItem("id_equipo5", id_equipo5)
		sessionStorage.setItem("tipo5", tipo5)
		sessionStorage.setItem("region5", region5)
		sessionStorage.setItem("loca5", loca5)
		sessionStorage.setItem("hostname5", hostname5)
		sessionStorage.setItem("ip5", ip5)
		sessionStorage.setItem("dispo5", dispo5)
	}
	
	$scope.getEnlaces = function(id) {
		if (id != null) {
			$scope.enData = true;
			$scope.enLoader = false;
			var data = {
				id : id
			}
			if (sessionStorage.getItem("enlace_id=" + id) !== null) {
				$scope.enLoader = true;
				$scope.enData = false;
				$scope.enlaces = JSON.parse(sessionStorage.getItem("enlace_id="+ id));
			} else {
				$http({
					method : 'POST',
					url : url + "/getEnlaces",
					data : data
				}).then(
						function(response) {
							sessionStorage.setItem("enlace_id=" + id, JSON.stringify(response.data));
							$scope.enLoader = true;
							$scope.enData = false;
							$scope.enlaces = response.data
						})
			}
		}
	}
	$scope.getLags = function(idEquipo) {
		$scope.enData = true;
		$scope.enLoader = false;
		daton = {
			id_equipo : idEquipo
		}
		$http({
			method : 'POST',
			url : url + "/getLags",
			data : daton
		}).then(function(response) {
			$scope.enLoader = true;
			$scope.enData = false;
			$scope.lags = response.data
		});
	}
	function leerFiltro() {
		var idnum = sessionStorage.getItem("idnum")
		var region = sessionStorage.getItem("region")
		var loca = sessionStorage.getItem("loca")
		var ip = sessionStorage.getItem("ip")
		var tipo = sessionStorage.getItem("tipo")
		
		var idnum2 = sessionStorage.getItem("idnum2")
		var region2 = sessionStorage.getItem("region2")
		var loca2 = sessionStorage.getItem("loca2")
		var ip2 = sessionStorage.getItem("ip2")
		var tipo2 = sessionStorage.getItem("tipo2")
		
		var idnum3 = sessionStorage.getItem("idnum3")
		var region3 = sessionStorage.getItem("region3")
		var loca3 = sessionStorage.getItem("loca3")
		var ip3 = sessionStorage.getItem("ip3")
		var tipo3 = sessionStorage.getItem("tipo3")
		
		var id_enlace4 = sessionStorage.getItem("id_enlace4")
		var tipo4 = sessionStorage.getItem("tipo4")
		var region_org4 = sessionStorage.getItem("region_org4")
		var localidad_org4 = sessionStorage.getItem("localidad_org4")
		var interfaz_org4 = sessionStorage.getItem("interfaz_org4")
		var lag4 = sessionStorage.getItem("lag4")
		var localidad_dst4 = sessionStorage.getItem("localidad_dst4")
		var interfaz_dst4 = sessionStorage.getItem("interfaz_dst4")
		var dispo4 = sessionStorage.getItem("dispo4")
		var min4 = sessionStorage.getItem("min4")
		var avg4 = sessionStorage.getItem("avg4")
		var max4 = sessionStorage.getItem("max4")
		var stddev4 = sessionStorage.getItem("stddev4")
		var capacidad4 = sessionStorage.getItem("capacidad4")
		var utilizacion24 = sessionStorage.getItem("utilizacion24")
		
		var id_equipo5 = sessionStorage.getItem("id_equipo5")
		var tipo5 = sessionStorage.getItem("tipo5")
		var region5 = sessionStorage.getItem("region5")
		var loca5 = sessionStorage.getItem("loca5")
		var hostname5 = sessionStorage.getItem("hostname5")
		var ip5 = sessionStorage.getItem("ip5")
		var dispo5 = sessionStorage.getItem("dispo5")
		
		if (idnum == null) {
			idnum = ""
		}
		if (region == null) {
			region = ""
		}
		if (loca == null) {
			loca = ""
		}
		if (ip == null) {
			ip = ""
		}
		if (tipo == null) {
			tipo = ""
		}
		
		if (idnum2 == null) {
			idnum2 = ""
		}
		if (region2 == null) {
			region2 = ""
		}
		if (loca2 == null) {
			loca2 = ""
		}
		if (ip2 == null) {
			ip2 = ""
		}
		if (tipo2 == null) {
			tipo2 = ""
		}
		
		if (idnum3 == null) {
			idnum3 = ""
		}
		if (region3 == null) {
			region3 = ""
		}
		if (loca3 == null) {
			loca3 = ""
		}
		if (ip3 == null) {
			ip3 = ""
		}
		if (tipo3 == null) {
			tipo3 = ""
		}
		
		if (id_enlace4 == null){
			id_enlace4 = ""
		}
		if (tipo4 == null){
			tipo4 = ""
		}
		if (region_org4 == null){
			region_org4 = ""
		}
		if (localidad_org4 == null){
			localidad_org4 = ""
		}
		if (interfaz_org4 == null){
			interfaz_org4 = ""
		}
		if (lag4 == null){
			lag4 = ""
		}
		if (localidad_dst4 == null){
			localidad_dst4 = ""
		}
		if (interfaz_dst4 == null){
			interfaz_dst4 = ""
		}
		if (dispo4 == null){
			dispo4 = ""
		}
		if (min4 == null){
			min4 = ""
		}
		if (avg4 == null){
			avg4 = ""
		}
		if (max4 == null){
			max4 = ""
		}
		if (stddev4 == null){
			stddev4 = ""
		}
		if (capacidad4 == null){
			capacidad4 = ""
		}
		if (utilizacion24 == null){
			utilizacion24 = ""
		}

		
		if (id_equipo5 == null) {
			id_equipo5 = ""
		}
		if (tipo5 == null) {
			tipo5 = ""
		}
		if (region5 == null) {
			region5 = ""	
		}
		if (loca5 == null) {
			loca5 = ""
		}
		if (hostname5 == null) {
			hostname5 = ""
		}
		if (ip5 == null) {
			ip5 = ""
		}
		if (dispo5 == null) {
			dispo5 = ""
		}
		
		$scope.search = {}
		$scope.search.ID = idnum
		$scope.search.REGION = region
		$scope.search.LOCALIDAD = loca
		$scope.search.IP = ip
		$scope.search.TIPO = tipo
		
		$scope.search2 = {}
		$scope.search2.ID = idnum2
		$scope.search2.REGION = region2
		$scope.search2.LOCALIDAD = loca2
		$scope.search2.IP = ip2
		$scope.search2.TIPO = tipo2
		
		$scope.search3 = {}
		$scope.search3.ID = idnum3
		$scope.search3.REGION = region3
		$scope.search3.LOCALIDAD = loca3
		$scope.search3.IP = ip3
		$scope.search3.TIPO = tipo3
		
		$scope.search4 = {}
		$scope.search4.id_enlace = id_enlace4
		$scope.search4.tipo = tipo4 
		$scope.search4.region_org = region_org4 
		$scope.search4.localidad_org = localidad_org4 
		$scope.search4.interfaz_org = interfaz_org4 
		$scope.search4.lag = lag4 
		$scope.search4.localidad_dst = localidad_dst4 
		$scope.search4.interfaz_dst = interfaz_dst4 
		$scope.search4.dispo = dispo4 
		$scope.search4.min = min4 
		$scope.search4.avg = avg4 
		$scope.search4.max = max4 
		$scope.search4.stddev = stddev4 
		$scope.search4.capacidad = capacidad4 
		$scope.search4.utilizacion2 = utilizacion24 
		
		$scope.search5 = {}
		
		if (tab == "lag") {
			$scope.getLags(sessionStorage.getItem("equipo_id"))
		} else {
			$scope.getEnlaces(sessionStorage.getItem("equipo_id"))
		}
		
		setTimeout(function() {
			var ident = sessionStorage.getItem("equipo_id")
			if (tab == "lag") {
				eval("$('#collapse88" + ident + "').collapse('toggle')")
			} else if (tab == "enlaces"){
				eval("$('#collapse" + ident + "').collapse('toggle')")
			} else {
				eval("$('#collapse77" + ident + "').collapse('toggle')")
			}
		}, 500);
		return null
	}
	if (sessionStorage.getItem("equipos_gen=") != null) {
		$scope.equipos = JSON.parse(sessionStorage.getItem("equipos_gen="))
		$scope.loader = true;
		leerFiltro()
	} else {
		$http({
			method : 'POST',
			url : url + "/getEquipos",
			data : data
		}).then(function(response) {
			$scope.equipos = response.data
			sessionStorage.setItem("equipos_gen=", JSON.stringify(response.data))
			$scope.loader = true;
			leerFiltro()
		});
	}
	$scope.irGrafica = function(id_enlace, equipo_id, desde) {
		sessionStorage.setItem("id_enlace", id_enlace);
		sessionStorage.setItem("equipo_id", equipo_id);
		sessionStorage.setItem("desde", desde);
		guardarFiltro()
		window.location = url + "/grafica"
	}
	$scope.irTrafico = function(id_enlace, equipo_id, desde) {
		sessionStorage.setItem("id_enlace", id_enlace);
		sessionStorage.setItem("equipo_id", equipo_id);
		sessionStorage.setItem("desde", desde);
		guardarFiltro()
		window.location = url + "/trafico"
	}
	$scope.irIndisponibilidad = function(id_enlace, equipo_id, desde) {
		sessionStorage.setItem("id_enlace", id_enlace);
		sessionStorage.setItem("equipo_id", equipo_id);
		sessionStorage.setItem("desde", desde);
		guardarFiltro()
		window.location = url + "/disponibilidad"
	}

	$scope.irGraficaLag = function(lag_group, equipo_id, id_lag, desde) {
		sessionStorage.setItem("lag_group", lag_group);
		sessionStorage.setItem("equipo_id", equipo_id);
		sessionStorage.setItem("id_lag", id_lag);
		sessionStorage.setItem("desde", desde);
		guardarFiltro()
		window.location = url + "/graficaLAG"
	}
	$scope.irTraficoLag = function(lag_group, equipo_id, id_lag, desde) {
		sessionStorage.setItem("lag_group", lag_group);
		sessionStorage.setItem("equipo_id", equipo_id);
		sessionStorage.setItem("id_lag", id_lag);
		sessionStorage.setItem("desde", desde);
		guardarFiltro()
		window.location = url + "/traficoLAG"
	}
	$scope.irIndisponibilidadLag = function(lag_group, equipo_id, id_lag, desde) {
		sessionStorage.setItem("lag_group", lag_group);
		sessionStorage.setItem("equipo_id", equipo_id);
		sessionStorage.setItem("id_lag", id_lag);
		sessionStorage.setItem("desde", desde);
		guardarFiltro()
		window.location = url + "/disponibilidadLAG"
	}
	
	$scope.irIndisponibilidadEquipo = function(equipo_id, desde){
		sessionStorage.setItem("equipo_id", equipo_id);
		sessionStorage.setItem("desde", desde);
		guardarFiltro()
		window.location = url + "/disponibilidadEQUIPO"
	}
	
	function getcaidos(){
		$http({
			method : 'POST',
			url : url + "/getCaidos",
			data : data
		}).then(function(response) {
			$scope.caidos = response.data
		});
	}
	$scope.sortBy = function(propertyName) {
	    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
	    $scope.propertyName = propertyName;
	  };
	function getReporteTraficoEnlace(){
		if (sessionStorage.getItem("traf_enla=") != null) {
			$scope.enlaces_reportes = JSON.parse(sessionStorage.getItem("traf_enla="))
		} else {
			$http({
				method : 'POST',
				url : url + "/getReporteTraficoEnlace",
				data : data
			}).then(function(response) {
				sessionStorage.setItem("traf_enla=", JSON.stringify(response.data))
				$scope.enlaces_reportes = response.data
				
			});
		}
	}
	getReporteTraficoEnlace();
	
	function getReporteTraficoLag(){
		if (sessionStorage.getItem("traf_lag=") != null) {
			$scope.lags_reportes = JSON.parse(sessionStorage.getItem("traf_lag="))
			console.log($scope.lags_reportes)
		} else {
			$http({
				method : 'POST',
				url : url + "/getReporteTraficoLag",
				data : data
			}).then(function(response) {
				console.log(response.data)
				sessionStorage.setItem("traf_lag=", JSON.stringify(response.data))
				$scope.lags_reportes = response.data
			});
		}
	}
	getReporteTraficoLag();
	
	$scope.exportTableTrafico = function(output){
		var date = new Date();
		dt = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
		var nombre = "";
		if (output == false) {
			nombre = "reporte_trafico_output_".concat(dt)
			var ExportButtons = document.getElementById('reporte_trafico_output');
			var instance = new TableExport(ExportButtons, {
			    formats: ['xlsx'],
			    fileName: nombre,
			    ignoreRows: 1,
			    exportButtons: false
			});
			var exportData = instance.getExportData()['reporte_trafico_output']['xlsx'];
		    //                   // data          // mime              // name              // extension
		    instance.export2file(exportData.data, exportData.mimeType, nombre, exportData.fileExtension);
		} else {
			nombre = "reporte_trafico_input_".concat(dt)
			var ExportButtons = document.getElementById('reporte_trafico_input');
			var instance = new TableExport(ExportButtons, {
			    formats: ['xlsx'],
			    fileName: nombre,
			    ignoreRows: 1,
			    exportButtons: false
			});
			var exportData = instance.getExportData()['reporte_trafico_input']['xlsx'];
		    //                   // data          // mime              // name              // extension
		    instance.export2file(exportData.data, exportData.mimeType, nombre, exportData.fileExtension);
		}
	}
	$scope.exportTableLag = function(output){
		var date = new Date();
		dt = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
		var nombre = "";
		if (output == false) {
			nombre = "reporte_trafico_lags_output_".concat(dt)
			var ExportButtons = document.getElementById('reporte_lag_output');
			var instance = new TableExport(ExportButtons, {
			    formats: ['xlsx'],
			    fileName: nombre,
			    ignoreRows: 1,
			    exportButtons: false
			});
			var exportData = instance.getExportData()['reporte_lag_output']['xlsx'];
		    //                   // data          // mime              // name              // extension
		    instance.export2file(exportData.data, exportData.mimeType, nombre, exportData.fileExtension);
		} else {
			nombre = "reporte_trafico_lags_input_".concat(dt)
			var ExportButtons = document.getElementById('reporte_lag_input');
			var instance = new TableExport(ExportButtons, {
			    formats: ['xlsx'],
			    fileName: nombre,
			    ignoreRows: 1,
			    exportButtons: false
			});
			var exportData = instance.getExportData()['reporte_lag_input']['xlsx'];
		    //                   // data          // mime              // name              // extension
		    instance.export2file(exportData.data, exportData.mimeType, nombre, exportData.fileExtension);
		}
	}
	$scope.getReporteTraficoEnlaceFechas = function(){
		var dtini = document.getElementById("dtini").value;
		var dtfin = document.getElementById("dtfin").value;
		var tmini = document.getElementById("tmini").value;
		var tmfin = document.getElementById("tmfin").value;
		var data = {
			fecha_ini : dtini + " " + tmini,
			fecha_fin : dtfin + " " + tmfin
		}
		$scope.enlaces_reportes = null;
		$http({
			method : 'POST',
			url : url + "/getReporteTraficoEnlace",
			data : data
		}).then(function(response) {
			$scope.enlaces_reportes = response.data
		});
	}
	$scope.getReporteTraficoLagFechas = function(){
		var dtini = document.getElementById("dtini").value;
		var dtfin = document.getElementById("dtfin").value;
		var tmini = document.getElementById("tmini").value;
		var tmfin = document.getElementById("tmfin").value;
		var data = {
			fecha_ini : dtini + " " + tmini,
			fecha_fin : dtfin + " " + tmfin
		}
		$scope.lags_reportes = null;
		$http({
			method : 'POST',
			url : url + "/getReporteTraficoLag",
			data : data
		}).then(function(response) {
			$scope.lags_reportes = response.data
		});
	}
	$scope.inputTrafic = function(){
		if ($scope.reporte_trafico_input == true){
			$scope.reporte_trafico_input = false;
			$scope.reporte_trafico_output = true;
		} else {
			$scope.reporte_trafico_input = true;
			$scope.reporte_trafico_output = false;
		}
	}
	
	$scope.inputLag = function(){
		if ($scope.reporte_lag_input == true){
			$scope.reporte_lag_input = false;
			$scope.reporte_lag_output = true;
		} else {
			$scope.reporte_lag_input = true;
			$scope.reporte_lag_output = false;
		}
	}
	
	$http({
		method : 'POST',
		url : url + "/getEquipos2",
		data : data
	}).then(function(response) {
		$scope.equipos2 = response.data
		$scope.loader = true;
	});
	
	if (sessionStorage.getItem("report_dispo_eq=") != null) {
		$scope.equipos_reportes = JSON.parse(sessionStorage.getItem("report_dispo_eq="))
	} else {
		$http({
			method : 'POST',
			url : url + "/getReporteDispoEquipos",
			data : data
		}).then(function(response) {
			$scope.equipos_reportes = response.data
			sessionStorage.setItem("report_dispo_eq=",JSON.stringify(response.data))
		});
	}
	
	
	$scope.getReporteEquiposFechas = function(){
		var dtini = document.getElementById("dtini").value;
		var dtfin = document.getElementById("dtfin").value;
		var tmini = document.getElementById("tmini").value;
		var tmfin = document.getElementById("tmfin").value;
		var data = {
			fecha_ini : dtini + " " + tmini,
			fecha_fin : dtfin + " " + tmfin
		}
		$scope.equipos_reportes = null;
		$http({
			method : 'POST',
			url : url + "/getReporteDispoEquipos",
			data : data
		}).then(function(response) {
			$scope.equipos_reportes = response.data
		});
	}
	function dispoEquiposDash(data) {
		if (data == null) {
			data = {};
		}
		$http({
			method : 'POST',
			url : url + "/getdispoEquiposDash",
			data : data
		}).then(function(response) {
			console.log(response.data)
			$scope.conexion = response.data["conexion"]
			$scope.c_conexion = response.data["c_conexion"]
			$scope.c_minutos_conexion = response.data["c_minutos_conexion"]
			$scope.distribuidor = response.data["distribuidor"]
			$scope.c_distribuidor = response.data["c_distribuidor"]
			$scope.c_minutos_distribuidor = response.data["c_minutos_distribuidor"]
			$scope.distribuidor_sr = response.data["distribuidor_sr"]
			$scope.c_distribuidor_sr = response.data["c_distribuidor_sr"]
			$scope.c_minutos_distribuidor_sr = response.data["c_minutos_distribuidor_sr"]
			$scope.distribuidor_rr = response.data["distribuidor_rr"]
			$scope.c_distribuidor_rr = response.data["c_distribuidor_rr"]
			$scope.c_minutos_distribuidor_rr = response.data["c_minutos_distribuidor_rr"]
			$scope.agregador_sr = response.data["agregador_sr"]
			$scope.c_agregador_sr = response.data["c_agregador_sr"]
			$scope.c_minutos_agregador_sr = response.data["c_minutos_agregador_sr"]
			$scope.agregador_rr = response.data["agregador_rr"]
			$scope.c_agregador_rr = response.data["c_agregador_rr"]
			$scope.c_minutos_agregador_rr = response.data["c_minutos_agregador_rr"]
			$scope.core = response.data["core"]
			$scope.c_core = response.data["c_core"]
			$scope.c_minutos_core = response.data["c_minutos_core"]
		});
	}
	dispoEquiposDash();
	
	function dispoEnlasDash(data) {
		if (data == null) {
			data = {};
		}
		$scope.enla_dash = []
		$http({
			method : 'POST',
			url : url + "/getdispoEnlasDash",
			data : data
		}).then(function(response) {
			$scope.enla_dash = response.data

		});
	}
	dispoEnlasDash();
	
	function dispoLagsDash(data) {
		if (data == null) {
			data = {};
		}
		$scope.lag_dash = []
		$http({
			method : 'POST',
			url : url + "/dispoLagsDash",
			data : data
		}).then(function(response) {
			$scope.lag_dash = response.data
		});
	}
	dispoLagsDash();
	
	function trafEnlaDash(data) {
		if (data == null) {
			data = {};
		}
		$scope.traf_core_dash = []
		$http({
			method : 'POST',
			url : url + "/trafEnlaDash",
			data : data
		}).then(function(response) {
			$scope.traf_core_dash = response.data
		});
	}
	trafEnlaDash();
	function trafAgregaDash(data) {
		if (data == null) {
			data = {};
		}
		$scope.traf_agrega_dash = []
		$http({
			method : 'POST',
			url : url + "/trafAgregaDash",
			data : data
		}).then(function(response) {
			$scope.traf_agrega_dash = response.data
		});
	}
	trafAgregaDash();
	
	$scope.getDashboardFechas = function(){
		var dtini = document.getElementById("dtini").value;
		var dtfin = document.getElementById("dtfin").value;
		var tmini = document.getElementById("tmini").value;
		var tmfin = document.getElementById("tmfin").value;
		
		var data = {
			fecha_ini : dtini + " " + tmini,
			fecha_fin : dtfin + " " + tmfin
		}
		dispoEquiposDash(data);
		dispoEnlasDash(data);
		dispoLagsDash(data);
		trafEnlaDash(data);
		trafAgregaDash(data);
	}
	
	$scope.exportTableEquipos = function(){
		var date = new Date();
		dt = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
		var nombre = "";
		nombre = "reporte_equipos".concat(dt)
		var ExportButtons = document.getElementById('reporte_equipos');
		var instance = new TableExport(ExportButtons, {
		    formats: ['xlsx'],
		    fileName: nombre,
		    ignoreRows: 1,
		    exportButtons: false
		});
		var exportData = instance.getExportData()['reporte_equipos']['xlsx'];
	    //                   // data          // mime              // name              // extension
	    instance.export2file(exportData.data, exportData.mimeType, nombre, exportData.fileExtension);
	}
});
