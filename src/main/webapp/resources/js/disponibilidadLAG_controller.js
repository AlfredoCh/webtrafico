var app = angular.module("traficoApp", []);
var url = "/WebTrafico"
app.controller("disponibilidadCtrl", function($scope, $http) {
	$scope.tabla2 = true;
	$scope.filtro = false;
	$scope.btn_filtro = true;
	$scope.equipo_id = sessionStorage.getItem("equipo_id");
	$scope.lag_group = sessionStorage.getItem("lag_group");
	$scope.id_lag = sessionStorage.getItem("id_lag");
	var datita = {
		id_equipo : $scope.equipo_id,
		lag_group : $scope.lag_group,
		id_lag : $scope.id_lag
	}
	$http({
		method : 'POST',
		url : url + "/getEquipo",
		data : datita
	}).then(function(response) {
		$scope.tipo = response.data["tipo"];
	});
	$http({
		method : 'POST',
		url : url + "/getUbigeo",
		data : datita
	}).then(function(response) {
		$scope.localidad = response.data["localidad"];
	});
	$http({
		method : 'POST',
		url : url + "/getDisponibilidad",
		data : datita
	}).then(function(response) {
		$scope.eventos = response.data
		console.log($scope.eventos)
		$scope.loader = true;
	});
	$http({
		method : 'POST',
		url : url + "/getDisponibilidadFechas",
		data : datita
	}).then(function(response) {
		$scope.semana = response.data[0];
		$scope.mes = response.data[1];
		$scope.anio = response.data[2];
		$scope.loader = true;
	});
	$http({
		method : 'POST',
		url : url + "/getAcumuladoTime",
		data : datita
	}).then(function(response) {
		$scope.t_sem = response.data["t_sem"];
		$scope.t_mes = response.data["t_mes"];
		$scope.t_ani = response.data["t_ani"];
	});
	$scope.regresar = function() {
		location.href = "/WebTrafico/home";
	}
	$scope.dispoInterval = function() {
		var dtini = document.getElementById("dtini").value;
		var dtfin = document.getElementById("dtfin").value;
		var tmini = document.getElementById("tmini").value;
		var tmfin = document.getElementById("tmfin").value;
		var data = {
			fecha_ini : dtini + " " + tmini,
			fecha_fin : dtfin + " " + tmfin,
			lag_group : $scope.lag_group,
			id_equipo : $scope.equipo_id,
			id_lag : $scope.id_lag
		}
		console.log(data)
		$http({
			method : 'POST',
			url : url + "/dispoInterval",
			data : data
		}).then(function(response) {
			$scope.dispo = response.data["dispo"];
			$scope.tiempo_acum = response.data["tiempo_acum"];
			$scope.events = response.data["eventos"];
			$scope.filtro = true;
			$scope.btn_filtro = false;
		});
	}
	
	$scope.regresarFiltro = function(){
		$scope.btn_filtro = true;
		$scope.filtro = false;
	}
});
