var app = angular.module("traficoApp", []);
var url = "/WebTrafico"
app.controller("graficaCtrl", function($scope, $http) {
	$scope.tabla1 = true;
	$scope.tabla2 = true;
	$scope.graf1 = true;
	$scope.graf2 = true;
	$scope.grafdrop1 = true;
	$scope.grafdrop2 = true;
	$scope.tabladrop1 = true;
	$scope.tabladrop2 = true;
	$scope.loder = true;
	ini = 0;
	$scope.equipo_id = sessionStorage.getItem("equipo_id");
	$scope.lag_group = sessionStorage.getItem("lag_group");
	$scope.id_lag = sessionStorage.getItem("id_lag");
	var datita = {
		id_equipo : $scope.equipo_id,
		lag_group : $scope.lag_group,
		id_lag : $scope.id_lag
	}
	$http({
		method : 'POST',
		url : url + "/getUbigeo",
		data : datita
	}).then(function(response) {
		$scope.localidad = response.data["localidad"];
	});
	$http({
		method : 'POST',
		url : url + "/getEquipo",
		data : datita
	}).then(function(response) {
		$scope.tipo = response.data["tipo"];
		var data = {
			id_equipo : $scope.equipo_id,
			lag_group : $scope.lag_group,
			tipo : $scope.tipo,
			id_lag : $scope.id_lag
		}
		$http({
			method : 'POST',
			url : url + "/getData",
			data : data
		}).then(function(response) {
			var data = new Array;
			for (var i = 0; i < response.data.length; i++) {
				var dato = response.data[i];
				var cola = {
					type : "scatter",
					mode : "lines",
					name : dato["tipo"],
					fill : 'tozeroy',
					x : dato["x"],
					y : dato["y"]
				}
				data.push(cola);
			}
			if (data.length == 8) {
				var layout = {
					title : 'EGRESS [ '
							+ $scope.tipo
							+ ' ] '
							+ $scope.localidad
							+ ' - LAG GROUP '
							+ $scope.lag_group,
					autosize : true,
					titlefont : {
						size : 14
					},
					legend : {
						orientation : "h",
						font : {
							size : 10
						},
						traceorder : "normal",
						xanchor : "left",
						yanchor : "top"
					},
					xaxis : {
						autorange : true,
						type : 'date'
					},
					yaxis : {
						autorange : true,
						type : 'linear',
						ticksuffix : 'bps',
						exponentformat : 'SI'
					}
				};
				$scope.loader = true;
				$scope.graf1 = false;
				$scope.tabla1 = false;
				Plotly.newPlot('grafica1', data, layout);
			} else {
				dataIngress = data
						.slice(
								0,
								8);

				dataEgress = data
						.slice(
								8,
								17);
				var layoutE = {
					title : 'INGRESS [ '
							+ $scope.tipo
							+ ' ] '
							+ $scope.localidad
							+ ' - LAG GROUP '
							+ $scope.lag_group,
					autosize : true,
					titlefont : {
						size : 14
					},
					legend : {
						orientation : "h",
						font : {
							size : 10
						},
						traceorder : "normal",
						xanchor : "left",
						yanchor : "top"
					},
					xaxis : {
						autorange : true,
						type : 'date'
					},
					yaxis : {
						autorange : true,
						type : 'linear',
						ticksuffix : 'bps',
						exponentformat : 'SI'
					}
				};
				var layoutI = {
					title : 'EGRESS [ '
							+ $scope.tipo
							+ ' ] '
							+ $scope.localidad
							+ ' - LAG GROUP '
							+ $scope.lag_group,
					autosize : true,
					titlefont : {
						size : 14
					},
					legend : {
						orientation : "h",
						font : {
							size : 10
						},
						traceorder : "normal",
						xanchor : "left",
						yanchor : "top"
					},
					xaxis : {
						autorange : true,
						type : 'date'
					},
					yaxis : {
						autorange : true,
						type : 'linear',
						ticksuffix : 'bps',
						exponentformat : 'SI'
					}
				};
				$scope.loader = true;
				$scope.graf2 = false;
				$scope.tabla2 = false;
				Plotly.newPlot('grafica2', dataIngress, layoutE);
				$scope.graf1 = false;
				$scope.tabla1 = false;
				Plotly.newPlot('grafica1', dataEgress, layoutI);
				}
			});
		});
		function getTable(){
			var data = {
					id_lag : $scope.id_lag
			}
			$http({
				method : 'POST',
				url : url + "/getTable",
				data : data
			}).then(function(response) {
				$scope.in_col1_min = response.data["in_col1_min"]
				$scope.in_col1_avg = response.data["in_col1_avg"]
				$scope.in_col1_max = response.data["in_col1_max"]
				$scope.in_col1_stddev = response.data["in_col1_stddev"]

				$scope.in_col2_min = response.data["in_col2_min"]
				$scope.in_col2_avg = response.data["in_col2_avg"]
				$scope.in_col2_max = response.data["in_col2_max"]
				$scope.in_col2_stddev = response.data["in_col2_stddev"]

				$scope.in_col3_min = response.data["in_col3_min"]
				$scope.in_col3_avg = response.data["in_col3_avg"]
				$scope.in_col3_max = response.data["in_col3_max"]
				$scope.in_col3_stddev = response.data["in_col3_stddev"]

				$scope.in_col4_min = response.data["in_col4_min"]
				$scope.in_col4_avg = response.data["in_col4_avg"]
				$scope.in_col4_max = response.data["in_col4_max"]
				$scope.in_col4_stddev = response.data["in_col4_stddev"]

				$scope.in_col5_min = response.data["in_col5_min"]
				$scope.in_col5_avg = response.data["in_col5_avg"]
				$scope.in_col5_max = response.data["in_col5_max"]
				$scope.in_col5_stddev = response.data["in_col5_stddev"]

				$scope.in_col6_min = response.data["in_col6_min"]
				$scope.in_col6_avg = response.data["in_col6_avg"]
				$scope.in_col6_max = response.data["in_col6_max"]
				$scope.in_col6_stddev = response.data["in_col6_stddev"]

				$scope.in_col7_min = response.data["in_col7_min"]
				$scope.in_col7_avg = response.data["in_col7_avg"]
				$scope.in_col7_max = response.data["in_col7_max"]
				$scope.in_col7_stddev = response.data["in_col7_stddev"]

				$scope.in_col8_min = response.data["in_col8_min"]
				$scope.in_col8_avg = response.data["in_col8_avg"]
				$scope.in_col8_max = response.data["in_col8_max"]
				$scope.in_col8_stddev = response.data["in_col8_stddev"]

				$scope.out_col1_min = response.data["out_col1_min"]
				$scope.out_col1_avg = response.data["out_col1_avg"]
				$scope.out_col1_max = response.data["out_col1_max"]
				$scope.out_col1_stddev = response.data["out_col1_stddev"]

				$scope.out_col2_min = response.data["out_col2_min"]
				$scope.out_col2_avg = response.data["out_col2_avg"]
				$scope.out_col2_max = response.data["out_col2_max"]
				$scope.out_col2_stddev = response.data["out_col2_stddev"]

				$scope.out_col3_min = response.data["out_col3_min"]
				$scope.out_col3_avg = response.data["out_col3_avg"]
				$scope.out_col3_max = response.data["out_col3_max"]
				$scope.out_col3_stddev = response.data["out_col3_stddev"]

				$scope.out_col4_min = response.data["out_col4_min"]
				$scope.out_col4_avg = response.data["out_col4_avg"]
				$scope.out_col4_max = response.data["out_col4_max"]
				$scope.out_col4_stddev = response.data["out_col4_stddev"]

				$scope.out_col5_min = response.data["out_col5_min"]
				$scope.out_col5_avg = response.data["out_col5_avg"]
				$scope.out_col5_max = response.data["out_col5_max"]
				$scope.out_col5_stddev = response.data["out_col5_stddev"]

				$scope.out_col6_min = response.data["out_col6_min"]
				$scope.out_col6_avg = response.data["out_col6_avg"]
				$scope.out_col6_max = response.data["out_col6_max"]
				$scope.out_col6_stddev = response.data["out_col6_stddev"]

				$scope.out_col7_min = response.data["out_col7_min"]
				$scope.out_col7_avg = response.data["out_col7_avg"]
				$scope.out_col7_max = response.data["out_col7_max"]
				$scope.out_col7_stddev = response.data["out_col7_stddev"]

				$scope.out_col8_min = response.data["out_col8_min"]
				$scope.out_col8_avg = response.data["out_col8_avg"]
				$scope.out_col8_max = response.data["out_col8_max"]
				$scope.out_col8_stddev = response.data["out_col8_stddev"]
				
				$scope.out_drop1_min = response.data["out_drop1_min"]
				$scope.out_drop1_avg = response.data["out_drop1_avg"]
				$scope.out_drop1_max = response.data["out_drop1_max"]
				$scope.out_drop1_stddev = response.data["out_drop1_stddev"]

				$scope.out_drop2_min = response.data["out_drop2_min"]
				$scope.out_drop2_avg = response.data["out_drop2_avg"]
				$scope.out_drop2_max = response.data["out_drop2_max"]
				$scope.out_drop2_stddev = response.data["out_drop2_stddev"]

				$scope.out_drop3_min = response.data["out_drop3_min"]
				$scope.out_drop3_avg = response.data["out_drop3_avg"]
				$scope.out_drop3_max = response.data["out_drop3_max"]
				$scope.out_drop3_stddev = response.data["out_drop3_stddev"]

				$scope.out_drop4_min = response.data["out_drop4_min"]
				$scope.out_drop4_avg = response.data["out_drop4_avg"]
				$scope.out_drop4_max = response.data["out_drop4_max"]
				$scope.out_drop4_stddev = response.data["out_drop4_stddev"]

				$scope.out_drop5_min = response.data["out_drop5_min"]
				$scope.out_drop5_avg = response.data["out_drop5_avg"]
				$scope.out_drop5_max = response.data["out_drop5_max"]
				$scope.out_drop5_stddev = response.data["out_drop5_stddev"]

				$scope.out_drop6_min = response.data["out_drop6_min"]
				$scope.out_drop6_avg = response.data["out_drop6_avg"]
				$scope.out_drop6_max = response.data["out_drop6_max"]
				$scope.out_drop6_stddev = response.data["out_drop6_stddev"]

				$scope.out_drop7_min = response.data["out_drop7_min"]
				$scope.out_drop7_avg = response.data["out_drop7_avg"]
				$scope.out_drop7_max = response.data["out_drop7_max"]
				$scope.out_drop7_stddev = response.data["out_drop7_stddev"]

				$scope.out_drop8_min = response.data["out_drop8_min"]
				$scope.out_drop8_avg = response.data["out_drop8_avg"]
				$scope.out_drop8_max = response.data["out_drop8_max"]
				$scope.out_drop8_stddev = response.data["out_drop8_stddev"]
				
				
				$scope.in_drop1_min = response.data["in_drop1_min"]
				$scope.in_drop1_avg = response.data["in_drop1_avg"]
				$scope.in_drop1_max = response.data["in_drop1_max"]
				$scope.in_drop1_stddev = response.data["in_drop1_stddev"]

				$scope.in_drop2_min = response.data["in_drop2_min"]
				$scope.in_drop2_avg = response.data["in_drop2_avg"]
				$scope.in_drop2_max = response.data["in_drop2_max"]
				$scope.in_drop2_stddev = response.data["in_drop2_stddev"]

				$scope.in_drop3_min = response.data["in_drop3_min"]
				$scope.in_drop3_avg = response.data["in_drop3_avg"]
				$scope.in_drop3_max = response.data["in_drop3_max"]
				$scope.in_drop3_stddev = response.data["in_drop3_stddev"]

				$scope.in_drop4_min = response.data["in_drop4_min"]
				$scope.in_drop4_avg = response.data["in_drop4_avg"]
				$scope.in_drop4_max = response.data["in_drop4_max"]
				$scope.in_drop4_stddev = response.data["in_drop4_stddev"]

				$scope.in_drop5_min = response.data["in_drop5_min"]
				$scope.in_drop5_avg = response.data["in_drop5_avg"]
				$scope.in_drop5_max = response.data["in_drop5_max"]
				$scope.in_drop5_stddev = response.data["in_drop5_stddev"]

				$scope.in_drop6_min = response.data["in_drop6_min"]
				$scope.in_drop6_avg = response.data["in_drop6_avg"]
				$scope.in_drop6_max = response.data["in_drop6_max"]
				$scope.in_drop6_stddev = response.data["in_drop6_stddev"]

				$scope.in_drop7_min = response.data["in_drop7_min"]
				$scope.in_drop7_avg = response.data["in_drop7_avg"]
				$scope.in_drop7_max = response.data["in_drop7_max"]
				$scope.in_drop7_stddev = response.data["in_drop7_stddev"]

				$scope.in_drop8_min = response.data["in_drop8_min"]
				$scope.in_drop8_avg = response.data["in_drop8_avg"]
				$scope.in_drop8_max = response.data["in_drop8_max"]
				$scope.in_drop8_stddev = response.data["in_drop8_stddev"]
			});
		}		
		getTable();
										
		$scope.estadisticas = function() {
			ini = ini + 1;
			$scope.loader = false;
			$scope.graf1 = true;
			$scope.graf2 = true;
			$scope.tabla1 = true;
			$scope.tabla2 = true;
			var dtini = document
					.getElementById("dtini").value;
			var dtfin = document
					.getElementById("dtfin").value;
			var tmini = document
					.getElementById("tmini").value;
			var tmfin = document
					.getElementById("tmfin").value;
			var data = {
				fecha_ini : dtini + " " + tmini,
				fecha_fin : dtfin + " " + tmfin,
				lag_group : $scope.lag_group,
				id_equipo : $scope.equipo_id,
				id_lag : $scope.id_lag
			}
			$http(
					{
						method : 'POST',
						url : url
								+ "/getEstadisticasColas",
						data : data
					}).then(function(response) {
						console.log(response.data)
						$scope.in_col1_min = response.data["in_col1_min"]
						$scope.in_col1_avg = response.data["in_col1_avg"]
						$scope.in_col1_max = response.data["in_col1_max"]
						$scope.in_col1_stddev = response.data["in_col1_stddev"]
	
						$scope.in_col2_min = response.data["in_col2_min"]
						$scope.in_col2_avg = response.data["in_col2_avg"]
						$scope.in_col2_max = response.data["in_col2_max"]
						$scope.in_col2_stddev = response.data["in_col2_stddev"]
	
						$scope.in_col3_min = response.data["in_col3_min"]
						$scope.in_col3_avg = response.data["in_col3_avg"]
						$scope.in_col3_max = response.data["in_col3_max"]
						$scope.in_col3_stddev = response.data["in_col3_stddev"]
	
						$scope.in_col4_min = response.data["in_col4_min"]
						$scope.in_col4_avg = response.data["in_col4_avg"]
						$scope.in_col4_max = response.data["in_col4_max"]
						$scope.in_col4_stddev = response.data["in_col4_stddev"]
	
						$scope.in_col5_min = response.data["in_col5_min"]
						$scope.in_col5_avg = response.data["in_col5_avg"]
						$scope.in_col5_max = response.data["in_col5_max"]
						$scope.in_col5_stddev = response.data["in_col5_stddev"]
	
						$scope.in_col6_min = response.data["in_col6_min"]
						$scope.in_col6_avg = response.data["in_col6_avg"]
						$scope.in_col6_max = response.data["in_col6_max"]
						$scope.in_col6_stddev = response.data["in_col6_stddev"]
	
						$scope.in_col7_min = response.data["in_col7_min"]
						$scope.in_col7_avg = response.data["in_col7_avg"]
						$scope.in_col7_max = response.data["in_col7_max"]
						$scope.in_col7_stddev = response.data["in_col7_stddev"]
	
						$scope.in_col8_min = response.data["in_col8_min"]
						$scope.in_col8_avg = response.data["in_col8_avg"]
						$scope.in_col8_max = response.data["in_col8_max"]
						$scope.in_col8_stddev = response.data["in_col8_stddev"]
	
						$scope.out_col1_min = response.data["out_col1_min"]
						$scope.out_col1_avg = response.data["out_col1_avg"]
						$scope.out_col1_max = response.data["out_col1_max"]
						$scope.out_col1_stddev = response.data["out_col1_stddev"]
	
						$scope.out_col2_min = response.data["out_col2_min"]
						$scope.out_col2_avg = response.data["out_col2_avg"]
						$scope.out_col2_max = response.data["out_col2_max"]
						$scope.out_col2_stddev = response.data["out_col2_stddev"]
	
						$scope.out_col3_min = response.data["out_col3_min"]
						$scope.out_col3_avg = response.data["out_col3_avg"]
						$scope.out_col3_max = response.data["out_col3_max"]
						$scope.out_col3_stddev = response.data["out_col3_stddev"]
	
						$scope.out_col4_min = response.data["out_col4_min"]
						$scope.out_col4_avg = response.data["out_col4_avg"]
						$scope.out_col4_max = response.data["out_col4_max"]
						$scope.out_col4_stddev = response.data["out_col4_stddev"]
	
						$scope.out_col5_min = response.data["out_col5_min"]
						$scope.out_col5_avg = response.data["out_col5_avg"]
						$scope.out_col5_max = response.data["out_col5_max"]
						$scope.out_col5_stddev = response.data["out_col5_stddev"]
	
						$scope.out_col6_min = response.data["out_col6_min"]
						$scope.out_col6_avg = response.data["out_col6_avg"]
						$scope.out_col6_max = response.data["out_col6_max"]
						$scope.out_col6_stddev = response.data["out_col6_stddev"]
	
						$scope.out_col7_min = response.data["out_col7_min"]
						$scope.out_col7_avg = response.data["out_col7_avg"]
						$scope.out_col7_max = response.data["out_col7_max"]
						$scope.out_col7_stddev = response.data["out_col7_stddev"]
	
						$scope.out_col8_min = response.data["out_col8_min"]
						$scope.out_col8_avg = response.data["out_col8_avg"]
						$scope.out_col8_max = response.data["out_col8_max"]
						$scope.out_col8_stddev = response.data["out_col8_stddev"]
						
						$scope.out_drop1_min = response.data["out_drop1_min"]
						$scope.out_drop1_avg = response.data["out_drop1_avg"]
						$scope.out_drop1_max = response.data["out_drop1_max"]
						$scope.out_drop1_stddev = response.data["out_drop1_stddev"]
	
						$scope.out_drop2_min = response.data["out_drop2_min"]
						$scope.out_drop2_avg = response.data["out_drop2_avg"]
						$scope.out_drop2_max = response.data["out_drop2_max"]
						$scope.out_drop2_stddev = response.data["out_drop2_stddev"]
	
						$scope.out_drop3_min = response.data["out_drop3_min"]
						$scope.out_drop3_avg = response.data["out_drop3_avg"]
						$scope.out_drop3_max = response.data["out_drop3_max"]
						$scope.out_drop3_stddev = response.data["out_drop3_stddev"]
	
						$scope.out_drop4_min = response.data["out_drop4_min"]
						$scope.out_drop4_avg = response.data["out_drop4_avg"]
						$scope.out_drop4_max = response.data["out_drop4_max"]
						$scope.out_drop4_stddev = response.data["out_drop4_stddev"]
	
						$scope.out_drop5_min = response.data["out_drop5_min"]
						$scope.out_drop5_avg = response.data["out_drop5_avg"]
						$scope.out_drop5_max = response.data["out_drop5_max"]
						$scope.out_drop5_stddev = response.data["out_drop5_stddev"]
	
						$scope.out_drop6_min = response.data["out_drop6_min"]
						$scope.out_drop6_avg = response.data["out_drop6_avg"]
						$scope.out_drop6_max = response.data["out_drop6_max"]
						$scope.out_drop6_stddev = response.data["out_drop6_stddev"]
	
						$scope.out_drop7_min = response.data["out_drop7_min"]
						$scope.out_drop7_avg = response.data["out_drop7_avg"]
						$scope.out_drop7_max = response.data["out_drop7_max"]
						$scope.out_drop7_stddev = response.data["out_drop7_stddev"]
	
						$scope.out_drop8_min = response.data["out_drop8_min"]
						$scope.out_drop8_avg = response.data["out_drop8_avg"]
						$scope.out_drop8_max = response.data["out_drop8_max"]
						$scope.out_drop8_stddev = response.data["out_drop8_stddev"]
						
						
						$scope.in_drop1_min = response.data["in_drop1_min"]
						$scope.in_drop1_avg = response.data["in_drop1_avg"]
						$scope.in_drop1_max = response.data["in_drop1_max"]
						$scope.in_drop1_stddev = response.data["in_drop1_stddev"]
	
						$scope.in_drop2_min = response.data["in_drop2_min"]
						$scope.in_drop2_avg = response.data["in_drop2_avg"]
						$scope.in_drop2_max = response.data["in_drop2_max"]
						$scope.in_drop2_stddev = response.data["in_drop2_stddev"]
	
						$scope.in_drop3_min = response.data["in_drop3_min"]
						$scope.in_drop3_avg = response.data["in_drop3_avg"]
						$scope.in_drop3_max = response.data["in_drop3_max"]
						$scope.in_drop3_stddev = response.data["in_drop3_stddev"]
	
						$scope.in_drop4_min = response.data["in_drop4_min"]
						$scope.in_drop4_avg = response.data["in_drop4_avg"]
						$scope.in_drop4_max = response.data["in_drop4_max"]
						$scope.in_drop4_stddev = response.data["in_drop4_stddev"]
	
						$scope.in_drop5_min = response.data["in_drop5_min"]
						$scope.in_drop5_avg = response.data["in_drop5_avg"]
						$scope.in_drop5_max = response.data["in_drop5_max"]
						$scope.in_drop5_stddev = response.data["in_drop5_stddev"]
	
						$scope.in_drop6_min = response.data["in_drop6_min"]
						$scope.in_drop6_avg = response.data["in_drop6_avg"]
						$scope.in_drop6_max = response.data["in_drop6_max"]
						$scope.in_drop6_stddev = response.data["in_drop6_stddev"]
	
						$scope.in_drop7_min = response.data["in_drop7_min"]
						$scope.in_drop7_avg = response.data["in_drop7_avg"]
						$scope.in_drop7_max = response.data["in_drop7_max"]
						$scope.in_drop7_stddev = response.data["in_drop7_stddev"]
	
						$scope.in_drop8_min = response.data["in_drop8_min"]
						$scope.in_drop8_avg = response.data["in_drop8_avg"]
						$scope.in_drop8_max = response.data["in_drop8_max"]
						$scope.in_drop8_stddev = response.data["in_drop8_stddev"]
					});
					var data = {
						lag_group : $scope.lag_group,
						id_equipo : $scope.equipo_id,
						fecha_ini : dtini + " " + tmini,
						fecha_fin : dtfin + " " + tmfin,
						id_lag : $scope.id_lag
					}
					$http({
								method : 'POST',
								url : url + "/getDataTraficoEstadColas",
								data : data
							}).then(function(response) {
								var dato = response.data;
								dataIngress = dato
										.slice(
												0,
												8);
								dataEgress = dato
										.slice(
												8,
												17);
								var dat2 = [];
								for (var i = 0; i < dataEgress.length; i++) {
									var temp = {
										type : "scatter",
										mode : "lines",
										name : dato[i + 8]["tittle"],
										fill : 'tozeroy',
										x : dato[i + 8]["x"],
										y : dato[i + 8]["y"]
									}

									dat2.push(temp);
								}
								var layout2 = {
									title : 'EGRESS [ '
											+ $scope.tipo
											+ ' ] '
											+ $scope.localidad
											+ ' - LAG GROUP '
											+ $scope.lag_group,
									autosize : true,
									titlefont : {
										size : 14
									},
									legend : {
										orientation : "h",
										font : {
											size : 10
										},
										traceorder : "normal",
										xanchor : "left",
										yanchor : "top"
									},
									xaxis : {
										fixedrange : true,
										type : 'date'
									},
									yaxis : {
										fixedrange : true,
										type : 'spline',
										ticksuffix : 'bps',
										exponentformat : 'SI'
									}
								};							
								var dat3 = [];
								for (var i = 0; i < dataIngress.length; i++) {
									var temp = {
										type : "scatter",
										mode : "lines",
										name : dato[i]["tittle"],
										fill : 'tozeroy',
										x : dato[i]["x"],
										y : dato[i]["y"]
									}

									dat3.push(temp);
								}
								var layout3 = {
									title : 'EGRESS [ '
											+ $scope.tipo
											+ ' ] '
											+ $scope.localidad
											+ ' - LAG GROUP '
											+ $scope.lag_group,
									autosize : true,
									titlefont : {
										size : 14
									},
									legend : {
										orientation : "h",
										font : {
											size : 10
										},
										traceorder : "normal",
										xanchor : "left",
										yanchor : "top"
									},
									xaxis : {
										fixedrange : true,
										type : 'date'
									},
									yaxis : {
										fixedrange : true,
										type : 'spline',
										ticksuffix : 'bps',
										exponentformat : 'SI'
									}
								};						
								$scope.graf1 = false;
								$scope.tabla1 = false;
								$scope.loader = true;
								Plotly.newPlot('grafica1', dat2, layout2);
								if ($scope.tipo == 'AGREGADOR' || $scope.tipo == 'CORE') {
									$scope.graf2 = false;
									$scope.tabla2 = false;
								}
								Plotly.newPlot('grafica2', dat3, layout3);
							});
							$http({
								method : 'POST',
								url : url + "/getDataDropsColas",
								data : data
							}).then(function(response) {
								var dato = response.data;
								dataIngress = dato.slice(0, 8);
								dataEgress = dato.slice(8, 17);
								var dat2 = [];
								for (var i = 0; i < dataEgress.length; i++) {
									var temp = {
										type : "scatter",
										mode : "lines",
										name : dato[i + 8]["tittle"],
										fill : 'tozeroy',
										x : dato[i + 8]["x"],
										y : dato[i + 8]["y"]
									}
									dat2.push(temp);
								}
								var layout2 = {
										title : 'DROP EGRESS [ '
											+ $scope.tipo
											+ ' ] '
											+ $scope.localidad
											+ ' - LAG GROUP '
											+ $scope.lag_group,
									autosize : true,
									titlefont : {
										size : 14
									},
									legend : {
										orientation : "h",
										font : {
											size : 10
										},
										traceorder : "normal",
										xanchor : "left",
										yanchor : "top"
									},
									xaxis : {
										fixedrange : true,
										type : 'date'
									},
									yaxis : {
										fixedrange : true,
										type : 'spline',
										ticksuffix : 'B/s',
										exponentformat : 'SI'
									}
								};
								
								var dat3 = [];
								for (var i = 0; i < dataIngress.length; i++) {
									var temp = {
										type : "scatter",
										mode : "lines",
										name : dato[i]["tittle"],
										fill : 'tozeroy',
										x : dato[i]["x"],
										y : dato[i]["y"]
									}
									dat3.push(temp);
								}
								var layout3 = {
										title : 'DROP INGRESS [ '
											+ $scope.tipo
											+ ' ] '
											+ $scope.localidad
											+ ' - LAG GROUP '
											+ $scope.lag_group,
									autosize : true,
									titlefont : {
										size : 14
									},
									legend : {
										orientation : "h",
										font : {
											size : 10
										},
										traceorder : "normal",
										xanchor : "left",
										yanchor : "top"
									},
									xaxis : {
										fixedrange : true,
										type : 'date'
									},
									yaxis : {
										fixedrange : true,
										type : 'spline',
										ticksuffix : 'B/s',
										exponentformat : 'SI'
									}
								};
								$scope.loader = true;
								Plotly.newPlot('graficadrops2', dat3, layout3);
								Plotly.newPlot('graficadrops1', dat2, layout2);
							});
						}
						$scope.verDrops = function() {
							if ($scope.grafdrop1 == true) {
								$scope.graf1 = true;
								$scope.tabla1 = true;
								$scope.graf2 = true;
								$scope.tabla2 = true;
								
								if (ini == 0){
									$scope.loder = false;
									data = {
										id_lag : $scope.id_lag,
										tipo : $scope.tipo
									}
									console.log("ini = 0")
									$http({
										method : 'POST',
										url : url + "/getDrops",
										data : data
									}).then(function(response) {
										console.log(response.data)
										var data = new Array;
										for (var i = 0; i < response.data.length; i++) {
											var dato = response.data[i];
											var cola = {
												type : "scatter",
												mode : "lines",
												name : dato["tipo"],
												fill : 'tozeroy',
												x : dato["x"],
												y : dato["y"]
											}
											data.push(cola);
										}
										if (data.length == 8) {
											var layout = {
												title : 'DROP EGRESS [ '
														+ $scope.tipo
														+ ' ] '
														+ $scope.localidad
														+ ' - LAG GROUP '
														+ $scope.lag_group,
												titlefont : {
													size : 14
												},
												legend : {
													orientation : "h",
													font : {
														size : 10
													},
													traceorder : "normal",
													xanchor : "left",
													yanchor : "top"
												},
												autosize : true,
												xaxis : {
													autorange : true,
													type : 'date',
												},
												yaxis : {
													autorange : true,
													type : 'linear',
													ticksuffix : 'B/s',
													exponentformat : 'SI'
												}
											};
											$scope.loader = true;
											Plotly.newPlot('graficadrops1', data, layout);
											$scope.grafdrop1 = false;
											$scope.tabladrop1 = false;
										} else {
											dataIngress = data.slice(0,8);
											dataEgress = data.slice(8,17);
											var layoutE = {
												title : 'DROP INGRESS [ '
														+ $scope.tipo
														+ ' ] '
														+ $scope.localidad
														+ ' - LAG GROUP '
														+ $scope.lag_group,
												autosize : true,
												titlefont : {
													size : 14
												},
												legend : {
													orientation : "h",
													font : {
														size : 10
													},
													traceorder : "normal",
													xanchor : "left",
													yanchor : "top"
												},
												xaxis : {
													autorange : true,
													type : 'date'
												},
												yaxis : {
													autorange : true,
													type : 'linear',
													ticksuffix : 'B/s',
													exponentformat : 'SI'
												}
											};
											var layoutI = {
												title : 'DROP EGRESS [ '
														+ $scope.tipo
														+ ' ] '
														+ $scope.localidad
														+ ' - LAG GROUP '
														+ $scope.lag_group,
												autosize : true,
												titlefont : {
													size : 14
												},
												legend : {
													orientation : "h",
													font : {
														size : 10
													},
													traceorder : "normal",
													xanchor : "left",
													yanchor : "top"
												},
												xaxis : {
													autorange : true,
													type : 'date'
												},
												yaxis : {
													autorange : true,
													type : 'linear',
													ticksuffix : 'B/s',
													exponentformat : 'SI'
												}
											};
											$scope.loader = true;
											Plotly.newPlot('graficadrops2', dataIngress, layoutE);
											$scope.grafdrop2 = false;
											$scope.tabladrop2 = false;
											Plotly.newPlot('graficadrops1', dataEgress, layoutI);
											$scope.grafdrop1 = false;
											$scope.tabladrop1 = false;
										}
									})
								} else {
									if ($scope.tipo == 'AGREGADOR' || $scope.tipo == 'CORE'){
										$scope.graf1 = true;
										$scope.tabla1 = true;;
										$scope.graf2 = true;
										$scope.tabla2 = true;
										$scope.grafdrop1 = false;
										$scope.tabladrop1 = false;
										$scope.grafdrop2 = false;
										$scope.tabladrop2 = false;
									} else {
										$scope.graf1 = true;
										$scope.tabla1 = true;;
										$scope.grafdrop1 = false;
										$scope.tabladrop1 = false;
									}
								}
							} else {
								$scope.grafdrop1 = true;
								$scope.tabladrop1 = true;
								$scope.grafdrop2 = true;
								$scope.tabladrop2 = true;
								
								$scope.graf1 = false;
								$scope.tabla1 = false;
								if ($scope.tipo == 'AGREGADOR' || $scope.tipo == 'CORE'){
									$scope.graf2 = false;
									$scope.tabla2 = false;
								}
							}
							ini = ini + 1;
				}
				$scope.regresar = function() {
					location.href = "/WebTrafico/home";
				}
			});
