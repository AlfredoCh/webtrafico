var app = angular.module("traficoApp", []);
var dat
var layout2
var url = "/WebTrafico"
app.controller("graficaCtrl", function($scope, $http) {
	$scope.tabla1 = true;
	$scope.graf1 = true;
	$scope.loader = false;
	$scope.equipo_id = sessionStorage.getItem("equipo_id");
	$scope.lag_group = sessionStorage.getItem("lag_group");
	$scope.id_lag = sessionStorage.getItem("id_lag");
	var datita = {
		id_equipo : $scope.equipo_id,
		lag_group : $scope.lag_group,
		id_lag : $scope.id_lag
	}
	$http({
		method : 'POST',
		url : url + "/getUbigeo",
		data : datita
	}).then(function(response) {
		$scope.localidad = response.data["localidad"];
		$scope.tabla2 = false;
	});
	$http({
		method : 'POST',
		url : url + "/getEquipo",
		data : datita
	}).then(function(response) {
		$scope.tipo = response.data["tipo"];
	});
	$http({
		method : 'POST',
		url : url + "/getDataTrafico",
		data : datita
	}).then(
			function(response) {
				var dato = response.data;
				var traOUT = {
					type : "scatter",
					mode : "lines",
					name : "OUTPUT",
					fill : 'tozeroy',
					x : dato[0]["fecha"],
					y : dato[0]["trafico"]
				}
				var traIN = {
					type : "scatter",
					mode : "lines",
					name : "INPUT",
					fill : 'tozeroy',
					x : dato[1]["fecha"],
					y : dato[1]["trafico_in"]
				}
				dat = [ traOUT, traIN ];
				layout = {
					title : 'TRAFICO [ ' + $scope.tipo + ' ] '
							+ $scope.localidad + ' - LAG GROUP '
							+ $scope.lag_group,
					autosize : true,
					titlefont : {
						size : 14
					},
					legend : {
						orientation : "h",
						font : {
							size : 10
						},
						traceorder : "normal",
						xanchor : "left",
						yanchor : "top"
					},
					xaxis : {
						autorange : true,
						type : 'date'
					},
					yaxis : {
						autorange : true,
						type : 'spline',
						ticksuffix : 'bps',
						exponentformat : 'SI'
					}
				};
				$scope.loader = true;
				Plotly.newPlot('grafica1', dat, layout)
				$scope.graf1 = false;
				$scope.tabla1 = false;
			});
	function getTable(){
		var data = {
				id_lag : $scope.id_lag
		}
		$http({
			method : 'POST',
			url : url + "/getTableTrafico",
			data : data
		}).then(function(response) {
			$scope.min = response.data["min"]
			$scope.avg = response.data["avg"]
			$scope.max = response.data["max"]
			$scope.stddev = response.data["stddev"]
			$scope.min_in = response.data["min_in"]
			$scope.avg_in = response.data["avg_in"]
			$scope.max_in = response.data["max_in"]
			$scope.stddev_in = response.data["stddev_in"]
		});
	}
	getTable();
	
	$scope.estadisticas = function() {
		$scope.loader = false;
		var dtini = document.getElementById("dtini").value;
		var dtfin = document.getElementById("dtfin").value;
		var tmini = document.getElementById("tmini").value;
		var tmfin = document.getElementById("tmfin").value;
		var data = {
			fecha_ini : dtini + " " + tmini,
			fecha_fin : dtfin + " " + tmfin,
			lag_group : $scope.lag_group,
			id_equipo : $scope.equipo_id,
			id_lag : $scope.id_lag
		}
		$http({
			method : 'POST',
			url : url + "/getEstadisticas",
			data : data
		}).then(function(response) {
			console.log(response.data)
			$scope.min = response.data["min"]
			$scope.avg = response.data["avg"]
			$scope.max = response.data["max"]
			$scope.stddev = response.data["stddev"]
			$scope.min_in = response.data["min_in"]
			$scope.avg_in = response.data["avg_in"]
			$scope.max_in = response.data["max_in"]
			$scope.stddev_in = response.data["stddev_in"]
		});
		var data = {
			lag_group : $scope.lag_group,
			id_equipo : $scope.equipo_id,
			tipo : $scope.tipo,
			fecha_ini : dtini + " " + tmini,
			fecha_fin : dtfin + " " + tmfin,
			id_lag : $scope.id_lag
		}
		$http({
			method : 'POST',
			url : url + "/getDataTraficoEstad",
			data : data
		}).then(
				function(response) {
					var dato = response.data;
					var traOUT = {
						type : "scatter",
						mode : "lines",
						name : "OUTPUT",
						fill : 'tozeroy',
						x : dato[0]["fecha"],
						y : dato[0]["trafico"]
					}
					var traIN = {
						type : "scatter",
						mode : "lines",
						name : "INPUT",
						fill : 'tozeroy',
						x : dato[1]["fecha"],
						y : dato[1]["trafico_in"]
					}
					var dat2 = [ traOUT, traIN ];
					var layout2 = {
						title : 'TRAFICO [ ' + $scope.tipo + ' ] '
								+ $scope.localidad + '- LAG GROUP '
								+ $scope.lag_group,
						autosize : true,
						titlefont : {
							size : 14
						},
						legend : {
							orientation : "h",
							font : {
								size : 10
							},
							traceorder : "normal",
							xanchor : "left",
							yanchor : "top"
						},
						xaxis : {
							fixedrange : true,
							type : 'date'
						},
						yaxis : {
							fixedrange : true,
							type : 'spline',
							ticksuffix : 'bps',
							exponentformat : 'SI'
						}
					};
					$scope.loader = true;
					Plotly.newPlot('grafica1', dat2, layout2);
				});
	}
	$scope.graphback = function() {
		if (!$scope.grafestad1) {
			$scope.graf1 = false;
		} else {
			$scope.grafestad1 = false;
		}
	}
	$scope.regresar = function() {
		location.href = "/WebTrafico/home";
	}
});
