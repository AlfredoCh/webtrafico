package com.pe.acp.webtraffic.model;

public class Registro {
	 
	  private String fecha;
	  private Float avgrt;
	  
	  public Registro(String fecha, Float avgrt) {
		this.fecha = fecha;
		this.avgrt = avgrt;
	  }
		
		public String getFecha() {
			return fecha;
		}
		
		public void setFecha(String fecha) {
			this.fecha = fecha;
		}
		
		public Float getAvgrt() {
			return avgrt;
		}
		
		public void setAvgrt(Float avgrt) {
			this.avgrt = avgrt;
		}
	}