package com.pe.acp.webtraffic.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.pe.acp.webtraffic.dao.DAO;

@CrossOrigin
@RestController
public class CustomerRestController {

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(Model model) {
		return new ModelAndView("login");
	}
	
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView home(Model model) {
		return new ModelAndView("home");
	}

	@RequestMapping(value = "/grafica", method = RequestMethod.GET)
	public ModelAndView grafica(Model model) {
		return new ModelAndView("grafica");
	}

	@RequestMapping(value = "/graficaLAG", method = RequestMethod.GET)
	public ModelAndView graficaLAG(Model model) {
		return new ModelAndView("graficaLAG");
	}

	@RequestMapping(value = "/trafico", method = RequestMethod.GET)
	public ModelAndView trafico(Model model) {
		return new ModelAndView("trafico");
	}

	@RequestMapping(value = "/traficoLAG", method = RequestMethod.GET)
	public ModelAndView traficoLAG(Model model) {
		return new ModelAndView("traficoLAG");
	}

	@RequestMapping(value = "/disponibilidad", method = RequestMethod.GET)
	public ModelAndView disponibilidad(Model model) {
		return new ModelAndView("disponibilidad");
	}

	@RequestMapping(value = "/disponibilidadLAG", method = RequestMethod.GET)
	public ModelAndView disponibilidadLAG(Model model) {
		return new ModelAndView("disponibilidadLAG");
	}
	
	@RequestMapping(value = "/disponibilidadEQUIPO", method = RequestMethod.GET)
	public ModelAndView disponibilidadEQUIPO(Model model) {
		return new ModelAndView("disponibilidadEQUIPO");
	}
	
	@CrossOrigin
	@PostMapping(value = "/login")
	public @ResponseBody Map<String, Object> login(@RequestBody Map<String, Object> payload) {
		Map<String, Object> rpta = new HashMap<String, Object>();
		if (DAO.log_in(String.valueOf(payload.get("user")), String.valueOf(payload.get("pass")))) {
			rpta.put("respuesta", "ok");
			return rpta;
		} else {
			rpta.put("respuesta", "failed");
			return rpta;
		}
	}

	@CrossOrigin
	@PostMapping(value = "/getEquipos")
	public @ResponseBody List<Map<String, Object>> getEquipos(@RequestBody Map<String, Object> payload) {
		List<Map<String, Object>> lstRpta = new ArrayList<Map<String, Object>>();
		lstRpta = DAO.getEquipos(DAO.getConnection(), payload);
		return lstRpta;
	}

	@CrossOrigin
	@PostMapping(value = "/getEquipo")
	public @ResponseBody Map<String, Object> getEquipo(@RequestBody Map<String, Object> payload) {
		Map<String, Object> lstRpta = new HashMap<String, Object>();
		lstRpta = DAO.getEquipo(DAO.getConnection(), payload);
		return lstRpta;
	}

	@CrossOrigin
	@PostMapping(value = "/getUbigeo")
	public @ResponseBody Map<String, Object> getUbigeo(@RequestBody Map<String, Object> payload) {
		Map<String, Object> lstRpta = new HashMap<String, Object>();
		lstRpta = DAO.getUbigeo(DAO.getConnection(), payload);
		return lstRpta;
	}

	@CrossOrigin
	@PostMapping(value = "/getEnlaces")
	public @ResponseBody List<Map<String, Object>> getEnlaces(@RequestBody Map<String, Object> payload) {
		List<Map<String, Object>> lstRpta = new ArrayList<Map<String, Object>>();
		lstRpta = DAO.getEnlaces(payload, DAO.getConnection());
		return lstRpta;
	}

	@CrossOrigin
	@PostMapping(value = "/getLags")
	public @ResponseBody List<Map<String, Object>> getLags(@RequestBody Map<String, Object> payload) {
		List<Map<String, Object>> lstRpta = new ArrayList<Map<String, Object>>();
		lstRpta = DAO.getLags(DAO.getConnection(), payload);
		return lstRpta;
	}

	@CrossOrigin
	@PostMapping(value = "/getEnlace")
	public @ResponseBody Map<String, Object> getEnlace(@RequestBody Map<String, Object> payload) {
		Map<String, Object> lstRpta = new HashMap<String, Object>();
		lstRpta = DAO.getEnlace(payload, DAO.getConnection());
		return lstRpta;
	}

	@CrossOrigin
	@PostMapping(value = "/getRegions")
	public @ResponseBody List<String> getRegiones() {
		List<String> rpta = DAO.getRegiones(DAO.getConnection());
		return rpta;
	}

	@CrossOrigin
	@PostMapping(value = "/getProvincias")
	public @ResponseBody List<String> getProvincias(@RequestBody Map<String, Object> payload) {
		List<String> rpta = DAO.getProvincias(DAO.getConnection(), payload);
		return rpta;
	}

	@CrossOrigin
	@PostMapping(value = "/getDistritos")
	public @ResponseBody List<String> getDistritos(@RequestBody Map<String, Object> payload) {
		List<String> rpta = DAO.getDistrito(DAO.getConnection(), payload);
		return rpta;
	}

	@CrossOrigin
	@PostMapping(value = "/getLocalidades")
	public @ResponseBody List<String> getLocalidades(@RequestBody Map<String, Object> payload) {
		List<String> rpta = DAO.getLocalidad(DAO.getConnection(), payload);
		return rpta;
	}

	@CrossOrigin
	@PostMapping(value = "/getTipos")
	public @ResponseBody List<String> getTipos() {
		List<String> rpta = DAO.getTipo(DAO.getConnection());
		return rpta;
	}

	@CrossOrigin
	@PostMapping(value = "/getData")
	public @ResponseBody List<Map<String, Object>> getData(@RequestBody Map<String, Object> payload) {
		return DAO.getRegistros(DAO.getConnection(), payload);
	}

	@CrossOrigin
	@PostMapping(value = "/getDataTrafico")
	public @ResponseBody List<Map<String, Object>> getDataTrafico(@RequestBody Map<String, Object> payload) {
		return DAO.getDataTrafico(DAO.getConnection(), payload);
	}

	@CrossOrigin
	@PostMapping(value = "/getDataTraficoEstad")
	public @ResponseBody List<Map<String, Object>> getDataTraficoEstad(@RequestBody Map<String, Object> payload) {
		return DAO.getDataTraficoEstad(DAO.getConnection(), payload);
	}

	@CrossOrigin
	@PostMapping(value = "/getDataTraficoEstadColas")
	public @ResponseBody List<Map<String, Object>> getDataTraficoEstadColas(@RequestBody Map<String, Object> payload) {
		return DAO.getDataTraficoEstadColas(DAO.getConnection(), payload);
	}

	@CrossOrigin
	@PostMapping(value = "/getDisponibilidad")
	public @ResponseBody List<Map<String, Object>> getDisponibilidad(@RequestBody Map<String, Object> payload) {
		return DAO.getDisponibilidad(DAO.getConnection(), payload);
	}

	@CrossOrigin
	@PostMapping(value = "/getDisponibilidadEquipo")
	public @ResponseBody List<Map<String, Object>> getDisponibilidadEquipo(@RequestBody Map<String, Object> payload) {
		return DAO.getDisponibilidadEquipo(DAO.getConnection(), payload);
	}
	
	@CrossOrigin
	@PostMapping(value = "/getDisponibilidadFechas")
	public @ResponseBody List<Float> getDisponibilidadFechas(@RequestBody Map<String, Object> payload) {
		return DAO.DisponibilidadFechas(DAO.getConnection(), payload);
	}
	
	@CrossOrigin
	@PostMapping(value = "/getDisponibilidadFechasEquipo")
	public @ResponseBody List<Float> getDisponibilidadFechasEquipo(@RequestBody Map<String, Object> payload) {
		return DAO.DisponibilidadFechasEquipo(DAO.getConnection(), payload);
	}
	
	@CrossOrigin
	@PostMapping(value = "/getEstadisticas")
	public @ResponseBody Map<String, String> getEstadisticas(@RequestBody Map<String, Object> payload) {
		return DAO.getEstadisticas(DAO.getConnection(), payload);
	}

	@CrossOrigin
	@PostMapping(value = "/getEstadisticasColas")
	public @ResponseBody Map<String, String> getEstadisticasColas(@RequestBody Map<String, Object> payload) {
		return DAO.getEstadisticasColas(DAO.getConnection(), payload);
	}

	@CrossOrigin
	@PostMapping(value = "/getAcumuladoTime")
	public @ResponseBody Map<String, Object> getAcumuladoTime(@RequestBody Map<String, Object> payload) {
		return DAO.getAcumuladoTime(DAO.getConnection(), payload);
	}
	
	
	@CrossOrigin
	@PostMapping(value = "/getAcumuladoTimeEquipo")
	public @ResponseBody Map<String, Object> getAcumuladoTimeEquipo(@RequestBody Map<String, Object> payload) {
		return DAO.getAcumuladoTimeEquipo(DAO.getConnection(), payload);
	}
	
	@CrossOrigin
	@PostMapping(value = "/dispoInterval")
	public @ResponseBody Map<String, Object> dispoInterval(@RequestBody Map<String, Object> payload) {
		return DAO.dispoInterval(DAO.getConnection(), payload);
	}
	
	@CrossOrigin
	@PostMapping(value = "/dispoIntervalEquipo")
	public @ResponseBody Map<String, Object> dispoIntervalEquipo(@RequestBody Map<String, Object> payload) {
		return DAO.dispoIntervalEquipo(DAO.getConnection(), payload);
	}
	
	@CrossOrigin
	@PostMapping(value = "/getCaidos")
	public @ResponseBody List<Map<String, Object>> getCaidos(@RequestBody Map<String, Object> payload) {
		return DAO.getCaidos(DAO.getConnection(), payload);
	}
	@CrossOrigin
	@PostMapping(value = "/getTable")
	public @ResponseBody Map<String, Object> getTable(@RequestBody Map<String, Object> payload) {
		return DAO.getTable(DAO.getConnection(), payload);
	}
	
	@CrossOrigin
	@PostMapping(value = "/getTableTrafico")
	public @ResponseBody Map<String, Object> getTableTrafico(@RequestBody Map<String, Object> payload) {
		return DAO.getTableTrafico(DAO.getConnection(), payload);
	}
	
	@CrossOrigin
	@PostMapping(value = "/getDrops")
	public @ResponseBody List<Map<String, Object>> getDrops(@RequestBody Map<String, Object> payload) {
		return DAO.getDrops(DAO.getConnection(), payload);
	}
	
	@CrossOrigin
	@PostMapping(value = "/getDataDropsColas")
	public @ResponseBody List<Map<String, Object>> getDataDropsColas(@RequestBody Map<String, Object> payload) {
		return DAO.getDataDropsColas(DAO.getConnection(), payload);
	}
	
	@CrossOrigin
	@PostMapping(value = "/getReporteTraficoEnlace")
	public @ResponseBody List<Map<String, Object>> getReporteTraficoEnlace(@RequestBody Map<String, Object> payload) {
		return DAO.getReporteTraficoEnlace(DAO.getConnection(), payload);
	}
	
	@CrossOrigin
	@PostMapping(value = "/getReporteDispoEquipos")
	public @ResponseBody List<Map<String, Object>> getReporteDispoEquipos(@RequestBody Map<String, Object> payload) {
		return DAO.getReporteDispoEquipos(DAO.getConnection(), payload);
	}
	
	@CrossOrigin
	@PostMapping(value = "/getEquipos2")
	public @ResponseBody List<Map<String, Object>> getEquipos2(@RequestBody Map<String, Object> payload) {
		List<Map<String, Object>> lstRpta = new ArrayList<Map<String, Object>>();
		lstRpta = DAO.getEquipos2(DAO.getConnection(), payload);
		return lstRpta;
	}
	
	@CrossOrigin
	@PostMapping(value = "/getdispoEquiposDash")
	public @ResponseBody Map<String, Object> getdispoEquiposDash(@RequestBody Map<String, Object> payload) {
		return DAO.getdispoEquiposDash(DAO.getConnection(), payload);
	}
	
	@CrossOrigin
	@PostMapping(value = "/getdispoEnlasDash")
	public @ResponseBody List<Map<String, Object>> getdispoEnlasDash(@RequestBody Map<String, Object> payload) {
		return DAO.getdispoEnlasDash(DAO.getConnection(), payload);
	}
	
	@CrossOrigin
	@PostMapping(value = "/dispoLagsDash")
	public @ResponseBody List<Map<String, Object>> dispoLagsDash(@RequestBody Map<String, Object> payload) {
		return DAO.dispoLagsDash(DAO.getConnection(), payload);
	}
	
	@CrossOrigin
	@PostMapping(value = "/trafEnlaDash")
	public @ResponseBody List<Map<String, Object>> trafEnlaDash(@RequestBody Map<String, Object> payload) {
		return DAO.trafEnlaDash(DAO.getConnection(), payload);
	}
	
	@CrossOrigin
	@PostMapping(value = "/trafAgregaDash")
	public @ResponseBody List<Map<String, Object>> trafAgregaDash(@RequestBody Map<String, Object> payload) {
		return DAO.trafAgregaDash(DAO.getConnection(), payload);
	}
	
	@CrossOrigin
	@PostMapping(value = "/getReporteTraficoLag")
	public @ResponseBody List<Map<String, Object>> getReporteTraficoLag(@RequestBody Map<String, Object> payload) {
		return DAO.getReporteTraficoLag(DAO.getConnection(), payload);
	}
}