package com.pe.acp.webtraffic.dao;

import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.HashMap;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.springframework.stereotype.Component;

@Component
public class DAO {

	public static Connection getConnection() {
		Connection conn = null;
		Properties prop = new Properties();
		InputStream input = null;

		try {
			// input = new FileInputStream("D:\\WAR\\dbDESA.properties");
			 input = new FileInputStream("D:\\WAR\\db.properties");
			//input = new FileInputStream("/opt/webtrafico/db.properties");
			prop.load(input);

			String userName = prop.getProperty("DB.SQL_SERVER.USER");
			String password = prop.getProperty("DB.SQL_SERVER.PASSWORD");
			String url = prop.getProperty("DB.SQL_SERVER.URL");
			Class.forName(prop.getProperty("DB.SQL_SERVER.CLASS_FORNAME"));

			conn = DriverManager.getConnection(url, userName, password);

		} catch (Exception e) {
			System.out.println("Error de conexion a base de datos POSTGRESQL.");
			e.printStackTrace();
		}
		return conn;

	}

	public static boolean log_in(String user, String pass) {
		Connection conn = getConnection();
		boolean rpta = false;
		String sql = "SELECT 1 FROM dev_webtrafico.\"Usuarios\" WHERE \"USERNAME\" = ? AND \"PASSWORD\" = ?";
		try {
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, user);
			stmt.setString(2, pass);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				rpta = true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rpta;
	}

	public static List<String> getRegiones(Connection conn) {
		List<String> rpta = new ArrayList<String>();
		String sql = "SELECT DISTINCT  \"REGION\"\n" + "	FROM dev_webtrafico.\"Ubigeo\" ORDER BY \"REGION\"";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				rpta.add(rs.getString(1));
			}
			rpta.add("");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rpta;
	};

	public static List<String> getProvincias(Connection conn, Map<String, Object> payload) {
		List<String> rpta = new ArrayList<String>();
		String sql = "SELECT DISTINCT \"PROVINCIA\"\n" + "	FROM dev_webtrafico.\"Ubigeo\"\n"
				+ "    WHERE \"REGION\"=? ORDER BY \"PROVINCIA\"";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, String.valueOf(payload.get("region")));
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				rpta.add(rs.getString(1));
			}
			rpta.add("");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rpta;
	};

	public static List<String> getDistrito(Connection conn, Map<String, Object> payload) {
		List<String> rpta = new ArrayList<String>();
		String sql = "SELECT DISTINCT \"DISTRITO\"\n" + "	FROM dev_webtrafico.\"Ubigeo\"\n"
				+ "    WHERE \"PROVINCIA\"=? ORDER BY \"DISTRITO\"";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, String.valueOf(payload.get("provincia")));
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				rpta.add(rs.getString(1));
			}
			rpta.add("");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rpta;
	};

	public static List<String> getLocalidad(Connection conn, Map<String, Object> payload) {
		List<String> rpta = new ArrayList<String>();
		String sql = "SELECT DISTINCT \"LOCALIDAD\"\n" + "	FROM dev_webtrafico.\"Ubigeo\"\n"
				+ "    where \"DISTRITO\" = ? ORDER BY \"LOCALIDAD\"";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, String.valueOf(payload.get("distrito")));
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				rpta.add(rs.getString(1));
			}
			rpta.add("");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rpta;
	};

	public static List<String> getTipo(Connection conn) {
		List<String> rpta = new ArrayList<String>();
		String slq = "SELECT DISTINCT \"TIPO\"\n" + "	FROM dev_webtrafico.\"Equipos\" ORDER BY \"TIPO\"";
		try {
			PreparedStatement ps = conn.prepareStatement(slq);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				rpta.add(rs.getString(1));
			}
			rpta.add("");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rpta;
	}
	
	public static List<Map<String, Object>> getEquipos(Connection conn, Map<String, Object> payload) {
		List<Map<String, Object>> rpta = new ArrayList<Map<String, Object>>();
		String sql1 = "SELECT E.\"ID_EQUIPO\", \"REGION\", \"LOCALIDAD\", \"IP\", \"TIPO\", CASE WHEN \"REDUNDANCIA\" IS NULL THEN '' ELSE \"REDUNDANCIA\" END\n"
				+ "	FROM dev_webtrafico.\"Equipos\" E\n" + "    JOIN dev_webtrafico.\"Ubigeo\" U\n"
				+ "    ON E.\"ID_EQUIPO\" = U.\"ID_EQUIPO\""
				+ "ORDER BY \"ID_EQUIPO\"\n";
		try {
			PreparedStatement ps = conn.prepareStatement(sql1);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Map<String, Object> equipo = new HashMap<String, Object>();
				equipo.put("ID", rs.getInt(1));
				equipo.put("REGION", rs.getString(2));
				equipo.put("LOCALIDAD", rs.getString(3));
				equipo.put("IP", rs.getString(4));
				equipo.put("TIPO", rs.getString(5)+" "+rs.getString(6));
				rpta.add(equipo);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rpta;
	}
	
	public static List<Map<String, Object>> getEquipos2(Connection conn, Map<String, Object> payload) {
		List<Map<String, Object>> rpta = new ArrayList<Map<String, Object>>();
		
		String sql1="SELECT E.\"ID_EQUIPO\", \"REGION\", \"LOCALIDAD\", \"IP\", \"TIPO\", CASE WHEN \"REDUNDANCIA\" IS NULL THEN '' ELSE \"REDUNDANCIA\" END\n"
		+ "	FROM dev_webtrafico.\"Equipos\" E\n" + "    JOIN dev_webtrafico.\"Ubigeo\" U\n"
		+ "    ON E.\"ID_EQUIPO\" = U.\"ID_EQUIPO\""
		+ "ORDER BY \"ID_EQUIPO\"\n";
		try {
			PreparedStatement ps = conn.prepareStatement(sql1);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Map<String, Object> equipo = new HashMap<String, Object>();
				equipo.put("ID", rs.getInt(1));
				equipo.put("REGION", rs.getString(2));
				equipo.put("LOCALIDAD", rs.getString(3));
				equipo.put("IP", rs.getString(4));
				equipo.put("TIPO", rs.getString(5)+" "+rs.getString(6));
				Float dispo = DAO.getDispoEquipo(conn, rs.getInt(1));
				equipo.put("DISPONIBILIDAD", dispo);
				String c = DAO.getColor(dispo, rs.getString(5), rs.getString(6));
				String color = "";
				if (c.equals("#33cc33")) {
					color = "btn btn-success btn-sm";
				} else if (c.equals("#ffff33")) {
					color = "btn btn-warning btn-sm";
				} else {
					color = "btn btn-danger btn-sm";
				}
				equipo.put("COLOR", color);
				rpta.add(equipo);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rpta;
	}
	private static String getColor(Float dispo, String tipo, String redundancia) {
		String color = "";
		if ( tipo.equals("CONEXI�N")) {
			if ( dispo < 99.6) {
				color = "#ff471a";
			} else if ( dispo >= 99.6 && dispo < 99.8 ) {
				color = "#ffff33";
			} else {
				color = "#33cc33";
			}
		} else if ( tipo.equals("DISTRIBUIDOR")) {
			if ( redundancia.equals("")) {
				if ( dispo < 99.7) {
					color = "#ff471a";
				} else if ( dispo >= 99.7 && dispo < 99.9 ) {
					color = "#ffff33";
				} else {
					color = "#33cc33";
				}
			} else if ( redundancia.equals("RR")) {
				if ( dispo < 99.999) {
					color = "#ff471a";
				} else if ( dispo >= 99.999 && dispo < 100 ) {
					color = "#ffff33";
				} else {
					color = "#33cc33";
				}
			} else {
				if ( dispo < 99.9) {
					color = "#ff471a";
				} else if ( dispo >= 99.9 && dispo < 100 ) {
					color = "#ffff33";
				} else {
					color = "#33cc33";
				}
			}
		} else if ( tipo.equals("AGREGADOR")) {
			if ( redundancia.equals("RR")) {
				if ( dispo < 99.999) {
					color = "#ff471a";
				} else if ( dispo >= 99.999 && dispo < 100 ) {
					color = "#ffff33";
				} else {
					color = "#33cc33";
				}
			} else {
				if ( dispo < 99.9) {
					color = "#ff471a";
				} else if ( dispo >= 99.9 && dispo < 100 ) {
					color = "#ffff33";
				} else {
					color = "#33cc33";
				}
			}
		} else {
			if ( dispo < 99.999) {
				color = "#ff471a";
			} else if ( dispo >= 99.999 && dispo < 100 ) {
				color = "#ffff33";
			} else {
				color = "#33cc33";
			}
		}
		return color;
	}
	private static Float getDispoEquipo(Connection conn, int id) {
		Float rpta = 0f;
		String sql1 = "SELECT ROUND((CAST (1 - (CAST (CAST (COUNT(1) AS FLOAT) / CAST ((1*60*24*7) AS FLOAT) AS FLOAT)) AS NUMERIC))*100,3)\r\n" + 
				"	FROM dev_webtrafico.\"DisponibilidadEquipo\"\r\n" + 
				"	WHERE \"ID_EQUIPO\"= ? AND \"DISPONIBILIDAD\"= 4 AND \"FECHA\" > current_timestamp - interval '7 days'";
		try {
			PreparedStatement ps1 = conn.prepareStatement(sql1);
			ps1.setInt(1, id);
			ResultSet rs1 = ps1.executeQuery();
			if (rs1.next()) {
				rpta = rs1.getFloat(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rpta;
	}

	public static Map<String, Object> getEquipo(Connection conn, Map<String, Object> payload) {
		Map<String, Object> rpta = new HashMap<String, Object>();
		String sql = "SELECT \"HOSTNAME\", \"IP\", \"TIPO\", CASE WHEN \"REDUNDANCIA\" IS NULL THEN '' ELSE \"REDUNDANCIA\" END\n"
				+ "	FROM dev_webtrafico.\"Equipos\"\n"
				+ "    WHERE \"ID_EQUIPO\"=?";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_equipo"))));
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				rpta.put("hostname", rs.getString(1));
				rpta.put("ip", rs.getString(2));
				rpta.put("tipo", rs.getString(3));
				rpta.put("redundancia", rs.getString(4));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rpta;
	}

	public static List<Map<String, Object>> getEnlaces(Map<String, Object> payload, Connection conn) {
		List<Map<String, Object>> rpta = new ArrayList<Map<String, Object>>();
		int id = Integer.parseInt(String.valueOf(payload.get("id")));
		String sql = "SELECT \"ID_ENLACE\", \"INTERFAZ_DESTINO\", \"DESTINO\", \"INTERFAZ\", \"LAG_GROUP\"\n"
				+ "	FROM dev_webtrafico.\"Enlaces\" EN\n" + "    JOIN dev_webtrafico.\"Interfaces\" INTF\n"
				+ "    ON EN.\"ID_INTERFAZ\" = INTF.\"ID_INTERFAZ\"\n"
				+ "    WHERE \"ID_EQUIPO\" = ? ORDER BY \"LAG_GROUP\", \"ID_ENLACE\"";
		try {
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Map<String, Object> mapita = new HashMap<String, Object>();
				mapita.put("id_enlace", rs.getInt(1));
				mapita.put("intf_dst", rs.getString(2));
				mapita.put("destino", rs.getString(3));
				mapita.put("intf_org", rs.getString(4));
				mapita.put("lag_group", rs.getInt(5));
				mapita.put("indisponibilidad", DAO.getIndis(conn, rs.getInt(1)));
				rpta.add(mapita);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rpta;
	};

	public static List<Map<String, Object>> getRegistros(Connection conn, Map<String, Object> payload) {
		List<Map<String, Object>> lstR = new ArrayList<Map<String, Object>>();
		List<BigDecimal> y_in_cola_1 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_in_cola_2 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_in_cola_3 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_in_cola_4 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_in_cola_5 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_in_cola_6 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_in_cola_7 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_in_cola_8 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_out_cola_1 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_out_cola_2 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_out_cola_3 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_out_cola_4 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_out_cola_5 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_out_cola_6 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_out_cola_7 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_out_cola_8 = new ArrayList<BigDecimal>();
		List<String> x = new ArrayList<String>();
		String sql = "";
		boolean lag = false;
		if (payload.get("tipo").equals("CONEXI�N") || payload.get("tipo").equals("DISTRIBUIDOR")) {

			if (payload.get("id_enlace") != null) {
				sql = "SELECT \"OUT_COLA_1\", \"OUT_COLA_2\", \"OUT_COLA_3\", \"OUT_COLA_4\", \"OUT_COLA_5\", \"OUT_COLA_6\", \"OUT_COLA_7\", \"OUT_COLA_8\", \"FECHA\"\n"
						+ "	FROM dev_webtrafico.\"DataDiferenciada\"\n" + " WHERE \"ID_ENLACE\"=? AND \"FECHA\" >= current_timestamp - interval '7 days' ORDER BY \"FECHA\"";
			} else {
				lag = true;
				sql = "SELECT \"OUT_COLA_1\", \"OUT_COLA_2\", \"OUT_COLA_3\", \"OUT_COLA_4\", \"OUT_COLA_5\", \"OUT_COLA_6\", \"OUT_COLA_7\", \"OUT_COLA_8\", \"FECHA\"\n"
						+ "	FROM dev_webtrafico.\"DataLagsDiferenciada\"\n"
						+ " WHERE \"ID_LAG\" = ? AND \"FECHA\" >= current_timestamp - interval '7 days' ORDER BY \"FECHA\"";
			}
			
			try {
				PreparedStatement stmt = conn.prepareStatement(sql);
				if (lag) {
					stmt.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_lag"))));
				} else {
					stmt.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_enlace"))));
				}

				ResultSet rs = stmt.executeQuery();
				while (rs.next()) {
					y_out_cola_1.add(rs.getBigDecimal(1)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_out_cola_2.add(rs.getBigDecimal(2)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_out_cola_3.add(rs.getBigDecimal(3)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_out_cola_4.add(rs.getBigDecimal(4)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_out_cola_5.add(rs.getBigDecimal(5)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_out_cola_6.add(rs.getBigDecimal(6)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_out_cola_7.add(rs.getBigDecimal(7)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_out_cola_8.add(rs.getBigDecimal(8)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					x.add(String.valueOf(rs.getTimestamp(9)));
				}
				Map<String, Object> cola1out = new HashMap<String, Object>();
				Map<String, Object> cola2out = new HashMap<String, Object>();
				Map<String, Object> cola3out = new HashMap<String, Object>();
				Map<String, Object> cola4out = new HashMap<String, Object>();
				Map<String, Object> cola5out = new HashMap<String, Object>();
				Map<String, Object> cola6out = new HashMap<String, Object>();
				Map<String, Object> cola7out = new HashMap<String, Object>();
				Map<String, Object> cola8out = new HashMap<String, Object>();

				cola1out.put("tipo", "DEFAULT");
				cola1out.put("y", y_out_cola_1);
				cola1out.put("x", x);
				lstR.add(cola1out);
				cola2out.put("tipo", "VIDEO");
				cola2out.put("y", y_out_cola_2);
				cola2out.put("x", x);
				lstR.add(cola2out);
				cola3out.put("tipo", "GESTI�N");
				cola3out.put("y", y_out_cola_3);
				cola3out.put("x", x);
				lstR.add(cola3out);
				cola4out.put("tipo", "CLIENTES");
				cola4out.put("y", y_out_cola_4);
				cola4out.put("x", x);
				lstR.add(cola4out);
				cola5out.put("tipo", "CLIENTES");
				cola5out.put("y", y_out_cola_5);
				cola5out.put("x", x);
				lstR.add(cola5out);
				cola6out.put("tipo", "CLIENTES");
				cola6out.put("y", y_out_cola_6);
				cola6out.put("x", x);
				lstR.add(cola6out);
				cola7out.put("tipo", "VOIP");
				cola7out.put("y", y_out_cola_7);
				cola7out.put("x", x);
				lstR.add(cola7out);
				cola8out.put("tipo", "ROUTING CONTROL");
				cola8out.put("y", y_out_cola_8);
				cola8out.put("x", x);
				lstR.add(cola8out);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else {
			boolean lag2 = false;
			String sql1 = "";
			if (payload.get("id_enlace") != null) {
				sql1 = "SELECT \"IN_COLA_1\", \"IN_COLA_2\", \"IN_COLA_3\", \"IN_COLA_4\", \"IN_COLA_5\", \"IN_COLA_6\", \"IN_COLA_7\", \"IN_COLA_8\", \"OUT_COLA_1\", \"OUT_COLA_2\", \"OUT_COLA_3\", \"OUT_COLA_4\", \"OUT_COLA_5\", \"OUT_COLA_6\", \"OUT_COLA_7\", \"OUT_COLA_8\", \"FECHA\"\n"
						+ "	FROM dev_webtrafico.\"DataDiferenciada\"\n" + " WHERE \"ID_ENLACE\"=? AND \"FECHA\" > current_timestamp - interval '7 days' ORDER BY \"FECHA\"";
			} else {
				lag2 = true;
				sql1 = "SELECT \"IN_COLA_1\", \"IN_COLA_2\", \"IN_COLA_3\", \"IN_COLA_4\", \"IN_COLA_5\", \"IN_COLA_6\", \"IN_COLA_7\", \"IN_COLA_8\", \"OUT_COLA_1\", \"OUT_COLA_2\", \"OUT_COLA_3\", \"OUT_COLA_4\", \"OUT_COLA_5\", \"OUT_COLA_6\", \"OUT_COLA_7\", \"OUT_COLA_8\", \"FECHA\"\n"
						+ "	FROM dev_webtrafico.\"DataLagsDiferenciada\"\n"
						+ " WHERE \"ID_LAG\" = ? AND \"FECHA\" > current_timestamp - interval '7 days' ORDER BY \"FECHA\"";
			}
			try {
				PreparedStatement ps = conn.prepareStatement(sql1);
				if (lag2) {
					ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_lag"))));
				} else {
					ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_enlace"))));
				}

				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					y_in_cola_1.add(rs.getBigDecimal(1)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_in_cola_2.add(rs.getBigDecimal(2)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_in_cola_3.add(rs.getBigDecimal(3)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_in_cola_4.add(rs.getBigDecimal(4)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_in_cola_5.add(rs.getBigDecimal(5)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_in_cola_6.add(rs.getBigDecimal(6)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_in_cola_7.add(rs.getBigDecimal(7)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_in_cola_8.add(rs.getBigDecimal(8)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_out_cola_1.add(rs.getBigDecimal(9)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_out_cola_2.add(rs.getBigDecimal(10)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_out_cola_3.add(rs.getBigDecimal(11)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_out_cola_4.add(rs.getBigDecimal(12)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_out_cola_5.add(rs.getBigDecimal(13)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_out_cola_6.add(rs.getBigDecimal(14)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_out_cola_7.add(rs.getBigDecimal(15)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					y_out_cola_8.add(rs.getBigDecimal(16)
							.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
					x.add(String.valueOf(rs.getTimestamp(17)));
				}
				Map<String, Object> cola1in = new HashMap<String, Object>();
				Map<String, Object> cola2in = new HashMap<String, Object>();
				Map<String, Object> cola3in = new HashMap<String, Object>();
				Map<String, Object> cola4in = new HashMap<String, Object>();
				Map<String, Object> cola5in = new HashMap<String, Object>();
				Map<String, Object> cola6in = new HashMap<String, Object>();
				Map<String, Object> cola7in = new HashMap<String, Object>();
				Map<String, Object> cola8in = new HashMap<String, Object>();
				Map<String, Object> cola1out = new HashMap<String, Object>();
				Map<String, Object> cola2out = new HashMap<String, Object>();
				Map<String, Object> cola3out = new HashMap<String, Object>();
				Map<String, Object> cola4out = new HashMap<String, Object>();
				Map<String, Object> cola5out = new HashMap<String, Object>();
				Map<String, Object> cola6out = new HashMap<String, Object>();
				Map<String, Object> cola7out = new HashMap<String, Object>();
				Map<String, Object> cola8out = new HashMap<String, Object>();

				cola1in.put("tipo", "DEFAULT");
				cola1in.put("y", y_in_cola_1);
				cola1in.put("x", x);
				lstR.add(cola1in);
				cola2in.put("tipo", "VIDEO");
				cola2in.put("y", y_in_cola_2);
				cola2in.put("x", x);
				lstR.add(cola2in);
				cola3in.put("tipo", "GESTI�N");
				cola3in.put("y", y_in_cola_3);
				cola3in.put("x", x);
				lstR.add(cola3in);
				cola4in.put("tipo", "CLIENTES");
				cola4in.put("y", y_in_cola_4);
				cola4in.put("x", x);
				lstR.add(cola4in);
				cola5in.put("tipo", "CLIENTES");
				cola5in.put("y", y_in_cola_5);
				cola5in.put("x", x);
				lstR.add(cola5in);
				cola6in.put("tipo", "CLIENTES");
				cola6in.put("y", y_in_cola_6);
				cola6in.put("x", x);
				lstR.add(cola6in);
				cola7in.put("tipo", "VOIP");
				cola7in.put("y", y_in_cola_7);
				cola7in.put("x", x);
				lstR.add(cola7in);
				cola8in.put("tipo", "ROUTING CONTROL");
				cola8in.put("y", y_in_cola_8);
				cola8in.put("x", x);
				lstR.add(cola8in);

				cola1out.put("tipo", "DEFAULT");
				cola1out.put("y", y_out_cola_1);
				cola1out.put("x", x);
				lstR.add(cola1out);
				cola2out.put("tipo", "VIDEO");
				cola2out.put("y", y_out_cola_2);
				cola2out.put("x", x);
				lstR.add(cola2out);
				cola3out.put("tipo", "GESTI�N");
				cola3out.put("y", y_out_cola_3);
				cola3out.put("x", x);
				lstR.add(cola3out);
				cola4out.put("tipo", "CLIENTES");
				cola4out.put("y", y_out_cola_4);
				cola4out.put("x", x);
				lstR.add(cola4out);
				cola5out.put("tipo", "CLIENTES");
				cola5out.put("y", y_out_cola_5);
				cola5out.put("x", x);
				lstR.add(cola5out);
				cola6out.put("tipo", "CLIENTES");
				cola6out.put("y", y_out_cola_6);
				cola6out.put("x", x);
				lstR.add(cola6out);
				cola7out.put("tipo", "VOIP");
				cola7out.put("y", y_out_cola_7);
				cola7out.put("x", x);
				lstR.add(cola7out);
				cola8out.put("tipo", "ROUTING CONTROL");
				cola8out.put("y", y_out_cola_8);
				cola8out.put("x", x);
				lstR.add(cola8out);

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

		return lstR;
	}

	public static Map<String, Object> getUbigeo(Connection conn, Map<String, Object> payload) {
		Map<String, Object> rpta = new HashMap<String, Object>();
		String sql = "SELECT \"REGION\", \"PROVINCIA\", \"DISTRITO\", \"LOCALIDAD\"\n"
				+ "FROM dev_webtrafico.\"Ubigeo\"\n" + "WHERE \"ID_EQUIPO\"=?";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_equipo"))));
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				rpta.put("region", rs.getString(1));
				rpta.put("provincia", rs.getString(2));
				rpta.put("distrito", rs.getString(3));
				rpta.put("localidad", rs.getString(4));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rpta;
	}

	public static List<Map<String, Object>> getDataTrafico(Connection conn, Map<String, Object> payload) {
		HashMap<String, Object> rpta = new HashMap<String, Object>();
		HashMap<String, Object> rpta2 = new HashMap<String, Object>();
		List<Map<String, Object>> rpta3 = new ArrayList<Map<String, Object>>();
		List<Object> x = new ArrayList<Object>();
		List<Object> y = new ArrayList<Object>();
		List<Object> y2 = new ArrayList<Object>();
		String sql = "";
		boolean lag = false;
		if (payload.get("id_enlace") != null) {
			sql = "SELECT \"TRAFICO\", \"TRAFICO_IN\", \"FECHA\"\n" + "	FROM dev_webtrafico.\"DataDiferenciada\"\n"
					+ "    WHERE \"ID_ENLACE\" = ? AND \"FECHA\" >= current_timestamp - interval '7 days' ORDER BY \"FECHA\"";
		} else {
			lag = true;
			sql = "SELECT \"TRAFICO\", \"TRAFICO_IN\", \"FECHA\"\n" + "	FROM dev_webtrafico.\"DataLagsDiferenciada\"\n"
					+ "    WHERE \"ID_LAG\" = ? AND \"FECHA\" >= current_timestamp - interval '7 days' ORDER BY \"FECHA\"";
		}

		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			if (lag) {
				ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_lag"))));
			} else {
				ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_enlace"))));
			}
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				x.add(String.valueOf(rs.getTimestamp(3)));
				BigDecimal pre = rs.getBigDecimal(1);
				if (pre == null) {
					y.add(pre);
				} else {
					y.add(pre.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
				}

				BigDecimal pre2 = rs.getBigDecimal(2);
				if (pre2 == null) {
					y2.add(pre2);
				} else {
					y2.add(pre2.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
				}
			}
			rpta.put("trafico", y);
			rpta.put("fecha", x);

			rpta2.put("trafico_in", y2);
			rpta2.put("fecha", x);

			rpta3.add(rpta);
			rpta3.add(rpta2);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rpta3;
	}

	public static Float getIndis(Connection conn, int id) {
		Float rpta = 0f;
		String sql1 = "SELECT ROUND((CAST (1 - (CAST (CAST (COUNT(1) AS FLOAT) / CAST ((1*60*24*7) AS FLOAT) AS FLOAT)) AS NUMERIC))*100,3)\r\n" + 
				"	FROM dev_webtrafico.\"Disponibilidad\"\r\n" + 
				"	WHERE \"ID_ENLACE\"= ? AND \"DISPONIBILIDAD\"= 'down' AND \"FECHA\" > current_timestamp - interval '7 days'";
		try {
			PreparedStatement ps1 = conn.prepareStatement(sql1);
			ps1.setInt(1, id);
			ResultSet rs1 = ps1.executeQuery();
			if (rs1.next()) {
				rpta = rs1.getFloat(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rpta;
	}

	public static List<Map<String, Object>> getDisponibilidad(Connection conn, Map<String, Object> payload) {
		List<Map<String, Object>> rpta = new ArrayList<Map<String, Object>>();
		String sql = "";
		boolean lag = false;
		if (payload.get("id_enlace") != null) {
			sql = "SELECT \"FECHA\", current_timestamp \n" + "FROM dev_webtrafico.\"Disponibilidad\"\n"
					+ "WHERE \"DISPONIBILIDAD\"= 'down' AND \"ID_ENLACE\"= ? \n" + "ORDER BY \"FECHA\"";
		} else {
			lag = true;
			sql = "SELECT \"FECHA\", current_timestamp \n" + "FROM dev_webtrafico.\"DisponibilidadLag\"\n"
					+ "WHERE \"DISPONIBILIDAD\"= 4 AND \"ID_LAG\" = ? \n" + "ORDER BY \"FECHA\"";
		}

		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			if (lag) {
				ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_lag"))));
			} else {
				ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_enlace"))));
			}

			ResultSet rs = ps.executeQuery();
			List<List<Timestamp>> eventos = new ArrayList<List<Timestamp>>();
			List<Timestamp> event = new ArrayList<Timestamp>();
			Timestamp currentTS = null;
			while (rs.next()) {
				currentTS = rs.getTimestamp(2);
				if (event.isEmpty()) {
					event.add(rs.getTimestamp(1));
					if (rs.isLast()) {
						List<Timestamp> temp = new ArrayList<Timestamp>(event);
						eventos.add(temp);
					}
				} else {
					if (rs.getTimestamp(1).getTime() - event.get(event.size() - 1).getTime() < 120000) {
						event.add(rs.getTimestamp(1));
						if (rs.isLast()) {
							List<Timestamp> temp = new ArrayList<Timestamp>(event);
							eventos.add(temp);
						}
					} else {
						List<Timestamp> temp = new ArrayList<Timestamp>(event);
						eventos.add(temp);
						event.clear();
						event.add(rs.getTimestamp(1));
						if (rs.isLast()) {
							List<Timestamp> temp2 = new ArrayList<Timestamp>(event);
							eventos.add(temp2);
						}
					}
				}
			}
			for (List<Timestamp> e : eventos) {
				Map<String, Object> eve = new HashMap<String, Object>();
				eve.put("inicio", String.valueOf(e.get(0)));
				Timestamp t = e.get(e.size() - 1);
				if (currentTS.getTime() - t.getTime() < 70000) {
					eve.put("fin", String.valueOf("A�n indisponible"));
				} else {
					eve.put("fin", String.valueOf(e.get(e.size() - 1)));
				}
				if (String.valueOf(e.get(0)).equals(String.valueOf(e.get(e.size() - 1)))) {
					String formated = formatdate(60000);
					Timestamp t2 = new Timestamp(e.get(0).getTime() + 60000);
					eve.put("fin", String.valueOf(t2));
					eve.put("duracion", formated);
					rpta.add(eve);
				} else {
					String formated = formatdate(e.get(e.size() - 1).getTime() - e.get(0).getTime() + 60000);
					eve.put("duracion", formated);
					rpta.add(eve);
				}
			}
			Collections.reverse(rpta);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rpta;
	}

	public static List<Map<String, Object>> getDisponibilidadEquipo(Connection conn, Map<String, Object> payload) {
		List<Map<String, Object>> rpta = new ArrayList<Map<String, Object>>();
		String sql = "";
		
		sql = "SELECT \"FECHA\", current_timestamp \n" + "FROM dev_webtrafico.\"DisponibilidadEquipo\"\n"
				+ "WHERE \"DISPONIBILIDAD\"= 4 AND \"ID_EQUIPO\"= ? \n" + "ORDER BY \"FECHA\"";

		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_equipo"))));

			ResultSet rs = ps.executeQuery();
			List<List<Timestamp>> eventos = new ArrayList<List<Timestamp>>();
			List<Timestamp> event = new ArrayList<Timestamp>();
			Timestamp currentTS = null;
			while (rs.next()) {
				currentTS = rs.getTimestamp(2);
				if (event.isEmpty()) {
					event.add(rs.getTimestamp(1));
					if (rs.isLast()) {
						List<Timestamp> temp = new ArrayList<Timestamp>(event);
						eventos.add(temp);
					}
				} else {
					if (rs.getTimestamp(1).getTime() - event.get(event.size() - 1).getTime() < 120000) {
						event.add(rs.getTimestamp(1));
						if (rs.isLast()) {
							List<Timestamp> temp = new ArrayList<Timestamp>(event);
							eventos.add(temp);
						}
					} else {
						List<Timestamp> temp = new ArrayList<Timestamp>(event);
						eventos.add(temp);
						event.clear();
						event.add(rs.getTimestamp(1));
						if (rs.isLast()) {
							List<Timestamp> temp2 = new ArrayList<Timestamp>(event);
							eventos.add(temp2);
						}
					}
				}
			}
			for (List<Timestamp> e : eventos) {
				Map<String, Object> eve = new HashMap<String, Object>();
				eve.put("inicio", String.valueOf(e.get(0)));
				Timestamp t = e.get(e.size() - 1);
				if (currentTS.getTime() - t.getTime() < 70000) {
					eve.put("fin", String.valueOf("A�n indisponible"));
				} else {
					eve.put("fin", String.valueOf(e.get(e.size() - 1)));
				}
				if (String.valueOf(e.get(0)).equals(String.valueOf(e.get(e.size() - 1)))) {
					String formated = formatdate(60000);
					Timestamp t2 = new Timestamp(e.get(0).getTime() + 60000);
					eve.put("fin", String.valueOf(t2));
					eve.put("duracion", formated);
					rpta.add(eve);
				} else {
					String formated = formatdate(e.get(e.size() - 1).getTime() - e.get(0).getTime() + 60000);
					eve.put("duracion", formated);
					rpta.add(eve);
				}
			}
			Collections.reverse(rpta);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rpta;
	}

	private static String formatdate(long dif) {
		Long different = dif;
		long secondsInMilli = 1000;
		long minutesInMilli = secondsInMilli * 60;
		long hoursInMilli = minutesInMilli * 60;
		long daysInMilli = hoursInMilli * 24;

		long elapsedDays = different / daysInMilli;
		different = different % daysInMilli;

		long elapsedHours = different / hoursInMilli;
		different = different % hoursInMilli;

		long elapsedMinutes = different / minutesInMilli;
		different = different % minutesInMilli;

		long elapsedSeconds = different / secondsInMilli;

		String rpta = elapsedDays + " d�as, " + elapsedHours + " horas, " + elapsedMinutes + " minutos, "
				+ elapsedSeconds + " segundos";
		return rpta;
	}

	public static Map<String, Object> getEnlace(Map<String, Object> payload, Connection conn) {
		HashMap<String, Object> rpta = new HashMap<String, Object>();
		String param = String.valueOf(payload.get("id_enlace"));
		int id = Integer.parseInt(param);
		String sql = "SELECT \"ID_ENLACE\", \"INTERFAZ_DESTINO\", \"DESTINO\", \"INTERFAZ\", \"LAG_GROUP\"\n"
				+ "	FROM dev_webtrafico.\"Enlaces\" EN\n" + "    JOIN dev_webtrafico.\"Interfaces\" INTF\n"
				+ "    ON EN.\"ID_INTERFAZ\" = INTF.\"ID_INTERFAZ\"\n" + "    WHERE \"ID_ENLACE\" = ?";

		try {
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				rpta.put("id_enlace", rs.getInt(1));
				rpta.put("intf_dst", rs.getString(2));
				rpta.put("destino", rs.getString(3));
				rpta.put("intf_org", rs.getString(4));
				rpta.put("lag_group", rs.getInt(5));
				rpta.put("indisponibilidad", DAO.getIndis(conn, rs.getInt(1)));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rpta;
	}

	public static Map<String, String> getEstadisticas(Connection conn, Map<String, Object> payload) {
		Map<String, String> d = new HashMap<String, String>();
		String sql = "";
		boolean lag = false;
		if (payload.get("id_enlace") != null) {
			sql = "SELECT MIN(\"TRAFICO\")*8/300, round(AVG(\"TRAFICO\"),4)*8/300, MAX(\"TRAFICO\")*8/300, stddev(\"TRAFICO\")*8/300,\n"
					+ "	MIN(\"TRAFICO_IN\")*8/300, round(AVG(\"TRAFICO_IN\"),4)*8/300, MAX(\"TRAFICO_IN\")*8/300, stddev(\"TRAFICO_IN\")*8/300\n"
					+ "	FROM dev_webtrafico.\"DataDiferenciada\"\n"
					+ "    WHERE \"ID_ENLACE\"=? AND \"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')";
		} else {
			lag = true;
			sql = "SELECT MIN(\"TRAFICO\")*8/300, round(AVG(\"TRAFICO\"),4)*8/300, MAX(\"TRAFICO\")*8/300, stddev(\"TRAFICO\")*8/300,\n"
					+ "	MIN(\"TRAFICO_IN\")*8/300, round(AVG(\"TRAFICO_IN\"),4)*8/300, MAX(\"TRAFICO_IN\")*8/300, stddev(\"TRAFICO_IN\")*8/300\n"
					+ "	FROM dev_webtrafico.\"DataLagsDiferenciada\"\n"
					+ "    WHERE \"ID_LAG\" = ? AND \"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')";
		}

		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			if (lag) {
				ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_lag"))));
				ps.setString(2, String.valueOf((payload.get("fecha_ini"))));
				ps.setString(3, String.valueOf((payload.get("fecha_fin"))));
			} else {
				ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_enlace"))));
				ps.setString(2, String.valueOf((payload.get("fecha_ini"))));
				ps.setString(3, String.valueOf((payload.get("fecha_fin"))));
			}

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				d.put("min", SIFormater(rs.getBigDecimal(1)));
				d.put("avg", SIFormater(rs.getBigDecimal(2)));
				d.put("max", SIFormater(rs.getBigDecimal(3)));
				d.put("stddev", SIFormater(rs.getBigDecimal(4)));

				d.put("min_in", SIFormater(rs.getBigDecimal(5)));
				d.put("avg_in", SIFormater(rs.getBigDecimal(6)));
				d.put("max_in", SIFormater(rs.getBigDecimal(7)));
				d.put("stddev_in", SIFormater(rs.getBigDecimal(8)));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return d;
	}

	public static Map<String, String> getEstadisticasColas(Connection conn, Map<String, Object> payload) {
		Map<String, String> d = new HashMap<String, String>();
		String sql = "";
		boolean lag = false;
		if (payload.get("id_enlace") != null) {
			sql = "SELECT\n"
					+ "MIN(\"IN_COLA_1\")*8/300, ROUND(AVG(\"IN_COLA_1\")*8/300,4), MAX(\"IN_COLA_1\")*8/300, STDDEV(\"IN_COLA_1\")*8/300,\n"
					+ "MIN(\"IN_COLA_2\")*8/300, ROUND(AVG(\"IN_COLA_2\")*8/300,4), MAX(\"IN_COLA_2\")*8/300, STDDEV(\"IN_COLA_2\")*8/300,\n"
					+ "MIN(\"IN_COLA_3\")*8/300, ROUND(AVG(\"IN_COLA_3\")*8/300,4), MAX(\"IN_COLA_3\")*8/300, STDDEV(\"IN_COLA_3\")*8/300,\n"
					+ "MIN(\"IN_COLA_4\")*8/300, ROUND(AVG(\"IN_COLA_4\")*8/300,4), MAX(\"IN_COLA_4\")*8/300, STDDEV(\"IN_COLA_4\")*8/300,\n"
					+ "MIN(\"IN_COLA_5\")*8/300, ROUND(AVG(\"IN_COLA_5\")*8/300,4), MAX(\"IN_COLA_5\")*8/300, STDDEV(\"IN_COLA_5\")*8/300,\n"
					+ "MIN(\"IN_COLA_6\")*8/300, ROUND(AVG(\"IN_COLA_6\")*8/300,4), MAX(\"IN_COLA_6\")*8/300, STDDEV(\"IN_COLA_6\")*8/300,\n"
					+ "MIN(\"IN_COLA_7\")*8/300, ROUND(AVG(\"IN_COLA_7\")*8/300,4), MAX(\"IN_COLA_7\")*8/300, STDDEV(\"IN_COLA_7\")*8/300,\n"
					+ "MIN(\"IN_COLA_8\")*8/300, ROUND(AVG(\"IN_COLA_8\")*8/300,4), MAX(\"IN_COLA_8\")*8/300, STDDEV(\"IN_COLA_8\")*8/300,\n"
					+ "MIN(\"OUT_COLA_1\")*8/300, ROUND(AVG(\"OUT_COLA_1\")*8/300,4), MAX(\"OUT_COLA_1\")*8/300, STDDEV(\"OUT_COLA_1\")*8/300,\n"
					+ "MIN(\"OUT_COLA_2\")*8/300, ROUND(AVG(\"OUT_COLA_2\")*8/300,4), MAX(\"OUT_COLA_2\")*8/300, STDDEV(\"OUT_COLA_2\")*8/300,\n"
					+ "MIN(\"OUT_COLA_3\")*8/300, ROUND(AVG(\"OUT_COLA_3\")*8/300,4), MAX(\"OUT_COLA_3\")*8/300, STDDEV(\"OUT_COLA_3\")*8/300,\n"
					+ "MIN(\"OUT_COLA_4\")*8/300, ROUND(AVG(\"OUT_COLA_4\")*8/300,4), MAX(\"OUT_COLA_4\")*8/300, STDDEV(\"OUT_COLA_4\")*8/300,\n"
					+ "MIN(\"OUT_COLA_5\")*8/300, ROUND(AVG(\"OUT_COLA_5\")*8/300,4), MAX(\"OUT_COLA_5\")*8/300, STDDEV(\"OUT_COLA_5\")*8/300,\n"
					+ "MIN(\"OUT_COLA_6\")*8/300, ROUND(AVG(\"OUT_COLA_6\")*8/300,4), MAX(\"OUT_COLA_6\")*8/300, STDDEV(\"OUT_COLA_6\")*8/300,\n"
					+ "MIN(\"OUT_COLA_7\")*8/300, ROUND(AVG(\"OUT_COLA_7\")*8/300,4), MAX(\"OUT_COLA_7\")*8/300, STDDEV(\"OUT_COLA_7\")*8/300,\n"
					+ "MIN(\"OUT_COLA_8\")*8/300, ROUND(AVG(\"OUT_COLA_8\")*8/300,4), MAX(\"OUT_COLA_8\")*8/300, STDDEV(\"OUT_COLA_8\")*8/300,\n"
					+ "MIN(\"IN_DROP_1\")/300, ROUND(AVG(\"IN_DROP_1\")/300,4), MAX(\"IN_DROP_1\")/300, STDDEV(\"IN_DROP_1\")/300,\n"
					+ "MIN(\"IN_DROP_2\")/300, ROUND(AVG(\"IN_DROP_2\")/300,4), MAX(\"IN_DROP_2\")/300, STDDEV(\"IN_DROP_2\")/300,\n"
					+ "MIN(\"IN_DROP_3\")/300, ROUND(AVG(\"IN_DROP_3\")/300,4), MAX(\"IN_DROP_3\")/300, STDDEV(\"IN_DROP_3\")/300,\n"
					+ "MIN(\"IN_DROP_4\")/300, ROUND(AVG(\"IN_DROP_4\")/300,4), MAX(\"IN_DROP_4\")/300, STDDEV(\"IN_DROP_4\")/300,\n"
					+ "MIN(\"IN_DROP_5\")/300, ROUND(AVG(\"IN_DROP_5\")/300,4), MAX(\"IN_DROP_5\")/300, STDDEV(\"IN_DROP_5\")/300,\n"
					+ "MIN(\"IN_DROP_6\")/300, ROUND(AVG(\"IN_DROP_6\")/300,4), MAX(\"IN_DROP_6\")/300, STDDEV(\"IN_DROP_6\")/300,\n"
					+ "MIN(\"IN_DROP_7\")/300, ROUND(AVG(\"IN_DROP_7\")/300,4), MAX(\"IN_DROP_7\")/300, STDDEV(\"IN_DROP_7\")/300,\n"
					+ "MIN(\"IN_DROP_8\")/300, ROUND(AVG(\"IN_DROP_8\")/300,4), MAX(\"IN_DROP_8\")/300, STDDEV(\"IN_DROP_8\")/300,\n"
					+ "MIN(\"OUT_DROP_1\")/300, ROUND(AVG(\"OUT_DROP_1\")/300,4), MAX(\"OUT_DROP_1\")/300, STDDEV(\"OUT_DROP_1\")/300,\n"
					+ "MIN(\"OUT_DROP_2\")/300, ROUND(AVG(\"OUT_DROP_2\")/300,4), MAX(\"OUT_DROP_2\")/300, STDDEV(\"OUT_DROP_2\")/300,\n"
					+ "MIN(\"OUT_DROP_3\")/300, ROUND(AVG(\"OUT_DROP_3\")/300,4), MAX(\"OUT_DROP_3\")/300, STDDEV(\"OUT_DROP_3\")/300,\n"
					+ "MIN(\"OUT_DROP_4\")/300, ROUND(AVG(\"OUT_DROP_4\")/300,4), MAX(\"OUT_DROP_4\")/300, STDDEV(\"OUT_DROP_4\")/300,\n"
					+ "MIN(\"OUT_DROP_5\")/300, ROUND(AVG(\"OUT_DROP_5\")/300,4), MAX(\"OUT_DROP_5\")/300, STDDEV(\"OUT_DROP_5\")/300,\n"
					+ "MIN(\"OUT_DROP_6\")/300, ROUND(AVG(\"OUT_DROP_6\")/300,4), MAX(\"OUT_DROP_6\")/300, STDDEV(\"OUT_DROP_6\")/300,\n"
					+ "MIN(\"OUT_DROP_7\")/300, ROUND(AVG(\"OUT_DROP_7\")/300,4), MAX(\"OUT_DROP_7\")/300, STDDEV(\"OUT_DROP_7\")/300,\n"
					+ "MIN(\"OUT_DROP_8\")/300, ROUND(AVG(\"OUT_DROP_8\")/300,4), MAX(\"OUT_DROP_8\")/300, STDDEV(\"OUT_DROP_8\")/300\n"
					+ "FROM dev_webtrafico.\"DataDiferenciada\"\n"
					+ "WHERE \"ID_ENLACE\" = ? AND \"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')";
		} else {
			lag = true;
			sql = "SELECT\n"
					+ "MIN(\"IN_COLA_1\")*8/300, ROUND(AVG(\"IN_COLA_1\")*8/300,4), MAX(\"IN_COLA_1\")*8/300, STDDEV(\"IN_COLA_1\")*8/300,\n"
					+ "MIN(\"IN_COLA_2\")*8/300, ROUND(AVG(\"IN_COLA_2\")*8/300,4), MAX(\"IN_COLA_2\")*8/300, STDDEV(\"IN_COLA_2\")*8/300,\n"
					+ "MIN(\"IN_COLA_3\")*8/300, ROUND(AVG(\"IN_COLA_3\")*8/300,4), MAX(\"IN_COLA_3\")*8/300, STDDEV(\"IN_COLA_3\")*8/300,\n"
					+ "MIN(\"IN_COLA_4\")*8/300, ROUND(AVG(\"IN_COLA_4\")*8/300,4), MAX(\"IN_COLA_4\")*8/300, STDDEV(\"IN_COLA_4\")*8/300,\n"
					+ "MIN(\"IN_COLA_5\")*8/300, ROUND(AVG(\"IN_COLA_5\")*8/300,4), MAX(\"IN_COLA_5\")*8/300, STDDEV(\"IN_COLA_5\")*8/300,\n"
					+ "MIN(\"IN_COLA_6\")*8/300, ROUND(AVG(\"IN_COLA_6\")*8/300,4), MAX(\"IN_COLA_6\")*8/300, STDDEV(\"IN_COLA_6\")*8/300,\n"
					+ "MIN(\"IN_COLA_7\")*8/300, ROUND(AVG(\"IN_COLA_7\")*8/300,4), MAX(\"IN_COLA_7\")*8/300, STDDEV(\"IN_COLA_7\")*8/300,\n"
					+ "MIN(\"IN_COLA_8\")*8/300, ROUND(AVG(\"IN_COLA_8\")*8/300,4), MAX(\"IN_COLA_8\")*8/300, STDDEV(\"IN_COLA_8\")*8/300,\n"
					+ "MIN(\"OUT_COLA_1\")*8/300, ROUND(AVG(\"OUT_COLA_1\")*8/300,4), MAX(\"OUT_COLA_1\")*8/300, STDDEV(\"OUT_COLA_1\")*8/300,\n"
					+ "MIN(\"OUT_COLA_2\")*8/300, ROUND(AVG(\"OUT_COLA_2\")*8/300,4), MAX(\"OUT_COLA_2\")*8/300, STDDEV(\"OUT_COLA_2\")*8/300,\n"
					+ "MIN(\"OUT_COLA_3\")*8/300, ROUND(AVG(\"OUT_COLA_3\")*8/300,4), MAX(\"OUT_COLA_3\")*8/300, STDDEV(\"OUT_COLA_3\")*8/300,\n"
					+ "MIN(\"OUT_COLA_4\")*8/300, ROUND(AVG(\"OUT_COLA_4\")*8/300,4), MAX(\"OUT_COLA_4\")*8/300, STDDEV(\"OUT_COLA_4\")*8/300,\n"
					+ "MIN(\"OUT_COLA_5\")*8/300, ROUND(AVG(\"OUT_COLA_5\")*8/300,4), MAX(\"OUT_COLA_5\")*8/300, STDDEV(\"OUT_COLA_5\")*8/300,\n"
					+ "MIN(\"OUT_COLA_6\")*8/300, ROUND(AVG(\"OUT_COLA_6\")*8/300,4), MAX(\"OUT_COLA_6\")*8/300, STDDEV(\"OUT_COLA_6\")*8/300,\n"
					+ "MIN(\"OUT_COLA_7\")*8/300, ROUND(AVG(\"OUT_COLA_7\")*8/300,4), MAX(\"OUT_COLA_7\")*8/300, STDDEV(\"OUT_COLA_7\")*8/300,\n"
					+ "MIN(\"OUT_COLA_8\")*8/300, ROUND(AVG(\"OUT_COLA_8\")*8/300,4), MAX(\"OUT_COLA_8\")*8/300, STDDEV(\"OUT_COLA_8\")*8/300,\n"
					+ "MIN(\"IN_DROP_1\")/300, ROUND(AVG(\"IN_DROP_1\")/300,4), MAX(\"IN_DROP_1\")/300, STDDEV(\"IN_DROP_1\")/300,\n"
					+ "MIN(\"IN_DROP_2\")/300, ROUND(AVG(\"IN_DROP_2\")/300,4), MAX(\"IN_DROP_2\")/300, STDDEV(\"IN_DROP_2\")/300,\n"
					+ "MIN(\"IN_DROP_3\")/300, ROUND(AVG(\"IN_DROP_3\")/300,4), MAX(\"IN_DROP_3\")/300, STDDEV(\"IN_DROP_3\")/300,\n"
					+ "MIN(\"IN_DROP_4\")/300, ROUND(AVG(\"IN_DROP_4\")/300,4), MAX(\"IN_DROP_4\")/300, STDDEV(\"IN_DROP_4\")/300,\n"
					+ "MIN(\"IN_DROP_5\")/300, ROUND(AVG(\"IN_DROP_5\")/300,4), MAX(\"IN_DROP_5\")/300, STDDEV(\"IN_DROP_5\")/300,\n"
					+ "MIN(\"IN_DROP_6\")/300, ROUND(AVG(\"IN_DROP_6\")/300,4), MAX(\"IN_DROP_6\")/300, STDDEV(\"IN_DROP_6\")/300,\n"
					+ "MIN(\"IN_DROP_7\")/300, ROUND(AVG(\"IN_DROP_7\")/300,4), MAX(\"IN_DROP_7\")/300, STDDEV(\"IN_DROP_7\")/300,\n"
					+ "MIN(\"IN_DROP_8\")/300, ROUND(AVG(\"IN_DROP_8\")/300,4), MAX(\"IN_DROP_8\")/300, STDDEV(\"IN_DROP_8\")/300,\n"
					+ "MIN(\"OUT_DROP_1\")/300, ROUND(AVG(\"OUT_DROP_1\")/300,4), MAX(\"OUT_DROP_1\")/300, STDDEV(\"OUT_DROP_1\")/300,\n"
					+ "MIN(\"OUT_DROP_2\")/300, ROUND(AVG(\"OUT_DROP_2\")/300,4), MAX(\"OUT_DROP_2\")/300, STDDEV(\"OUT_DROP_2\")/300,\n"
					+ "MIN(\"OUT_DROP_3\")/300, ROUND(AVG(\"OUT_DROP_3\")/300,4), MAX(\"OUT_DROP_3\")/300, STDDEV(\"OUT_DROP_3\")/300,\n"
					+ "MIN(\"OUT_DROP_4\")/300, ROUND(AVG(\"OUT_DROP_4\")/300,4), MAX(\"OUT_DROP_4\")/300, STDDEV(\"OUT_DROP_4\")/300,\n"
					+ "MIN(\"OUT_DROP_5\")/300, ROUND(AVG(\"OUT_DROP_5\")/300,4), MAX(\"OUT_DROP_5\")/300, STDDEV(\"OUT_DROP_5\")/300,\n"
					+ "MIN(\"OUT_DROP_6\")/300, ROUND(AVG(\"OUT_DROP_6\")/300,4), MAX(\"OUT_DROP_6\")/300, STDDEV(\"OUT_DROP_6\")/300,\n"
					+ "MIN(\"OUT_DROP_7\")/300, ROUND(AVG(\"OUT_DROP_7\")/300,4), MAX(\"OUT_DROP_7\")/300, STDDEV(\"OUT_DROP_7\")/300,\n"
					+ "MIN(\"OUT_DROP_8\")/300, ROUND(AVG(\"OUT_DROP_8\")/300,4), MAX(\"OUT_DROP_8\")/300, STDDEV(\"OUT_DROP_8\")/300\n"
					+ "FROM dev_webtrafico.\"DataLagsDiferenciada\"\n"
					+ "WHERE \"ID_LAG\" = ? AND \"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')";
		}

		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			if (lag) {
				ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_lag"))));
				ps.setString(2, String.valueOf((payload.get("fecha_ini"))));
				ps.setString(3, String.valueOf((payload.get("fecha_fin"))));
			} else {
				ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_enlace"))));
				ps.setString(2, String.valueOf((payload.get("fecha_ini"))));
				ps.setString(3, String.valueOf((payload.get("fecha_fin"))));
			}

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				d.put("in_col1_min", SIFormater(rs.getBigDecimal(1)));
				d.put("in_col1_avg", SIFormater(rs.getBigDecimal(2)));
				d.put("in_col1_max", SIFormater(rs.getBigDecimal(3)));
				d.put("in_col1_stddev", SIFormater(rs.getBigDecimal(4)));

				d.put("in_col2_min", SIFormater(rs.getBigDecimal(5)));
				d.put("in_col2_avg", SIFormater(rs.getBigDecimal(6)));
				d.put("in_col2_max", SIFormater(rs.getBigDecimal(7)));
				d.put("in_col2_stddev", SIFormater(rs.getBigDecimal(8)));

				d.put("in_col3_min", SIFormater(rs.getBigDecimal(9)));
				d.put("in_col3_avg", SIFormater(rs.getBigDecimal(10)));
				d.put("in_col3_max", SIFormater(rs.getBigDecimal(11)));
				d.put("in_col3_stddev", SIFormater(rs.getBigDecimal(12)));

				d.put("in_col4_min", SIFormater(rs.getBigDecimal(13)));
				d.put("in_col4_avg", SIFormater(rs.getBigDecimal(14)));
				d.put("in_col4_max", SIFormater(rs.getBigDecimal(15)));
				d.put("in_col4_stddev", SIFormater(rs.getBigDecimal(16)));

				d.put("in_col5_min", SIFormater(rs.getBigDecimal(17)));
				d.put("in_col5_avg", SIFormater(rs.getBigDecimal(18)));
				d.put("in_col5_max", SIFormater(rs.getBigDecimal(19)));
				d.put("in_col5_stddev", SIFormater(rs.getBigDecimal(20)));

				d.put("in_col6_min", SIFormater(rs.getBigDecimal(21)));
				d.put("in_col6_avg", SIFormater(rs.getBigDecimal(22)));
				d.put("in_col6_max", SIFormater(rs.getBigDecimal(23)));
				d.put("in_col6_stddev", SIFormater(rs.getBigDecimal(24)));

				d.put("in_col7_min", SIFormater(rs.getBigDecimal(25)));
				d.put("in_col7_avg", SIFormater(rs.getBigDecimal(26)));
				d.put("in_col7_max", SIFormater(rs.getBigDecimal(27)));
				d.put("in_col7_stddev", SIFormater(rs.getBigDecimal(28)));

				d.put("in_col8_min", SIFormater(rs.getBigDecimal(29)));
				d.put("in_col8_avg", SIFormater(rs.getBigDecimal(30)));
				d.put("in_col8_max", SIFormater(rs.getBigDecimal(31)));
				d.put("in_col8_stddev", SIFormater(rs.getBigDecimal(32)));

				d.put("out_col1_min", SIFormater(rs.getBigDecimal(33)));
				d.put("out_col1_avg", SIFormater(rs.getBigDecimal(34)));
				d.put("out_col1_max", SIFormater(rs.getBigDecimal(35)));
				d.put("out_col1_stddev", SIFormater(rs.getBigDecimal(36)));

				d.put("out_col2_min", SIFormater(rs.getBigDecimal(37)));
				d.put("out_col2_avg", SIFormater(rs.getBigDecimal(38)));
				d.put("out_col2_max", SIFormater(rs.getBigDecimal(39)));
				d.put("out_col2_stddev", SIFormater(rs.getBigDecimal(40)));

				d.put("out_col3_min", SIFormater(rs.getBigDecimal(41)));
				d.put("out_col3_avg", SIFormater(rs.getBigDecimal(42)));
				d.put("out_col3_max", SIFormater(rs.getBigDecimal(43)));
				d.put("out_col3_stddev", SIFormater(rs.getBigDecimal(44)));

				d.put("out_col4_min", SIFormater(rs.getBigDecimal(45)));
				d.put("out_col4_avg", SIFormater(rs.getBigDecimal(46)));
				d.put("out_col4_max", SIFormater(rs.getBigDecimal(47)));
				d.put("out_col4_stddev", SIFormater(rs.getBigDecimal(48)));

				d.put("out_col5_min", SIFormater(rs.getBigDecimal(49)));
				d.put("out_col5_avg", SIFormater(rs.getBigDecimal(50)));
				d.put("out_col5_max", SIFormater(rs.getBigDecimal(51)));
				d.put("out_col5_stddev", SIFormater(rs.getBigDecimal(52)));

				d.put("out_col6_min", SIFormater(rs.getBigDecimal(53)));
				d.put("out_col6_avg", SIFormater(rs.getBigDecimal(54)));
				d.put("out_col6_max", SIFormater(rs.getBigDecimal(55)));
				d.put("out_col6_stddev", SIFormater(rs.getBigDecimal(56)));

				d.put("out_col7_min", SIFormater(rs.getBigDecimal(57)));
				d.put("out_col7_avg", SIFormater(rs.getBigDecimal(58)));
				d.put("out_col7_max", SIFormater(rs.getBigDecimal(59)));
				d.put("out_col7_stddev", SIFormater(rs.getBigDecimal(60)));

				d.put("out_col8_min", SIFormater(rs.getBigDecimal(61)));
				d.put("out_col8_avg", SIFormater(rs.getBigDecimal(62)));
				d.put("out_col8_max", SIFormater(rs.getBigDecimal(63)));
				d.put("out_col8_stddev", SIFormater(rs.getBigDecimal(64)));
				
				d.put("in_drop1_min", SIFormater(rs.getBigDecimal(65)));
				d.put("in_drop1_avg", SIFormater(rs.getBigDecimal(66)));
				d.put("in_drop1_max", SIFormater(rs.getBigDecimal(67)));
				d.put("in_drop1_stddev", SIFormater(rs.getBigDecimal(68)));

				d.put("in_drop2_min", SIFormater(rs.getBigDecimal(69)));
				d.put("in_drop2_avg", SIFormater(rs.getBigDecimal(70)));
				d.put("in_drop2_max", SIFormater(rs.getBigDecimal(71)));
				d.put("in_drop2_stddev", SIFormater(rs.getBigDecimal(72)));

				d.put("in_drop3_min", SIFormater(rs.getBigDecimal(73)));
				d.put("in_drop3_avg", SIFormater(rs.getBigDecimal(74)));
				d.put("in_drop3_max", SIFormater(rs.getBigDecimal(75)));
				d.put("in_drop3_stddev", SIFormater(rs.getBigDecimal(76)));

				d.put("in_drop4_min", SIFormater(rs.getBigDecimal(77)));
				d.put("in_drop4_avg", SIFormater(rs.getBigDecimal(78)));
				d.put("in_drop4_max", SIFormater(rs.getBigDecimal(79)));
				d.put("in_drop4_stddev", SIFormater(rs.getBigDecimal(80)));

				d.put("in_drop5_min", SIFormater(rs.getBigDecimal(81)));
				d.put("in_drop5_avg", SIFormater(rs.getBigDecimal(82)));
				d.put("in_drop5_max", SIFormater(rs.getBigDecimal(83)));
				d.put("in_drop5_stddev", SIFormater(rs.getBigDecimal(84)));

				d.put("in_drop6_min", SIFormater(rs.getBigDecimal(85)));
				d.put("in_drop6_avg", SIFormater(rs.getBigDecimal(86)));
				d.put("in_drop6_max", SIFormater(rs.getBigDecimal(87)));
				d.put("in_drop6_stddev", SIFormater(rs.getBigDecimal(88)));

				d.put("in_drop7_min", SIFormater(rs.getBigDecimal(89)));
				d.put("in_drop7_avg", SIFormater(rs.getBigDecimal(90)));
				d.put("in_drop7_max", SIFormater(rs.getBigDecimal(91)));
				d.put("in_drop7_stddev", SIFormater(rs.getBigDecimal(92)));

				d.put("in_drop8_min", SIFormater(rs.getBigDecimal(93)));
				d.put("in_drop8_avg", SIFormater(rs.getBigDecimal(94)));
				d.put("in_drop8_max", SIFormater(rs.getBigDecimal(95)));
				d.put("in_drop8_stddev", SIFormater(rs.getBigDecimal(96)));

				d.put("out_drop1_min", SIFormater(rs.getBigDecimal(97)));
				d.put("out_drop1_avg", SIFormater(rs.getBigDecimal(98)));
				d.put("out_drop1_max", SIFormater(rs.getBigDecimal(99)));
				d.put("out_drop1_stddev", SIFormater(rs.getBigDecimal(100)));

				d.put("out_drop2_min", SIFormater(rs.getBigDecimal(101)));
				d.put("out_drop2_avg", SIFormater(rs.getBigDecimal(102)));
				d.put("out_drop2_max", SIFormater(rs.getBigDecimal(103)));
				d.put("out_drop2_stddev", SIFormater(rs.getBigDecimal(104)));

				d.put("out_drop3_min", SIFormater(rs.getBigDecimal(105)));
				d.put("out_drop3_avg", SIFormater(rs.getBigDecimal(106)));
				d.put("out_drop3_max", SIFormater(rs.getBigDecimal(107)));
				d.put("out_drop3_stddev", SIFormater(rs.getBigDecimal(108)));

				d.put("out_drop4_min", SIFormater(rs.getBigDecimal(109)));
				d.put("out_drop4_avg", SIFormater(rs.getBigDecimal(110)));
				d.put("out_drop4_max", SIFormater(rs.getBigDecimal(111)));
				d.put("out_drop4_stddev", SIFormater(rs.getBigDecimal(112)));

				d.put("out_drop5_min", SIFormater(rs.getBigDecimal(113)));
				d.put("out_drop5_avg", SIFormater(rs.getBigDecimal(114)));
				d.put("out_drop5_max", SIFormater(rs.getBigDecimal(115)));
				d.put("out_drop5_stddev", SIFormater(rs.getBigDecimal(116)));

				d.put("out_drop6_min", SIFormater(rs.getBigDecimal(117)));
				d.put("out_drop6_avg", SIFormater(rs.getBigDecimal(118)));
				d.put("out_drop6_max", SIFormater(rs.getBigDecimal(119)));
				d.put("out_drop6_stddev", SIFormater(rs.getBigDecimal(120)));

				d.put("out_drop7_min", SIFormater(rs.getBigDecimal(121)));
				d.put("out_drop7_avg", SIFormater(rs.getBigDecimal(122)));
				d.put("out_drop7_max", SIFormater(rs.getBigDecimal(123)));
				d.put("out_drop7_stddev", SIFormater(rs.getBigDecimal(124)));

				d.put("out_drop8_min", SIFormater(rs.getBigDecimal(125)));
				d.put("out_drop8_avg", SIFormater(rs.getBigDecimal(126)));
				d.put("out_drop8_max", SIFormater(rs.getBigDecimal(127)));
				d.put("out_drop8_stddev", SIFormater(rs.getBigDecimal(128)));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return d;
	}

	private static String SIFormater(BigDecimal bps) {
		String rpta = "";
		try {
			if (bps.divide(new BigDecimal(1000000000), 4, BigDecimal.ROUND_HALF_UP).compareTo(new BigDecimal(1)) == 1) {
				rpta = redondear(bps.divide(new BigDecimal(1000000000), 4, BigDecimal.ROUND_HALF_UP)) + " Gbps";
			} else {
				if (bps.divide(new BigDecimal(1000000), 4, BigDecimal.ROUND_HALF_UP)
						.compareTo(new BigDecimal(1)) == 1) {
					rpta = redondear(bps.divide(new BigDecimal(1000000), 4, BigDecimal.ROUND_HALF_UP)) + " Mbps";
				} else {
					if (bps.divide(new BigDecimal(1000), 4, BigDecimal.ROUND_HALF_UP)
							.compareTo(new BigDecimal(1)) == 1) {
						rpta = redondear(bps.divide(new BigDecimal(1000), 4, BigDecimal.ROUND_HALF_UP)) + " Kbps";
					} else {
						rpta = redondear(bps) + " bps";
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return rpta;
	};

	private static String redondear(BigDecimal input) {
		String rpta = "0.000";
		try {
			String bps = String.valueOf(input);
			String[] partes = bps.split("\\.");
			String cortado = partes[1].substring(0, 3);
			rpta = partes[0] + "." + cortado;

		} catch (Exception e) {

		}
		return rpta;
	}

	public static List<Map<String, Object>> getDataTraficoEstad(Connection conn, Map<String, Object> payload) {
		HashMap<String, Object> rpta = new HashMap<String, Object>();
		HashMap<String, Object> rpta2 = new HashMap<String, Object>();
		List<Map<String, Object>> rpta3 = new ArrayList<Map<String, Object>>();
		List<Object> x = new ArrayList<Object>();
		List<Object> y = new ArrayList<Object>();
		List<Object> y2 = new ArrayList<Object>();
		String sql = "";
		boolean lag = false;
		if (payload.get("id_enlace") != null) {
			sql = "SELECT \"TRAFICO\", \"TRAFICO_IN\", \"FECHA\"\n" + "	FROM dev_webtrafico.\"DataDiferenciada\"\n"
					+ "    WHERE \"ID_ENLACE\"=? AND \"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') ORDER BY \"FECHA\"";
		} else {
			lag = true;
			sql = "SELECT \"TRAFICO\", \"TRAFICO_IN\", \"FECHA\"\n" + "	FROM dev_webtrafico.\"DataLagsDiferenciada\"\n"
					+ "    WHERE \"ID_LAG\" = ? AND \"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')\n"
					+ "    ORDER BY \"FECHA\"";
		}
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			if (lag) {
				ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_lag"))));
				ps.setString(2, String.valueOf(payload.get("fecha_ini")));
				ps.setString(3, String.valueOf(payload.get("fecha_fin")));
			} else {
				ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_enlace"))));
				ps.setString(2, String.valueOf(payload.get("fecha_ini")));
				ps.setString(3, String.valueOf(payload.get("fecha_fin")));
			}

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				x.add(String.valueOf(rs.getTimestamp(3)));
				BigDecimal pre = rs.getBigDecimal(1);
				if (pre == null) {
					y.add(pre);
				} else {
					y.add(pre.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
				}

				BigDecimal pre2 = rs.getBigDecimal(2);
				if (pre2 == null) {
					y2.add(pre2);
				} else {
					y2.add(pre2.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
				}
			}
			rpta.put("trafico", y);
			rpta.put("fecha", x);

			rpta2.put("trafico_in", y2);
			rpta2.put("fecha", x);

			rpta3.add(rpta);
			rpta3.add(rpta2);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rpta3;
	}

	public static List<Map<String, Object>> getDataTraficoEstadColas(Connection conn, Map<String, Object> payload) {
		List<Map<String, Object>> rpta3 = new ArrayList<Map<String, Object>>();
		List<Object> x = new ArrayList<Object>();
		List<Object> in_col1 = new ArrayList<Object>();
		List<Object> in_col2 = new ArrayList<Object>();
		List<Object> in_col3 = new ArrayList<Object>();
		List<Object> in_col4 = new ArrayList<Object>();
		List<Object> in_col5 = new ArrayList<Object>();
		List<Object> in_col6 = new ArrayList<Object>();
		List<Object> in_col7 = new ArrayList<Object>();
		List<Object> in_col8 = new ArrayList<Object>();
		List<Object> out_col1 = new ArrayList<Object>();
		List<Object> out_col2 = new ArrayList<Object>();
		List<Object> out_col3 = new ArrayList<Object>();
		List<Object> out_col4 = new ArrayList<Object>();
		List<Object> out_col5 = new ArrayList<Object>();
		List<Object> out_col6 = new ArrayList<Object>();
		List<Object> out_col7 = new ArrayList<Object>();
		List<Object> out_col8 = new ArrayList<Object>();
		String sql = "";
		boolean lag = false;
		if (payload.get("id_enlace") != null) {
			sql = "SELECT \"IN_COLA_1\", \"IN_COLA_2\", \"IN_COLA_3\", \"IN_COLA_4\", \"IN_COLA_5\", \"IN_COLA_6\", \"IN_COLA_7\", \"IN_COLA_8\", \"OUT_COLA_1\", \"OUT_COLA_2\", \"OUT_COLA_3\", \"OUT_COLA_4\", \"OUT_COLA_5\", \"OUT_COLA_6\", \"OUT_COLA_7\", \"OUT_COLA_8\", \"FECHA\"\n"
					+ "	FROM dev_webtrafico.\"DataDiferenciada\"\n"
					+ "    WHERE \"ID_ENLACE\"= ? AND \"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')"
					+ "    ORDER BY \"FECHA\"";
		} else {
			lag = true;
			sql = "SELECT \"IN_COLA_1\", \"IN_COLA_2\", \"IN_COLA_3\", \"IN_COLA_4\", \"IN_COLA_5\", \"IN_COLA_6\", \"IN_COLA_7\", \"IN_COLA_8\", \"OUT_COLA_1\", \"OUT_COLA_2\", \"OUT_COLA_3\", \"OUT_COLA_4\", \"OUT_COLA_5\", \"OUT_COLA_6\", \"OUT_COLA_7\", \"OUT_COLA_8\", \"FECHA\"\n"
					+ "	FROM dev_webtrafico.\"DataLagsDiferenciada\"\n"
					+ "    WHERE \"ID_LAG\" = ? AND \"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')"
					+ "    ORDER BY \"FECHA\"";
		}
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			if (lag) {
				ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_lag"))));
				ps.setString(2, String.valueOf(payload.get("fecha_ini")));
				ps.setString(3, String.valueOf(payload.get("fecha_fin")));
			} else {
				ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_enlace"))));
				ps.setString(2, String.valueOf(payload.get("fecha_ini")));
				ps.setString(3, String.valueOf(payload.get("fecha_fin")));
			}

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				in_col1.add(rs.getBigDecimal(1)
						.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
				in_col2.add(rs.getBigDecimal(2)
						.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
				in_col3.add(rs.getBigDecimal(3)
						.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
				in_col4.add(rs.getBigDecimal(4)
						.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
				in_col5.add(rs.getBigDecimal(5)
						.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
				in_col6.add(rs.getBigDecimal(6)
						.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
				in_col7.add(rs.getBigDecimal(7)
						.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
				in_col8.add(rs.getBigDecimal(8)
						.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
				out_col1.add(rs.getBigDecimal(9)
						.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
				out_col2.add(rs.getBigDecimal(10)
						.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
				out_col3.add(rs.getBigDecimal(11)
						.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
				out_col4.add(rs.getBigDecimal(12)
						.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
				out_col5.add(rs.getBigDecimal(13)
						.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
				out_col6.add(rs.getBigDecimal(14)
						.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
				out_col7.add(rs.getBigDecimal(15)
						.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
				out_col8.add(rs.getBigDecimal(16)
						.multiply(new BigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP)));
				x.add(String.valueOf(rs.getTimestamp(17)));
			}
			Map<String, Object> in_cola_1 = new HashMap<String, Object>();
			Map<String, Object> in_cola_2 = new HashMap<String, Object>();
			Map<String, Object> in_cola_3 = new HashMap<String, Object>();
			Map<String, Object> in_cola_4 = new HashMap<String, Object>();
			Map<String, Object> in_cola_5 = new HashMap<String, Object>();
			Map<String, Object> in_cola_6 = new HashMap<String, Object>();
			Map<String, Object> in_cola_7 = new HashMap<String, Object>();
			Map<String, Object> in_cola_8 = new HashMap<String, Object>();
			Map<String, Object> out_cola_1 = new HashMap<String, Object>();
			Map<String, Object> out_cola_2 = new HashMap<String, Object>();
			Map<String, Object> out_cola_3 = new HashMap<String, Object>();
			Map<String, Object> out_cola_4 = new HashMap<String, Object>();
			Map<String, Object> out_cola_5 = new HashMap<String, Object>();
			Map<String, Object> out_cola_6 = new HashMap<String, Object>();
			Map<String, Object> out_cola_7 = new HashMap<String, Object>();
			Map<String, Object> out_cola_8 = new HashMap<String, Object>();
			in_cola_1.put("y", in_col1);
			in_cola_1.put("x", x);
			in_cola_1.put("tittle", "DEFAULT");
			in_cola_2.put("y", in_col2);
			in_cola_2.put("x", x);
			in_cola_2.put("tittle", "VIDEO");
			in_cola_3.put("y", in_col3);
			in_cola_3.put("x", x);
			in_cola_3.put("tittle", "GESTI�N");
			in_cola_4.put("y", in_col4);
			in_cola_4.put("x", x);
			in_cola_4.put("tittle", "CLIENTES");
			in_cola_5.put("y", in_col5);
			in_cola_5.put("x", x);
			in_cola_5.put("tittle", "CLIENTES");
			in_cola_6.put("y", in_col6);
			in_cola_6.put("x", x);
			in_cola_6.put("tittle", "CLIENTES");
			in_cola_7.put("y", in_col7);
			in_cola_7.put("x", x);
			in_cola_7.put("tittle", "VOIP");
			in_cola_8.put("y", in_col8);
			in_cola_8.put("x", x);
			in_cola_8.put("tittle", "ROUTING CONTROL");

			out_cola_1.put("y", out_col1);
			out_cola_1.put("x", x);
			out_cola_1.put("tittle", "DEFAULT");
			out_cola_2.put("y", out_col2);
			out_cola_2.put("x", x);
			out_cola_2.put("tittle", "VIDEO");
			out_cola_3.put("y", out_col3);
			out_cola_3.put("x", x);
			out_cola_3.put("tittle", "GESTI�N");
			out_cola_4.put("y", out_col4);
			out_cola_4.put("x", x);
			out_cola_4.put("tittle", "CLIENTES");
			out_cola_5.put("y", out_col5);
			out_cola_5.put("x", x);
			out_cola_5.put("tittle", "CLIENTES");
			out_cola_6.put("y", out_col6);
			out_cola_6.put("x", x);
			out_cola_6.put("tittle", "CLIENTES");
			out_cola_7.put("y", out_col7);
			out_cola_7.put("x", x);
			out_cola_7.put("tittle", "VOIP");
			out_cola_8.put("y", out_col8);
			out_cola_8.put("x", x);
			out_cola_8.put("tittle", "ROUTING CONTROL");

			rpta3.add(in_cola_1);
			rpta3.add(in_cola_2);
			rpta3.add(in_cola_3);
			rpta3.add(in_cola_4);
			rpta3.add(in_cola_5);
			rpta3.add(in_cola_6);
			rpta3.add(in_cola_7);
			rpta3.add(in_cola_8);
			rpta3.add(out_cola_1);
			rpta3.add(out_cola_2);
			rpta3.add(out_cola_3);
			rpta3.add(out_cola_4);
			rpta3.add(out_cola_5);
			rpta3.add(out_cola_6);
			rpta3.add(out_cola_7);
			rpta3.add(out_cola_8);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rpta3;
	}

	public static List<Float> DisponibilidadFechas(Connection conn, Map<String, Object> payload) {
		List<Float> rpta = new ArrayList<Float>();
		Float indis1 = 0f;
		Float indis2 = 0f;
		Float indis3 = 0f;
		Integer totsem = 1 * 60 * 24 * 7;
		Integer totmes = 1 * 60 * 24 * 30;
		Integer totanio = 1 * 60 * 24 * 365;
		boolean lag = false;
		String sql1 = "";
		String sql2 = "";
		String sql3 = "";
		if (payload.get("id_enlace") != null) {
			sql1 = "SELECT COUNT(1)\n" + "	FROM dev_webtrafico.\"Disponibilidad\"\n"
					+ "    WHERE \"ID_ENLACE\" = ? AND \"DISPONIBILIDAD\"= 'down' AND \"FECHA\" > current_timestamp - interval '7 days'";
			sql2 = "SELECT COUNT(1)\n" + "	FROM dev_webtrafico.\"Disponibilidad\"\n"
					+ "    WHERE \"ID_ENLACE\" = ? AND \"DISPONIBILIDAD\"= 'down' AND \"FECHA\" > current_timestamp - interval '30 days'";
			sql3 = "SELECT COUNT(1)\n" + "	FROM dev_webtrafico.\"Disponibilidad\"\n"
					+ "    WHERE \"ID_ENLACE\" = ? AND \"DISPONIBILIDAD\"= 'down' AND \"FECHA\" > current_timestamp - interval '365 days'";
		} else {
			lag = true;
			sql1 = "SELECT COUNT(1)\n" + "	FROM dev_webtrafico.\"DisponibilidadLag\"\n"
					+ "    WHERE \"ID_LAG\" = ? AND \"DISPONIBILIDAD\"= 4 AND \"FECHA\" > current_timestamp - interval '7 days'";
			sql2 = "SELECT COUNT(1)\n" + "	FROM dev_webtrafico.\"DisponibilidadLag\"\n"
					+ "    WHERE \"ID_LAG\" = ? AND \"DISPONIBILIDAD\"= 4 AND \"FECHA\" > current_timestamp - interval '30 days'";
			sql3 = "SELECT COUNT(1)\n" + "	FROM dev_webtrafico.\"DisponibilidadLag\"\n"
					+ "    WHERE \"ID_LAG\" = ? AND \"DISPONIBILIDAD\"= 4 AND \"FECHA\" > current_timestamp - interval '365 days'";
		}

		try {
			PreparedStatement ps1 = conn.prepareStatement(sql1);
			PreparedStatement ps2 = conn.prepareStatement(sql2);
			PreparedStatement ps3 = conn.prepareStatement(sql3);
			if (lag) {
				ps1.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_lag"))));

				ps2.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_lag"))));

				ps3.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_lag"))));

			} else {
				ps1.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_enlace"))));
				ps2.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_enlace"))));
				ps3.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_enlace"))));
			}
			ResultSet rs1 = ps1.executeQuery();
			if (rs1.next()) {
				indis1 = (float) rs1.getInt(1) / (float) totsem;
			}
			ResultSet rs2 = ps2.executeQuery();
			if (rs2.next()) {
				indis2 = (float) rs2.getInt(1) / (float) totmes;
			}
			ResultSet rs3 = ps3.executeQuery();
			if (rs3.next()) {
				indis3 = (float) rs3.getInt(1) / (float) totanio;
			}

			String concoma1 = String.format("%.3f", indis1 * 100f);
			String conpunto1 = concoma1.replace(",", ".");
			String concoma2 = String.format("%.3f", indis2 * 100f);
			String conpunto2 = concoma2.replace(",", ".");
			String concoma3 = String.format("%.3f", indis3 * 100f);
			String conpunto3 = concoma3.replace(",", ".");
			rpta.add((100f - Float.parseFloat(conpunto1)));
			rpta.add((100f - Float.parseFloat(conpunto2)));
			rpta.add((100f - Float.parseFloat(conpunto3)));

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rpta;
	}
	
	public static List<Float> DisponibilidadFechasEquipo(Connection conn, Map<String, Object> payload) {
		List<Float> rpta = new ArrayList<Float>();
		Float indis1 = 0f;
		Float indis2 = 0f;
		Float indis3 = 0f;
		
		String sql1 = "";
		String sql2 = "";
		String sql3 = "";
		
		sql1 = "SELECT ROUND((CAST (1 - (CAST (CAST (COUNT(1) AS FLOAT) / CAST ((1*60*24*7) AS FLOAT) AS FLOAT)) AS NUMERIC))*100,3)\r\n" + 
				"	FROM dev_webtrafico.\"DisponibilidadEquipo\"\r\n" + 
				"	WHERE \"ID_EQUIPO\"= ? AND \"DISPONIBILIDAD\"= 4 AND \"FECHA\" > current_timestamp - interval '7 days'";
		
		sql2 = "SELECT ROUND((CAST (1 - (CAST (CAST (COUNT(1) AS FLOAT) / CAST ((1*60*24*30) AS FLOAT) AS FLOAT)) AS NUMERIC))*100,3)\r\n" + 
				"	FROM dev_webtrafico.\"DisponibilidadEquipo\"\r\n" + 
				"	WHERE \"ID_EQUIPO\"= ? AND \"DISPONIBILIDAD\"= 4 AND \"FECHA\" > current_timestamp - interval '30 days'";
		
		sql3 = "SELECT ROUND((CAST (1 - (CAST (CAST (COUNT(1) AS FLOAT) / CAST ((1*60*24*365) AS FLOAT) AS FLOAT)) AS NUMERIC))*100,3)\r\n" + 
				"	FROM dev_webtrafico.\"DisponibilidadEquipo\"\r\n" + 
				"	WHERE \"ID_EQUIPO\"= ? AND \"DISPONIBILIDAD\"= 4 AND \"FECHA\" > current_timestamp - interval '365 days'";
		try {
			PreparedStatement ps1 = conn.prepareStatement(sql1);
			PreparedStatement ps2 = conn.prepareStatement(sql2);
			PreparedStatement ps3 = conn.prepareStatement(sql3);

			ps1.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_equipo"))));
			ps2.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_equipo"))));
			ps3.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_equipo"))));
			
			ResultSet rs1 = ps1.executeQuery();
			if (rs1.next()) {
				indis1 = rs1.getFloat(1);
			}
			ResultSet rs2 = ps2.executeQuery();
			if (rs2.next()) {
				indis2 =  rs2.getFloat(1);
			}
			ResultSet rs3 = ps3.executeQuery();
			if (rs3.next()) {
				indis3 = rs3.getFloat(1);
			}

			rpta.add(indis1);
			rpta.add(indis2);
			rpta.add(indis3);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rpta;
	}
	
	public static List<Map<String, Object>> getLags(Connection conn, Map<String, Object> payload) {
		List<Map<String, Object>> rpta = new ArrayList<Map<String, Object>>();
		String sql = "SELECT \"LAG_GROUP\", \"ID_LAG\"\n" + "FROM dev_webtrafico.\"Lags\"\n"
				+ "WHERE \"ID_EQUIPO\"=? \n" + "ORDER BY \"LAG_GROUP\"";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_equipo"))));
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Map<String, Object> lag = new HashMap<String, Object>();
				int lag_group = rs.getInt(1);
				lag.put("lag_group", lag_group);

				String concoma = String.format("%.3f", (1f - DAO.getIndis2(conn, rs.getInt(2))) * 100f);
				String conpunto = concoma.replace(",", ".");
				lag.put("disponibilidad", Float.parseFloat(conpunto));
				lag.put("id_lag", rs.getInt(2));
				rpta.add(lag);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rpta;
	}

	public static Float getIndis2(Connection conn, int id_lag) {
		Float rpta = 0.0f;
		Integer indis = 0;
		Integer tot = 1 * 60 * 24 * 7;
		String sql1 = "SELECT COUNT(1)\n" + "FROM dev_webtrafico.\"DisponibilidadLag\"\n"
				+ "WHERE \"DISPONIBILIDAD\" = 4 AND \"ID_LAG\" = ? AND \"FECHA\" > current_timestamp - interval '7 days'\n";
		try {
			PreparedStatement ps1 = conn.prepareStatement(sql1);
			ps1.setInt(1, id_lag);
			ResultSet rs1 = ps1.executeQuery();
			if (rs1.next()) {
				indis = rs1.getInt(1);
			}
			rpta = (float) ((float) indis / (float) tot);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Error !!");
			e.printStackTrace();
		}
		return rpta;
	}

	public static Map<String, Object> getAcumuladoTime(Connection conn, Map<String, Object> payload) {
		Map<String, Object> rpta = new HashMap<String, Object>();
		boolean lag = false;
		String sql1 = "";
		String sql2 = "";
		String sql3 = "";
		if (payload.get("id_enlace") != null) {
			sql1 = "SELECT COUNT(1)\n" + "	FROM dev_webtrafico.\"Disponibilidad\"\n"
					+ "    WHERE \"DISPONIBILIDAD\" = 'down' AND \"FECHA\" > current_timestamp - interval '7 days' AND\n"
					+ "    	\"ID_ENLACE\" = ?";
			sql2 = "SELECT COUNT(1)\n" + "	FROM dev_webtrafico.\"Disponibilidad\"\n"
					+ "    WHERE \"DISPONIBILIDAD\" = 'down' AND \"FECHA\" > current_timestamp - interval '30 days' AND\n"
					+ "    	\"ID_ENLACE\" = ? ";
			sql3 = "SELECT COUNT(1)\n" + "	FROM dev_webtrafico.\"Disponibilidad\"\n"
					+ "    WHERE \"DISPONIBILIDAD\" = 'down' AND \"FECHA\" > current_timestamp - interval '365 days' AND\n"
					+ "    	\"ID_ENLACE\" = ?";
		} else {
			lag = true;
			sql1 = "SELECT COUNT(1)\n" + "	FROM dev_webtrafico.\"DisponibilidadLag\"\n"
					+ "    WHERE \"FECHA\" > current_timestamp - interval '7 days' AND\n" + "    	\"ID_LAG\" = ?";
			sql2 = "SELECT COUNT(1)\n" + "	FROM dev_webtrafico.\"DisponibilidadLag\"\n"
					+ "    WHERE \"FECHA\" > current_timestamp - interval '30 days' AND\n" + "    	\"ID_LAG\" = ?";
			sql3 = "SELECT COUNT(1)\n" + "	FROM dev_webtrafico.\"DisponibilidadLag\"\n"
					+ "    WHERE \"FECHA\" > current_timestamp - interval '365 days' AND\n" + "    	\"ID_LAG\" = ?";
		}

		try {
			PreparedStatement ps1 = conn.prepareStatement(sql1);
			PreparedStatement ps2 = conn.prepareStatement(sql2);
			PreparedStatement ps3 = conn.prepareStatement(sql3);

			if (lag) {
				ps1.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_lag"))));

				ps2.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_lag"))));

				ps3.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_lag"))));

			} else {
				ps1.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_enlace"))));
				ps2.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_enlace"))));
				ps3.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_enlace"))));
			}

			ResultSet rs1 = ps1.executeQuery();
			ResultSet rs2 = ps2.executeQuery();
			ResultSet rs3 = ps3.executeQuery();

			if (rs1.next()) {
				String formated = formatdate(rs1.getInt(1) * 1000 * 60);
				rpta.put("t_sem", formated);
			}
			if (rs2.next()) {
				String formated = formatdate(rs2.getInt(1) * 1000 * 60);
				rpta.put("t_mes", formated);
			}
			if (rs3.next()) {
				String formated = formatdate(rs3.getInt(1) * 1000 * 60);
				rpta.put("t_ani", formated);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rpta;
	}
	
	public static Map<String, Object> getAcumuladoTimeEquipo(Connection conn, Map<String, Object> payload) {
		Map<String, Object> rpta = new HashMap<String, Object>();
		
		String sql1 = "";
		String sql2 = "";
		String sql3 = "";
		
		sql1 = "SELECT COUNT(1)\n" + "	FROM dev_webtrafico.\"DisponibilidadEquipo\"\n"
					+ "    WHERE \"DISPONIBILIDAD\" = 4 AND \"FECHA\" > current_timestamp - interval '7 days' AND\n"
					+ "    	\"ID_EQUIPO\" = ?";
			sql2 = "SELECT COUNT(1)\n" + "	FROM dev_webtrafico.\"DisponibilidadEquipo\"\n"
					+ "    WHERE \"DISPONIBILIDAD\" = 4 AND \"FECHA\" > current_timestamp - interval '30 days' AND\n"
					+ "    	\"ID_EQUIPO\" = ? ";
			sql3 = "SELECT COUNT(1)\n" + "	FROM dev_webtrafico.\"DisponibilidadEquipo\"\n"
					+ "    WHERE \"DISPONIBILIDAD\" = 4 AND \"FECHA\" > current_timestamp - interval '365 days' AND\n"
					+ "    	\"ID_EQUIPO\" = ?";

		try {
			PreparedStatement ps1 = conn.prepareStatement(sql1);
			PreparedStatement ps2 = conn.prepareStatement(sql2);
			PreparedStatement ps3 = conn.prepareStatement(sql3);

			ps1.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_equipo"))));
			ps2.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_equipo"))));
			ps3.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_equipo"))));

			ResultSet rs1 = ps1.executeQuery();
			ResultSet rs2 = ps2.executeQuery();
			ResultSet rs3 = ps3.executeQuery();

			if (rs1.next()) {
				String formated = formatdate(rs1.getInt(1) * 1000 * 60);
				rpta.put("t_sem", formated);
			}
			if (rs2.next()) {
				String formated = formatdate(rs2.getInt(1) * 1000 * 60);
				rpta.put("t_mes", formated);
			}
			if (rs3.next()) {
				String formated = formatdate(rs3.getInt(1) * 1000 * 60);
				rpta.put("t_ani", formated);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rpta;
	}
	
	public static Map<String, Object> dispoInterval(Connection conn, Map<String, Object> payload) {
		Map<String, Object> rpta = new HashMap<String, Object>();
		String sql1 = "";
		boolean lag = false;
		if (payload.get("id_enlace") != null) {
			sql1 = "SELECT COUNT(1) FROM dev_webtrafico.\"Disponibilidad\" WHERE \"DISPONIBILIDAD\" = 'down' AND \"ID_ENLACE\" = ? AND \"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')";
		} else {
			lag = true;
			sql1 = "SELECT COUNT(1) FROM dev_webtrafico.\"DisponibilidadLag\" WHERE \"ID_LAG\" = ?  AND \"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')";
		}
		long indis = 0;
		try {
			PreparedStatement ps1 = conn.prepareStatement(sql1);
			if (lag) {
				ps1.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_lag"))));
				ps1.setString(2, String.valueOf(payload.get("fecha_ini")));
				ps1.setString(3, String.valueOf(payload.get("fecha_fin")));
			} else {
				ps1.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_enlace"))));
				ps1.setString(2, String.valueOf(payload.get("fecha_ini")));
				ps1.setString(3, String.valueOf(payload.get("fecha_fin")));
			}

			ResultSet rs1 = ps1.executeQuery();
			if (rs1.next()) {
				indis = rs1.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Timestamp ini = dateFormater(String.valueOf(payload.get("fecha_ini")));
		Timestamp fin = dateFormater(String.valueOf(payload.get("fecha_fin")));

		Float dif_inmilis = (float) (fin.getTime() - ini.getTime());
		Float dif_mins = dif_inmilis / (1000 * 60);

		Float disponibilidad = (float) ((float) indis / (float) dif_mins);

		String concoma = String.format("%.3f", (1f - disponibilidad) * 100f);
		String conpunto = concoma.replace(",", ".");

		rpta.put("dispo", Float.parseFloat(conpunto));
		rpta.put("tiempo_acum", formatdate(indis * 1000 * 60));
		List<Map<String, Object>> eventos = getEventos(conn, payload);
		rpta.put("eventos", eventos);
		return rpta;
	}
	
	public static Map<String, Object> dispoIntervalEquipo(Connection conn, Map<String, Object> payload) {
		Map<String, Object> rpta = new HashMap<String, Object>();
		String sql1 = "";

		sql1 = "SELECT COUNT(1) FROM dev_webtrafico.\"DisponibilidadEquipo\""
				+ "WHERE \"DISPONIBILIDAD\" = 4 AND \"ID_EQUIPO\" = ? AND"
				+ "\"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND"
				+ "\"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')";
		
		long indis = 0;
		try {
			PreparedStatement ps1 = conn.prepareStatement(sql1);

			ps1.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_equipo"))));
			ps1.setString(2, String.valueOf(payload.get("fecha_ini")));
			ps1.setString(3, String.valueOf(payload.get("fecha_fin")));

			ResultSet rs1 = ps1.executeQuery();
			if (rs1.next()) {
				indis = rs1.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Timestamp ini = dateFormater(String.valueOf(payload.get("fecha_ini")));
		Timestamp fin = dateFormater(String.valueOf(payload.get("fecha_fin")));

		Float dif_inmilis = (float) (fin.getTime() - ini.getTime());
		Float dif_mins = dif_inmilis / (1000 * 60);

		Float disponibilidad = (float) ((float) indis / (float) dif_mins);

		String concoma = String.format("%.3f", (1f - disponibilidad) * 100f);
		String conpunto = concoma.replace(",", ".");

		rpta.put("dispo", Float.parseFloat(conpunto));
		rpta.put("tiempo_acum", formatdate(indis * 1000 * 60));
		List<Map<String, Object>> eventos = getEventosEquipos(conn, payload);
		rpta.put("eventos", eventos);
		return rpta;
	}
	
	private static Timestamp dateFormater(String date) {
		Timestamp timestamp = null;
		try {
			SimpleDateFormat dateFormat;
			if (date.length() > 11) {
				dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
			} else {
				dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			}
			Date parsedDate = dateFormat.parse(date);
			timestamp = new java.sql.Timestamp(parsedDate.getTime());
		} catch (Exception e) { // this generic but you can control another types of exception
			// look the origin of excption
		}
		return timestamp;
	}

	private static List<Map<String, Object>> getEventos(Connection conn, Map<String, Object> payload) {
		List<Map<String, Object>> rpta = new ArrayList<Map<String, Object>>();
		String sql = "";
		boolean lag = false;
		if (payload.get("id_enlace") != null) {
			sql = "SELECT \"FECHA\", current_timestamp \n" + "FROM dev_webtrafico.\"Disponibilidad\"\n"
					+ "WHERE \"DISPONIBILIDAD\"= 'down' AND \"ID_ENLACE\"= ? AND \"FECHA\" > to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" <= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') \n"
					+ "ORDER BY \"FECHA\"";
		} else {
			lag = true;
			sql = "SELECT \"FECHA\", current_timestamp \n" + "FROM dev_webtrafico.\"DisponibilidadLag\"\n"
					+ "WHERE \"DISPONIBILIDAD\"= 4 AND \"ID_LAG\" = ? AND \"FECHA\" > to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" <= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') \n"
					+ "ORDER BY \"FECHA\"";
		}

		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			if (lag) {
				ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_lag"))));
				ps.setString(2, String.valueOf(payload.get("fecha_ini")));
				ps.setString(3, String.valueOf(payload.get("fecha_fin")));
			} else {
				ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_enlace"))));
				ps.setString(2, String.valueOf(payload.get("fecha_ini")));
				ps.setString(3, String.valueOf(payload.get("fecha_fin")));
			}

			ResultSet rs = ps.executeQuery();
			List<List<Timestamp>> eventos = new ArrayList<List<Timestamp>>();
			List<Timestamp> event = new ArrayList<Timestamp>();
			Timestamp currentTS = null;
			while (rs.next()) {
				currentTS = rs.getTimestamp(2);
				if (event.isEmpty()) {
					event.add(rs.getTimestamp(1));
					if (rs.isLast()) {
						List<Timestamp> temp = new ArrayList<Timestamp>(event);
						eventos.add(temp);
					}
				} else {
					if (rs.getTimestamp(1).getTime() - event.get(event.size() - 1).getTime() < 120000) {
						event.add(rs.getTimestamp(1));
						if (rs.isLast()) {
							List<Timestamp> temp = new ArrayList<Timestamp>(event);
							eventos.add(temp);
						}
					} else {
						List<Timestamp> temp = new ArrayList<Timestamp>(event);
						eventos.add(temp);
						event.clear();
						event.add(rs.getTimestamp(1));
						if (rs.isLast()) {
							List<Timestamp> temp2 = new ArrayList<Timestamp>(event);
							eventos.add(temp2);
						}
					}
				}
			}
			for (List<Timestamp> e : eventos) {
				Map<String, Object> eve = new HashMap<String, Object>();
				eve.put("inicio", String.valueOf(e.get(0)));
				Timestamp t = e.get(e.size() - 1);
				if (currentTS.getTime() - t.getTime() < 80000) {
					eve.put("fin", String.valueOf("A�n indisponible"));
				} else {
					eve.put("fin", String.valueOf(e.get(e.size() - 1)));
				}
				if (String.valueOf(e.get(0)).equals(String.valueOf(e.get(e.size() - 1)))) {
					String formated = formatdate(60000);
					Timestamp t2 = new Timestamp(e.get(0).getTime() + 60000);
					eve.put("fin", String.valueOf(t2));
					eve.put("duracion", formated);
					rpta.add(eve);
				} else {
					String formated = formatdate(e.get(e.size() - 1).getTime() - e.get(0).getTime() + 60000);
					eve.put("duracion", formated);
					rpta.add(eve);
				}
			}
			Collections.reverse(rpta);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rpta;
	}
	
	private static List<Map<String, Object>> getEventosEquipos(Connection conn, Map<String, Object> payload) {
		List<Map<String, Object>> rpta = new ArrayList<Map<String, Object>>();
		String sql = "";

		sql = "SELECT \"FECHA\", current_timestamp \n" + "FROM dev_webtrafico.\"DisponibilidadEquipo\"\n"
					+ "WHERE \"DISPONIBILIDAD\"= 4 AND \"ID_EQUIPO\"= ? AND \"FECHA\" > to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" <= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') \n"
					+ "ORDER BY \"FECHA\"";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);

			ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_equipo"))));
			ps.setString(2, String.valueOf(payload.get("fecha_ini")));
			ps.setString(3, String.valueOf(payload.get("fecha_fin")));


			ResultSet rs = ps.executeQuery();
			List<List<Timestamp>> eventos = new ArrayList<List<Timestamp>>();
			List<Timestamp> event = new ArrayList<Timestamp>();
			Timestamp currentTS = null;
			while (rs.next()) {
				currentTS = rs.getTimestamp(2);
				if (event.isEmpty()) {
					event.add(rs.getTimestamp(1));
					if (rs.isLast()) {
						List<Timestamp> temp = new ArrayList<Timestamp>(event);
						eventos.add(temp);
					}
				} else {
					if (rs.getTimestamp(1).getTime() - event.get(event.size() - 1).getTime() < 120000) {
						event.add(rs.getTimestamp(1));
						if (rs.isLast()) {
							List<Timestamp> temp = new ArrayList<Timestamp>(event);
							eventos.add(temp);
						}
					} else {
						List<Timestamp> temp = new ArrayList<Timestamp>(event);
						eventos.add(temp);
						event.clear();
						event.add(rs.getTimestamp(1));
						if (rs.isLast()) {
							List<Timestamp> temp2 = new ArrayList<Timestamp>(event);
							eventos.add(temp2);
						}
					}
				}
			}
			for (List<Timestamp> e : eventos) {
				Map<String, Object> eve = new HashMap<String, Object>();
				eve.put("inicio", String.valueOf(e.get(0)));
				Timestamp t = e.get(e.size() - 1);
				if (currentTS.getTime() - t.getTime() < 80000) {
					eve.put("fin", String.valueOf("A�n indisponible"));
				} else {
					eve.put("fin", String.valueOf(e.get(e.size() - 1)));
				}
				if (String.valueOf(e.get(0)).equals(String.valueOf(e.get(e.size() - 1)))) {
					String formated = formatdate(60000);
					Timestamp t2 = new Timestamp(e.get(0).getTime() + 60000);
					eve.put("fin", String.valueOf(t2));
					eve.put("duracion", formated);
					rpta.add(eve);
				} else {
					String formated = formatdate(e.get(e.size() - 1).getTime() - e.get(0).getTime() + 60000);
					eve.put("duracion", formated);
					rpta.add(eve);
				}
			}
			Collections.reverse(rpta);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rpta;
	}

	public static List<Map<String, Object>> getCaidos(Connection conn, Map<String, Object> payload) {
		List<Map<String, Object>> rpta = new ArrayList<Map<String, Object>>();
		String sql = "SELECT EQ.\"ID_EQUIPO\", '[ '||\"TIPO\" ||'] '|| \"LOCALIDAD\" ||' '|| \"INTERFAZ\" AS \"ORIGEN\", '[ '||(\r\n"
				+ "    CASE WHEN \"TIPO\"='CORE' THEN 'CORE'\r\n" + "    WHEN \"TIPO\" = 'AGREGADOR' THEN 'CORE'\r\n"
				+ "    WHEN \"TIPO\" = 'DISTRIBUIDOR' THEN 'AGREGADOR'\r\n"
				+ "    WHEN \"TIPO\" = 'CONEXI�N' THEN 'DISTRIBUIDOR'\r\n"
				+ "	END) || '] ' || \"DESTINO\" || ' / ' || \"INTERFAZ_DESTINO\" AS \"DESTINO\", \"ID_ENLACE\"\r\n"
				+ "FROM dev_webtrafico.\"Equipos\" eq\r\n" + "JOIN dev_webtrafico.\"Enlaces\" en\r\n"
				+ "ON eq.\"ID_EQUIPO\" = en.\"ID_EQUIPO\"\r\n" + "JOIN dev_webtrafico.\"Ubigeo\" ub\r\n"
				+ "ON ub.\"ID_EQUIPO\" = eq.\"ID_EQUIPO\"\r\n" + "JOIN dev_webtrafico.\"Interfaces\" intf\r\n"
				+ "ON en.\"ID_INTERFAZ\" = intf.\"ID_INTERFAZ\"\r\n" + "WHERE en.\"ID_ENLACE\" IN (\r\n"
				+ "	SELECT \"ID_ENLACE\"\r\n" + "	FROM dev_webtrafico.\"Disponibilidad\"\r\n"
				+ "    WHERE \"FECHA\" > current_timestamp - interval '1 minute'\r\n"
				+ "	) ORDER BY eq.\"ID_EQUIPO\" DESC;";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Map<String, Object> caido = new HashMap<String, Object>();
				caido.put("id_equipo", rs.getInt(1));
				caido.put("origen", rs.getString(2));
				caido.put("destino", rs.getString(3));
				caido.put("id_enlace", rs.getInt(4));
				caido.put("indisponibilidad", DAO.getIndis(conn, rs.getInt(4)));
				rpta.add(caido);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rpta;
	}
	
	public static List<Map<String, Object>> getDrops(Connection conn, Map<String, Object> payload) {
		List<Map<String, Object>> lstR = new ArrayList<Map<String, Object>>();
		List<BigDecimal> y_in_cola_1 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_in_cola_2 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_in_cola_3 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_in_cola_4 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_in_cola_5 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_in_cola_6 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_in_cola_7 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_in_cola_8 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_out_cola_1 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_out_cola_2 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_out_cola_3 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_out_cola_4 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_out_cola_5 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_out_cola_6 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_out_cola_7 = new ArrayList<BigDecimal>();
		List<BigDecimal> y_out_cola_8 = new ArrayList<BigDecimal>();
		List<String> x = new ArrayList<String>();
		String sql = "";
		boolean lag = false;
		if (payload.get("tipo").equals("CONEXI�N") || payload.get("tipo").equals("DISTRIBUIDOR")) {

			if (payload.get("id_enlace") != null) {
				sql = "SELECT \"OUT_DROP_1\", \"OUT_DROP_2\", \"OUT_DROP_3\", \"OUT_DROP_4\", \"OUT_DROP_5\", \"OUT_DROP_6\", \"OUT_DROP_7\", \"OUT_DROP_8\", \"FECHA\"\n"
						+ "	FROM dev_webtrafico.\"DataDiferenciada\"\n" + " WHERE \"ID_ENLACE\"=? AND \"FECHA\" >= current_timestamp - interval '7 days' ORDER BY \"FECHA\"";
			} else {
				lag = true;
				sql = "SELECT \"OUT_DROP_1\", \"OUT_DROP_2\", \"OUT_DROP_3\", \"OUT_DROP_4\", \"OUT_DROP_5\", \"OUT_DROP_6\", \"OUT_DROP_7\", \"OUT_DROP_8\", \"FECHA\"\n"
						+ "	FROM dev_webtrafico.\"DataLagsDiferenciada\"\n"
						+ " WHERE \"ID_LAG\" = ? AND \"FECHA\" >= current_timestamp - interval '7 days' ORDER BY \"FECHA\"";
			}
			
			try {
				PreparedStatement stmt = conn.prepareStatement(sql);
				if (lag) {
					stmt.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_lag"))));
				} else {
					stmt.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_enlace"))));
				}

				ResultSet rs = stmt.executeQuery();
				while (rs.next()) {
					if (rs.getBigDecimal(1) == null) {
						y_out_cola_1.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_2.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_3.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_4.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_5.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_6.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_7.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_8.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					} else {
						y_out_cola_1.add(rs.getBigDecimal(1).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_2.add(rs.getBigDecimal(2).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_3.add(rs.getBigDecimal(3).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_4.add(rs.getBigDecimal(4).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_5.add(rs.getBigDecimal(5).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_6.add(rs.getBigDecimal(6).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_7.add(rs.getBigDecimal(7).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_8.add(rs.getBigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					}
					x.add(String.valueOf(rs.getTimestamp(9)));
				}
				Map<String, Object> cola1out = new HashMap<String, Object>();
				Map<String, Object> cola2out = new HashMap<String, Object>();
				Map<String, Object> cola3out = new HashMap<String, Object>();
				Map<String, Object> cola4out = new HashMap<String, Object>();
				Map<String, Object> cola5out = new HashMap<String, Object>();
				Map<String, Object> cola6out = new HashMap<String, Object>();
				Map<String, Object> cola7out = new HashMap<String, Object>();
				Map<String, Object> cola8out = new HashMap<String, Object>();

				cola1out.put("tipo", "DEFAULT");
				cola1out.put("y", y_out_cola_1);
				cola1out.put("x", x);
				lstR.add(cola1out);
				cola2out.put("tipo", "VIDEO");
				cola2out.put("y", y_out_cola_2);
				cola2out.put("x", x);
				lstR.add(cola2out);
				cola3out.put("tipo", "GESTI�N");
				cola3out.put("y", y_out_cola_3);
				cola3out.put("x", x);
				lstR.add(cola3out);
				cola4out.put("tipo", "CLIENTES");
				cola4out.put("y", y_out_cola_4);
				cola4out.put("x", x);
				lstR.add(cola4out);
				cola5out.put("tipo", "CLIENTES");
				cola5out.put("y", y_out_cola_5);
				cola5out.put("x", x);
				lstR.add(cola5out);
				cola6out.put("tipo", "CLIENTES");
				cola6out.put("y", y_out_cola_6);
				cola6out.put("x", x);
				lstR.add(cola6out);
				cola7out.put("tipo", "VOIP");
				cola7out.put("y", y_out_cola_7);
				cola7out.put("x", x);
				lstR.add(cola7out);
				cola8out.put("tipo", "ROUTING CONTROL");
				cola8out.put("y", y_out_cola_8);
				cola8out.put("x", x);
				lstR.add(cola8out);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else {
			boolean lag2 = false;
			String sql1 = "";
			if (payload.get("id_enlace") != null) {
				sql1 = "SELECT \"IN_DROP_1\", \"IN_DROP_2\", \"IN_DROP_3\", \"IN_DROP_4\", \"IN_DROP_5\", \"IN_DROP_6\", \"IN_DROP_7\", \"IN_DROP_8\", \"OUT_DROP_1\", \"OUT_DROP_2\", \"OUT_DROP_3\", \"OUT_DROP_4\", \"OUT_DROP_5\", \"OUT_DROP_6\", \"OUT_DROP_7\", \"OUT_DROP_8\", \"FECHA\"\n"
						+ "	FROM dev_webtrafico.\"DataDiferenciada\"\n" + " WHERE \"ID_ENLACE\"=? AND \"FECHA\" > current_timestamp - interval '7 days' ORDER BY \"FECHA\"";
			} else {
				lag2 = true;
				sql1 = "SELECT \"IN_DROP_1\", \"IN_DROP_2\", \"IN_DROP_3\", \"IN_DROP_4\", \"IN_DROP_5\", \"IN_DROP_6\", \"IN_DROP_7\", \"IN_DROP_8\", \"OUT_DROP_1\", \"OUT_DROP_2\", \"OUT_DROP_3\", \"OUT_DROP_4\", \"OUT_DROP_5\", \"OUT_DROP_6\", \"OUT_DROP_7\", \"OUT_DROP_8\", \"FECHA\"\n"
						+ "	FROM dev_webtrafico.\"DataLagsDiferenciada\"\n"
						+ " WHERE \"ID_LAG\" = ? AND \"FECHA\" > current_timestamp - interval '7 days' ORDER BY \"FECHA\"";
			}
			try {
				PreparedStatement ps = conn.prepareStatement(sql1);
				if (lag2) {
					ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_lag"))));
				} else {
					ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_enlace"))));
				}

				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					if (rs.getBigDecimal(1) == null) {
						y_in_cola_1.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_in_cola_2.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_in_cola_3.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_in_cola_4.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_in_cola_5.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_in_cola_6.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_in_cola_7.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_in_cola_8.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_1.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_2.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_3.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_4.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_5.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_6.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_7.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_8.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					} else {
						y_in_cola_1.add(rs.getBigDecimal(1).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_in_cola_2.add(rs.getBigDecimal(2).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_in_cola_3.add(rs.getBigDecimal(3).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_in_cola_4.add(rs.getBigDecimal(4).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_in_cola_5.add(rs.getBigDecimal(5).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_in_cola_6.add(rs.getBigDecimal(6).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_in_cola_7.add(rs.getBigDecimal(7).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_in_cola_8.add(rs.getBigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_1.add(rs.getBigDecimal(9).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_2.add(rs.getBigDecimal(10).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_3.add(rs.getBigDecimal(11).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_4.add(rs.getBigDecimal(12).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_5.add(rs.getBigDecimal(13).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_6.add(rs.getBigDecimal(14).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_7.add(rs.getBigDecimal(15).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
						y_out_cola_8.add(rs.getBigDecimal(16).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					}
					x.add(String.valueOf(rs.getTimestamp(17)));
				}
				Map<String, Object> cola1in = new HashMap<String, Object>();
				Map<String, Object> cola2in = new HashMap<String, Object>();
				Map<String, Object> cola3in = new HashMap<String, Object>();
				Map<String, Object> cola4in = new HashMap<String, Object>();
				Map<String, Object> cola5in = new HashMap<String, Object>();
				Map<String, Object> cola6in = new HashMap<String, Object>();
				Map<String, Object> cola7in = new HashMap<String, Object>();
				Map<String, Object> cola8in = new HashMap<String, Object>();
				Map<String, Object> cola1out = new HashMap<String, Object>();
				Map<String, Object> cola2out = new HashMap<String, Object>();
				Map<String, Object> cola3out = new HashMap<String, Object>();
				Map<String, Object> cola4out = new HashMap<String, Object>();
				Map<String, Object> cola5out = new HashMap<String, Object>();
				Map<String, Object> cola6out = new HashMap<String, Object>();
				Map<String, Object> cola7out = new HashMap<String, Object>();
				Map<String, Object> cola8out = new HashMap<String, Object>();

				cola1in.put("tipo", "DEFAULT");
				cola1in.put("y", y_in_cola_1);
				cola1in.put("x", x);
				lstR.add(cola1in);
				cola2in.put("tipo", "VIDEO");
				cola2in.put("y", y_in_cola_2);
				cola2in.put("x", x);
				lstR.add(cola2in);
				cola3in.put("tipo", "GESTI�N");
				cola3in.put("y", y_in_cola_3);
				cola3in.put("x", x);
				lstR.add(cola3in);
				cola4in.put("tipo", "CLIENTES");
				cola4in.put("y", y_in_cola_4);
				cola4in.put("x", x);
				lstR.add(cola4in);
				cola5in.put("tipo", "CLIENTES");
				cola5in.put("y", y_in_cola_5);
				cola5in.put("x", x);
				lstR.add(cola5in);
				cola6in.put("tipo", "CLIENTES");
				cola6in.put("y", y_in_cola_6);
				cola6in.put("x", x);
				lstR.add(cola6in);
				cola7in.put("tipo", "VOIP");
				cola7in.put("y", y_in_cola_7);
				cola7in.put("x", x);
				lstR.add(cola7in);
				cola8in.put("tipo", "ROUTING CONTROL");
				cola8in.put("y", y_in_cola_8);
				cola8in.put("x", x);
				lstR.add(cola8in);

				cola1out.put("tipo", "DEFAULT");
				cola1out.put("y", y_out_cola_1);
				cola1out.put("x", x);
				lstR.add(cola1out);
				cola2out.put("tipo", "VIDEO");
				cola2out.put("y", y_out_cola_2);
				cola2out.put("x", x);
				lstR.add(cola2out);
				cola3out.put("tipo", "GESTI�N");
				cola3out.put("y", y_out_cola_3);
				cola3out.put("x", x);
				lstR.add(cola3out);
				cola4out.put("tipo", "CLIENTES");
				cola4out.put("y", y_out_cola_4);
				cola4out.put("x", x);
				lstR.add(cola4out);
				cola5out.put("tipo", "CLIENTES");
				cola5out.put("y", y_out_cola_5);
				cola5out.put("x", x);
				lstR.add(cola5out);
				cola6out.put("tipo", "CLIENTES");
				cola6out.put("y", y_out_cola_6);
				cola6out.put("x", x);
				lstR.add(cola6out);
				cola7out.put("tipo", "VOIP");
				cola7out.put("y", y_out_cola_7);
				cola7out.put("x", x);
				lstR.add(cola7out);
				cola8out.put("tipo", "ROUTING CONTROL");
				cola8out.put("y", y_out_cola_8);
				cola8out.put("x", x);
				lstR.add(cola8out);

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return lstR;
	}

	public static Map<String, Object> getTable(Connection conn, Map<String, Object> payload) {
		Map<String, Object> d = new HashMap<String, Object>();
		String sql = "";
		boolean lag = false;
		if (payload.get("id_enlace") != null) {
			sql = "SELECT\n"
					+ "MIN(\"IN_COLA_1\")*8/300, ROUND(AVG(\"IN_COLA_1\")*8/300,4), MAX(\"IN_COLA_1\")*8/300, STDDEV(\"IN_COLA_1\")*8/300,\n"
					+ "MIN(\"IN_COLA_2\")*8/300, ROUND(AVG(\"IN_COLA_2\")*8/300,4), MAX(\"IN_COLA_2\")*8/300, STDDEV(\"IN_COLA_2\")*8/300,\n"
					+ "MIN(\"IN_COLA_3\")*8/300, ROUND(AVG(\"IN_COLA_3\")*8/300,4), MAX(\"IN_COLA_3\")*8/300, STDDEV(\"IN_COLA_3\")*8/300,\n"
					+ "MIN(\"IN_COLA_4\")*8/300, ROUND(AVG(\"IN_COLA_4\")*8/300,4), MAX(\"IN_COLA_4\")*8/300, STDDEV(\"IN_COLA_4\")*8/300,\n"
					+ "MIN(\"IN_COLA_5\")*8/300, ROUND(AVG(\"IN_COLA_5\")*8/300,4), MAX(\"IN_COLA_5\")*8/300, STDDEV(\"IN_COLA_5\")*8/300,\n"
					+ "MIN(\"IN_COLA_6\")*8/300, ROUND(AVG(\"IN_COLA_6\")*8/300,4), MAX(\"IN_COLA_6\")*8/300, STDDEV(\"IN_COLA_6\")*8/300,\n"
					+ "MIN(\"IN_COLA_7\")*8/300, ROUND(AVG(\"IN_COLA_7\")*8/300,4), MAX(\"IN_COLA_7\")*8/300, STDDEV(\"IN_COLA_7\")*8/300,\n"
					+ "MIN(\"IN_COLA_8\")*8/300, ROUND(AVG(\"IN_COLA_8\")*8/300,4), MAX(\"IN_COLA_8\")*8/300, STDDEV(\"IN_COLA_8\")*8/300,\n"
					+ "MIN(\"OUT_COLA_1\")*8/300, ROUND(AVG(\"OUT_COLA_1\")*8/300,4), MAX(\"OUT_COLA_1\")*8/300, STDDEV(\"OUT_COLA_1\")*8/300,\n"
					+ "MIN(\"OUT_COLA_2\")*8/300, ROUND(AVG(\"OUT_COLA_2\")*8/300,4), MAX(\"OUT_COLA_2\")*8/300, STDDEV(\"OUT_COLA_2\")*8/300,\n"
					+ "MIN(\"OUT_COLA_3\")*8/300, ROUND(AVG(\"OUT_COLA_3\")*8/300,4), MAX(\"OUT_COLA_3\")*8/300, STDDEV(\"OUT_COLA_3\")*8/300,\n"
					+ "MIN(\"OUT_COLA_4\")*8/300, ROUND(AVG(\"OUT_COLA_4\")*8/300,4), MAX(\"OUT_COLA_4\")*8/300, STDDEV(\"OUT_COLA_4\")*8/300,\n"
					+ "MIN(\"OUT_COLA_5\")*8/300, ROUND(AVG(\"OUT_COLA_5\")*8/300,4), MAX(\"OUT_COLA_5\")*8/300, STDDEV(\"OUT_COLA_5\")*8/300,\n"
					+ "MIN(\"OUT_COLA_6\")*8/300, ROUND(AVG(\"OUT_COLA_6\")*8/300,4), MAX(\"OUT_COLA_6\")*8/300, STDDEV(\"OUT_COLA_6\")*8/300,\n"
					+ "MIN(\"OUT_COLA_7\")*8/300, ROUND(AVG(\"OUT_COLA_7\")*8/300,4), MAX(\"OUT_COLA_7\")*8/300, STDDEV(\"OUT_COLA_7\")*8/300,\n"
					+ "MIN(\"OUT_COLA_8\")*8/300, ROUND(AVG(\"OUT_COLA_8\")*8/300,4), MAX(\"OUT_COLA_8\")*8/300, STDDEV(\"OUT_COLA_8\")*8/300,\n"
					+ "MIN(\"IN_DROP_1\")/300, ROUND(AVG(\"IN_DROP_1\")/300,4), MAX(\"IN_DROP_1\")/300, STDDEV(\"IN_DROP_1\")/300,\n"
					+ "MIN(\"IN_DROP_2\")/300, ROUND(AVG(\"IN_DROP_2\")/300,4), MAX(\"IN_DROP_2\")/300, STDDEV(\"IN_DROP_2\")/300,\n"
					+ "MIN(\"IN_DROP_3\")/300, ROUND(AVG(\"IN_DROP_3\")/300,4), MAX(\"IN_DROP_3\")/300, STDDEV(\"IN_DROP_3\")/300,\n"
					+ "MIN(\"IN_DROP_4\")/300, ROUND(AVG(\"IN_DROP_4\")/300,4), MAX(\"IN_DROP_4\")/300, STDDEV(\"IN_DROP_4\")/300,\n"
					+ "MIN(\"IN_DROP_5\")/300, ROUND(AVG(\"IN_DROP_5\")/300,4), MAX(\"IN_DROP_5\")/300, STDDEV(\"IN_DROP_5\")/300,\n"
					+ "MIN(\"IN_DROP_6\")/300, ROUND(AVG(\"IN_DROP_6\")/300,4), MAX(\"IN_DROP_6\")/300, STDDEV(\"IN_DROP_6\")/300,\n"
					+ "MIN(\"IN_DROP_7\")/300, ROUND(AVG(\"IN_DROP_7\")/300,4), MAX(\"IN_DROP_7\")/300, STDDEV(\"IN_DROP_7\")/300,\n"
					+ "MIN(\"IN_DROP_8\")/300, ROUND(AVG(\"IN_DROP_8\")/300,4), MAX(\"IN_DROP_8\")/300, STDDEV(\"IN_DROP_8\")/300,\n"
					+ "MIN(\"OUT_DROP_1\")/300, ROUND(AVG(\"OUT_DROP_1\")/300,4), MAX(\"OUT_DROP_1\")/300, STDDEV(\"OUT_DROP_1\")/300,\n"
					+ "MIN(\"OUT_DROP_2\")/300, ROUND(AVG(\"OUT_DROP_2\")/300,4), MAX(\"OUT_DROP_2\")/300, STDDEV(\"OUT_DROP_2\")/300,\n"
					+ "MIN(\"OUT_DROP_3\")/300, ROUND(AVG(\"OUT_DROP_3\")/300,4), MAX(\"OUT_DROP_3\")/300, STDDEV(\"OUT_DROP_3\")/300,\n"
					+ "MIN(\"OUT_DROP_4\")/300, ROUND(AVG(\"OUT_DROP_4\")/300,4), MAX(\"OUT_DROP_4\")/300, STDDEV(\"OUT_DROP_4\")/300,\n"
					+ "MIN(\"OUT_DROP_5\")/300, ROUND(AVG(\"OUT_DROP_5\")/300,4), MAX(\"OUT_DROP_5\")/300, STDDEV(\"OUT_DROP_5\")/300,\n"
					+ "MIN(\"OUT_DROP_6\")/300, ROUND(AVG(\"OUT_DROP_6\")/300,4), MAX(\"OUT_DROP_6\")/300, STDDEV(\"OUT_DROP_6\")/300,\n"
					+ "MIN(\"OUT_DROP_7\")/300, ROUND(AVG(\"OUT_DROP_7\")/300,4), MAX(\"OUT_DROP_7\")/300, STDDEV(\"OUT_DROP_7\")/300,\n"
					+ "MIN(\"OUT_DROP_8\")/300, ROUND(AVG(\"OUT_DROP_8\")/300,4), MAX(\"OUT_DROP_8\")/300, STDDEV(\"OUT_DROP_8\")/300\n"
					+ "FROM dev_webtrafico.\"DataDiferenciada\"\n"
					+ "WHERE \"ID_ENLACE\" = ? AND \"FECHA\" >= current_timestamp - interval '7 days' ";
		} else {
			lag = true;
			sql = "SELECT\n"
					+ "MIN(\"IN_COLA_1\")*8/300, ROUND(AVG(\"IN_COLA_1\")*8/300,4), MAX(\"IN_COLA_1\")*8/300, STDDEV(\"IN_COLA_1\")*8/300,\n"
					+ "MIN(\"IN_COLA_2\")*8/300, ROUND(AVG(\"IN_COLA_2\")*8/300,4), MAX(\"IN_COLA_2\")*8/300, STDDEV(\"IN_COLA_2\")*8/300,\n"
					+ "MIN(\"IN_COLA_3\")*8/300, ROUND(AVG(\"IN_COLA_3\")*8/300,4), MAX(\"IN_COLA_3\")*8/300, STDDEV(\"IN_COLA_3\")*8/300,\n"
					+ "MIN(\"IN_COLA_4\")*8/300, ROUND(AVG(\"IN_COLA_4\")*8/300,4), MAX(\"IN_COLA_4\")*8/300, STDDEV(\"IN_COLA_4\")*8/300,\n"
					+ "MIN(\"IN_COLA_5\")*8/300, ROUND(AVG(\"IN_COLA_5\")*8/300,4), MAX(\"IN_COLA_5\")*8/300, STDDEV(\"IN_COLA_5\")*8/300,\n"
					+ "MIN(\"IN_COLA_6\")*8/300, ROUND(AVG(\"IN_COLA_6\")*8/300,4), MAX(\"IN_COLA_6\")*8/300, STDDEV(\"IN_COLA_6\")*8/300,\n"
					+ "MIN(\"IN_COLA_7\")*8/300, ROUND(AVG(\"IN_COLA_7\")*8/300,4), MAX(\"IN_COLA_7\")*8/300, STDDEV(\"IN_COLA_7\")*8/300,\n"
					+ "MIN(\"IN_COLA_8\")*8/300, ROUND(AVG(\"IN_COLA_8\")*8/300,4), MAX(\"IN_COLA_8\")*8/300, STDDEV(\"IN_COLA_8\")*8/300,\n"
					+ "MIN(\"OUT_COLA_1\")*8/300, ROUND(AVG(\"OUT_COLA_1\")*8/300,4), MAX(\"OUT_COLA_1\")*8/300, STDDEV(\"OUT_COLA_1\")*8/300,\n"
					+ "MIN(\"OUT_COLA_2\")*8/300, ROUND(AVG(\"OUT_COLA_2\")*8/300,4), MAX(\"OUT_COLA_2\")*8/300, STDDEV(\"OUT_COLA_2\")*8/300,\n"
					+ "MIN(\"OUT_COLA_3\")*8/300, ROUND(AVG(\"OUT_COLA_3\")*8/300,4), MAX(\"OUT_COLA_3\")*8/300, STDDEV(\"OUT_COLA_3\")*8/300,\n"
					+ "MIN(\"OUT_COLA_4\")*8/300, ROUND(AVG(\"OUT_COLA_4\")*8/300,4), MAX(\"OUT_COLA_4\")*8/300, STDDEV(\"OUT_COLA_4\")*8/300,\n"
					+ "MIN(\"OUT_COLA_5\")*8/300, ROUND(AVG(\"OUT_COLA_5\")*8/300,4), MAX(\"OUT_COLA_5\")*8/300, STDDEV(\"OUT_COLA_5\")*8/300,\n"
					+ "MIN(\"OUT_COLA_6\")*8/300, ROUND(AVG(\"OUT_COLA_6\")*8/300,4), MAX(\"OUT_COLA_6\")*8/300, STDDEV(\"OUT_COLA_6\")*8/300,\n"
					+ "MIN(\"OUT_COLA_7\")*8/300, ROUND(AVG(\"OUT_COLA_7\")*8/300,4), MAX(\"OUT_COLA_7\")*8/300, STDDEV(\"OUT_COLA_7\")*8/300,\n"
					+ "MIN(\"OUT_COLA_8\")*8/300, ROUND(AVG(\"OUT_COLA_8\")*8/300,4), MAX(\"OUT_COLA_8\")*8/300, STDDEV(\"OUT_COLA_8\")*8/300,\n"
					+ "MIN(\"IN_DROP_1\")/300, ROUND(AVG(\"IN_DROP_1\")/300,4), MAX(\"IN_DROP_1\")/300, STDDEV(\"IN_DROP_1\")/300,\n"
					+ "MIN(\"IN_DROP_2\")/300, ROUND(AVG(\"IN_DROP_2\")/300,4), MAX(\"IN_DROP_2\")/300, STDDEV(\"IN_DROP_2\")/300,\n"
					+ "MIN(\"IN_DROP_3\")/300, ROUND(AVG(\"IN_DROP_3\")/300,4), MAX(\"IN_DROP_3\")/300, STDDEV(\"IN_DROP_3\")/300,\n"
					+ "MIN(\"IN_DROP_4\")/300, ROUND(AVG(\"IN_DROP_4\")/300,4), MAX(\"IN_DROP_4\")/300, STDDEV(\"IN_DROP_4\")/300,\n"
					+ "MIN(\"IN_DROP_5\")/300, ROUND(AVG(\"IN_DROP_5\")/300,4), MAX(\"IN_DROP_5\")/300, STDDEV(\"IN_DROP_5\")/300,\n"
					+ "MIN(\"IN_DROP_6\")/300, ROUND(AVG(\"IN_DROP_6\")/300,4), MAX(\"IN_DROP_6\")/300, STDDEV(\"IN_DROP_6\")/300,\n"
					+ "MIN(\"IN_DROP_7\")/300, ROUND(AVG(\"IN_DROP_7\")/300,4), MAX(\"IN_DROP_7\")/300, STDDEV(\"IN_DROP_7\")/300,\n"
					+ "MIN(\"IN_DROP_8\")/300, ROUND(AVG(\"IN_DROP_8\")/300,4), MAX(\"IN_DROP_8\")/300, STDDEV(\"IN_DROP_8\")/300,\n"
					+ "MIN(\"OUT_DROP_1\")/300, ROUND(AVG(\"OUT_DROP_1\")/300,4), MAX(\"OUT_DROP_1\")/300, STDDEV(\"OUT_DROP_1\")/300,\n"
					+ "MIN(\"OUT_DROP_2\")/300, ROUND(AVG(\"OUT_DROP_2\")/300,4), MAX(\"OUT_DROP_2\")/300, STDDEV(\"OUT_DROP_2\")/300,\n"
					+ "MIN(\"OUT_DROP_3\")/300, ROUND(AVG(\"OUT_DROP_3\")/300,4), MAX(\"OUT_DROP_3\")/300, STDDEV(\"OUT_DROP_3\")/300,\n"
					+ "MIN(\"OUT_DROP_4\")/300, ROUND(AVG(\"OUT_DROP_4\")/300,4), MAX(\"OUT_DROP_4\")/300, STDDEV(\"OUT_DROP_4\")/300,\n"
					+ "MIN(\"OUT_DROP_5\")/300, ROUND(AVG(\"OUT_DROP_5\")/300,4), MAX(\"OUT_DROP_5\")/300, STDDEV(\"OUT_DROP_5\")/300,\n"
					+ "MIN(\"OUT_DROP_6\")/300, ROUND(AVG(\"OUT_DROP_6\")/300,4), MAX(\"OUT_DROP_6\")/300, STDDEV(\"OUT_DROP_6\")/300,\n"
					+ "MIN(\"OUT_DROP_7\")/300, ROUND(AVG(\"OUT_DROP_7\")/300,4), MAX(\"OUT_DROP_7\")/300, STDDEV(\"OUT_DROP_7\")/300,\n"
					+ "MIN(\"OUT_DROP_8\")/300, ROUND(AVG(\"OUT_DROP_8\")/300,4), MAX(\"OUT_DROP_8\")/300, STDDEV(\"OUT_DROP_8\")/300\n"
					+ "FROM dev_webtrafico.\"DataLagsDiferenciada\"\n"
					+ "WHERE \"ID_LAG\" = ? AND \"FECHA\" >= current_timestamp - interval '7 days' ";
		}
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			if (lag) {
				ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_lag"))));
			} else {
				ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_enlace"))));	
			}
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				d.put("in_col1_min", SIFormater(rs.getBigDecimal(1)));
				d.put("in_col1_avg", SIFormater(rs.getBigDecimal(2)));
				d.put("in_col1_max", SIFormater(rs.getBigDecimal(3)));
				d.put("in_col1_stddev", SIFormater(rs.getBigDecimal(4)));

				d.put("in_col2_min", SIFormater(rs.getBigDecimal(5)));
				d.put("in_col2_avg", SIFormater(rs.getBigDecimal(6)));
				d.put("in_col2_max", SIFormater(rs.getBigDecimal(7)));
				d.put("in_col2_stddev", SIFormater(rs.getBigDecimal(8)));

				d.put("in_col3_min", SIFormater(rs.getBigDecimal(9)));
				d.put("in_col3_avg", SIFormater(rs.getBigDecimal(10)));
				d.put("in_col3_max", SIFormater(rs.getBigDecimal(11)));
				d.put("in_col3_stddev", SIFormater(rs.getBigDecimal(12)));

				d.put("in_col4_min", SIFormater(rs.getBigDecimal(13)));
				d.put("in_col4_avg", SIFormater(rs.getBigDecimal(14)));
				d.put("in_col4_max", SIFormater(rs.getBigDecimal(15)));
				d.put("in_col4_stddev", SIFormater(rs.getBigDecimal(16)));

				d.put("in_col5_min", SIFormater(rs.getBigDecimal(17)));
				d.put("in_col5_avg", SIFormater(rs.getBigDecimal(18)));
				d.put("in_col5_max", SIFormater(rs.getBigDecimal(19)));
				d.put("in_col5_stddev", SIFormater(rs.getBigDecimal(20)));

				d.put("in_col6_min", SIFormater(rs.getBigDecimal(21)));
				d.put("in_col6_avg", SIFormater(rs.getBigDecimal(22)));
				d.put("in_col6_max", SIFormater(rs.getBigDecimal(23)));
				d.put("in_col6_stddev", SIFormater(rs.getBigDecimal(24)));

				d.put("in_col7_min", SIFormater(rs.getBigDecimal(25)));
				d.put("in_col7_avg", SIFormater(rs.getBigDecimal(26)));
				d.put("in_col7_max", SIFormater(rs.getBigDecimal(27)));
				d.put("in_col7_stddev", SIFormater(rs.getBigDecimal(28)));

				d.put("in_col8_min", SIFormater(rs.getBigDecimal(29)));
				d.put("in_col8_avg", SIFormater(rs.getBigDecimal(30)));
				d.put("in_col8_max", SIFormater(rs.getBigDecimal(31)));
				d.put("in_col8_stddev", SIFormater(rs.getBigDecimal(32)));

				d.put("out_col1_min", SIFormater(rs.getBigDecimal(33)));
				d.put("out_col1_avg", SIFormater(rs.getBigDecimal(34)));
				d.put("out_col1_max", SIFormater(rs.getBigDecimal(35)));
				d.put("out_col1_stddev", SIFormater(rs.getBigDecimal(36)));

				d.put("out_col2_min", SIFormater(rs.getBigDecimal(37)));
				d.put("out_col2_avg", SIFormater(rs.getBigDecimal(38)));
				d.put("out_col2_max", SIFormater(rs.getBigDecimal(39)));
				d.put("out_col2_stddev", SIFormater(rs.getBigDecimal(40)));

				d.put("out_col3_min", SIFormater(rs.getBigDecimal(41)));
				d.put("out_col3_avg", SIFormater(rs.getBigDecimal(42)));
				d.put("out_col3_max", SIFormater(rs.getBigDecimal(43)));
				d.put("out_col3_stddev", SIFormater(rs.getBigDecimal(44)));

				d.put("out_col4_min", SIFormater(rs.getBigDecimal(45)));
				d.put("out_col4_avg", SIFormater(rs.getBigDecimal(46)));
				d.put("out_col4_max", SIFormater(rs.getBigDecimal(47)));
				d.put("out_col4_stddev", SIFormater(rs.getBigDecimal(48)));

				d.put("out_col5_min", SIFormater(rs.getBigDecimal(49)));
				d.put("out_col5_avg", SIFormater(rs.getBigDecimal(50)));
				d.put("out_col5_max", SIFormater(rs.getBigDecimal(51)));
				d.put("out_col5_stddev", SIFormater(rs.getBigDecimal(52)));

				d.put("out_col6_min", SIFormater(rs.getBigDecimal(53)));
				d.put("out_col6_avg", SIFormater(rs.getBigDecimal(54)));
				d.put("out_col6_max", SIFormater(rs.getBigDecimal(55)));
				d.put("out_col6_stddev", SIFormater(rs.getBigDecimal(56)));

				d.put("out_col7_min", SIFormater(rs.getBigDecimal(57)));
				d.put("out_col7_avg", SIFormater(rs.getBigDecimal(58)));
				d.put("out_col7_max", SIFormater(rs.getBigDecimal(59)));
				d.put("out_col7_stddev", SIFormater(rs.getBigDecimal(60)));

				d.put("out_col8_min", SIFormater(rs.getBigDecimal(61)));
				d.put("out_col8_avg", SIFormater(rs.getBigDecimal(62)));
				d.put("out_col8_max", SIFormater(rs.getBigDecimal(63)));
				d.put("out_col8_stddev", SIFormater(rs.getBigDecimal(64)));
				
				d.put("in_drop1_min", SIFormater(rs.getBigDecimal(65)));
				d.put("in_drop1_avg", SIFormater(rs.getBigDecimal(66)));
				d.put("in_drop1_max", SIFormater(rs.getBigDecimal(67)));
				d.put("in_drop1_stddev", SIFormater(rs.getBigDecimal(68)));

				d.put("in_drop2_min", SIFormater(rs.getBigDecimal(69)));
				d.put("in_drop2_avg", SIFormater(rs.getBigDecimal(70)));
				d.put("in_drop2_max", SIFormater(rs.getBigDecimal(71)));
				d.put("in_drop2_stddev", SIFormater(rs.getBigDecimal(72)));

				d.put("in_drop3_min", SIFormater(rs.getBigDecimal(73)));
				d.put("in_drop3_avg", SIFormater(rs.getBigDecimal(74)));
				d.put("in_drop3_max", SIFormater(rs.getBigDecimal(75)));
				d.put("in_drop3_stddev", SIFormater(rs.getBigDecimal(76)));

				d.put("in_drop4_min", SIFormater(rs.getBigDecimal(77)));
				d.put("in_drop4_avg", SIFormater(rs.getBigDecimal(78)));
				d.put("in_drop4_max", SIFormater(rs.getBigDecimal(79)));
				d.put("in_drop4_stddev", SIFormater(rs.getBigDecimal(80)));

				d.put("in_drop5_min", SIFormater(rs.getBigDecimal(81)));
				d.put("in_drop5_avg", SIFormater(rs.getBigDecimal(82)));
				d.put("in_drop5_max", SIFormater(rs.getBigDecimal(83)));
				d.put("in_drop5_stddev", SIFormater(rs.getBigDecimal(84)));

				d.put("in_drop6_min", SIFormater(rs.getBigDecimal(85)));
				d.put("in_drop6_avg", SIFormater(rs.getBigDecimal(86)));
				d.put("in_drop6_max", SIFormater(rs.getBigDecimal(87)));
				d.put("in_drop6_stddev", SIFormater(rs.getBigDecimal(88)));

				d.put("in_drop7_min", SIFormater(rs.getBigDecimal(89)));
				d.put("in_drop7_avg", SIFormater(rs.getBigDecimal(90)));
				d.put("in_drop7_max", SIFormater(rs.getBigDecimal(91)));
				d.put("in_drop7_stddev", SIFormater(rs.getBigDecimal(92)));

				d.put("in_drop8_min", SIFormater(rs.getBigDecimal(93)));
				d.put("in_drop8_avg", SIFormater(rs.getBigDecimal(94)));
				d.put("in_drop8_max", SIFormater(rs.getBigDecimal(95)));
				d.put("in_drop8_stddev", SIFormater(rs.getBigDecimal(96)));

				d.put("out_drop1_min", SIFormater(rs.getBigDecimal(97)));
				d.put("out_drop1_avg", SIFormater(rs.getBigDecimal(98)));
				d.put("out_drop1_max", SIFormater(rs.getBigDecimal(99)));
				d.put("out_drop1_stddev", SIFormater(rs.getBigDecimal(100)));

				d.put("out_drop2_min", SIFormater(rs.getBigDecimal(101)));
				d.put("out_drop2_avg", SIFormater(rs.getBigDecimal(102)));
				d.put("out_drop2_max", SIFormater(rs.getBigDecimal(103)));
				d.put("out_drop2_stddev", SIFormater(rs.getBigDecimal(104)));

				d.put("out_drop3_min", SIFormater(rs.getBigDecimal(105)));
				d.put("out_drop3_avg", SIFormater(rs.getBigDecimal(106)));
				d.put("out_drop3_max", SIFormater(rs.getBigDecimal(107)));
				d.put("out_drop3_stddev", SIFormater(rs.getBigDecimal(108)));

				d.put("out_drop4_min", SIFormater(rs.getBigDecimal(109)));
				d.put("out_drop4_avg", SIFormater(rs.getBigDecimal(110)));
				d.put("out_drop4_max", SIFormater(rs.getBigDecimal(111)));
				d.put("out_drop4_stddev", SIFormater(rs.getBigDecimal(112)));

				d.put("out_drop5_min", SIFormater(rs.getBigDecimal(113)));
				d.put("out_drop5_avg", SIFormater(rs.getBigDecimal(114)));
				d.put("out_drop5_max", SIFormater(rs.getBigDecimal(115)));
				d.put("out_drop5_stddev", SIFormater(rs.getBigDecimal(116)));

				d.put("out_drop6_min", SIFormater(rs.getBigDecimal(117)));
				d.put("out_drop6_avg", SIFormater(rs.getBigDecimal(118)));
				d.put("out_drop6_max", SIFormater(rs.getBigDecimal(119)));
				d.put("out_drop6_stddev", SIFormater(rs.getBigDecimal(120)));

				d.put("out_drop7_min", SIFormater(rs.getBigDecimal(121)));
				d.put("out_drop7_avg", SIFormater(rs.getBigDecimal(122)));
				d.put("out_drop7_max", SIFormater(rs.getBigDecimal(123)));
				d.put("out_drop7_stddev", SIFormater(rs.getBigDecimal(124)));

				d.put("out_drop8_min", SIFormater(rs.getBigDecimal(125)));
				d.put("out_drop8_avg", SIFormater(rs.getBigDecimal(126)));
				d.put("out_drop8_max", SIFormater(rs.getBigDecimal(127)));
				d.put("out_drop8_stddev", SIFormater(rs.getBigDecimal(128)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return d;
	}
	
	public static Map<String, Object> getTableTrafico(Connection conn, Map<String, Object> payload) {
		Map<String, Object> d = new HashMap<String, Object>();
		String sql = "";
		boolean lag = false;
		if (payload.get("id_enlace") != null) {
			sql = "SELECT MIN(\"TRAFICO\")*8/300, round(AVG(\"TRAFICO\"),4)*8/300, MAX(\"TRAFICO\")*8/300, stddev(\"TRAFICO\")*8/300,\n"
					+ "	MIN(\"TRAFICO_IN\")*8/300, round(AVG(\"TRAFICO_IN\"),4)*8/300, MAX(\"TRAFICO_IN\")*8/300, stddev(\"TRAFICO_IN\")*8/300\n"
					+ "	FROM dev_webtrafico.\"DataDiferenciada\"\n"
					+ "    WHERE \"ID_ENLACE\"=? AND \"FECHA\" >= current_timestamp - interval '7 days'";
		} else {
			lag = true;
			sql = "SELECT MIN(\"TRAFICO\")*8/300, round(AVG(\"TRAFICO\"),4)*8/300, MAX(\"TRAFICO\")*8/300, stddev(\"TRAFICO\")*8/300,\n"
					+ "	MIN(\"TRAFICO_IN\")*8/300, round(AVG(\"TRAFICO_IN\"),4)*8/300, MAX(\"TRAFICO_IN\")*8/300, stddev(\"TRAFICO_IN\")*8/300\n"
					+ "	FROM dev_webtrafico.\"DataLagsDiferenciada\"\n"
					+ "    WHERE \"ID_LAG\" = ? AND \"FECHA\" >= current_timestamp - interval '7 days'";
		}
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			if (lag) {
				ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_lag"))));
			} else {
				ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_enlace"))));	
			}
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				d.put("min", SIFormater(rs.getBigDecimal(1)));
				d.put("avg", SIFormater(rs.getBigDecimal(2)));
				d.put("max", SIFormater(rs.getBigDecimal(3)));
				d.put("stddev", SIFormater(rs.getBigDecimal(4)));

				d.put("min_in", SIFormater(rs.getBigDecimal(5)));
				d.put("avg_in", SIFormater(rs.getBigDecimal(6)));
				d.put("max_in", SIFormater(rs.getBigDecimal(7)));
				d.put("stddev_in", SIFormater(rs.getBigDecimal(8)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return d;
	}

	public static List<Map<String, Object>> getDataDropsColas(Connection conn, Map<String, Object> payload) {
		List<Map<String, Object>> rpta3 = new ArrayList<Map<String, Object>>();
		List<Object> x = new ArrayList<Object>();
		List<Object> in_col1 = new ArrayList<Object>();
		List<Object> in_col2 = new ArrayList<Object>();
		List<Object> in_col3 = new ArrayList<Object>();
		List<Object> in_col4 = new ArrayList<Object>();
		List<Object> in_col5 = new ArrayList<Object>();
		List<Object> in_col6 = new ArrayList<Object>();
		List<Object> in_col7 = new ArrayList<Object>();
		List<Object> in_col8 = new ArrayList<Object>();
		List<Object> out_col1 = new ArrayList<Object>();
		List<Object> out_col2 = new ArrayList<Object>();
		List<Object> out_col3 = new ArrayList<Object>();
		List<Object> out_col4 = new ArrayList<Object>();
		List<Object> out_col5 = new ArrayList<Object>();
		List<Object> out_col6 = new ArrayList<Object>();
		List<Object> out_col7 = new ArrayList<Object>();
		List<Object> out_col8 = new ArrayList<Object>();
		String sql = "";
		boolean lag = false;
		if (payload.get("id_enlace") != null) {
			sql = "SELECT \"IN_DROP_1\", \"IN_DROP_2\", \"IN_DROP_3\", \"IN_DROP_4\", \"IN_DROP_5\", \"IN_DROP_6\", \"IN_DROP_7\", \"IN_DROP_8\", \"OUT_DROP_1\", \"OUT_DROP_2\", \"OUT_DROP_3\", \"OUT_DROP_4\", \"OUT_DROP_5\", \"OUT_DROP_6\", \"OUT_DROP_7\", \"OUT_DROP_8\", \"FECHA\"\n"
					+ "	FROM dev_webtrafico.\"DataDiferenciada\"\n"
					+ "    WHERE \"ID_ENLACE\"= ? AND \"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')"
					+ "    ORDER BY \"FECHA\"";
		} else {
			lag = true;
			sql = "SELECT \"IN_DROP_1\", \"IN_DROP_2\", \"IN_DROP_3\", \"IN_DROP_4\", \"IN_DROP_5\", \"IN_DROP_6\", \"IN_DROP_7\", \"IN_DROP_8\", \"OUT_DROP_1\", \"OUT_DROP_2\", \"OUT_DROP_3\", \"OUT_DROP_4\", \"OUT_DROP_5\", \"OUT_DROP_6\", \"OUT_DROP_7\", \"OUT_DROP_8\", \"FECHA\"\n"
					+ "	FROM dev_webtrafico.\"DataLagsDiferenciada\"\n"
					+ "    WHERE \"ID_LAG\" = ? AND \"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')"
					+ "    ORDER BY \"FECHA\"";
		}
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			if (lag) {
				ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_lag"))));
				ps.setString(2, String.valueOf(payload.get("fecha_ini")));
				ps.setString(3, String.valueOf(payload.get("fecha_fin")));
			} else {
				ps.setInt(1, Integer.parseInt(String.valueOf(payload.get("id_enlace"))));
				ps.setString(2, String.valueOf(payload.get("fecha_ini")));
				ps.setString(3, String.valueOf(payload.get("fecha_fin")));
			}

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				if (rs.getBigDecimal(1) == null) {
					in_col1.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					in_col2.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					in_col3.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					in_col4.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					in_col5.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					in_col6.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					in_col7.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					in_col8.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					out_col1.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					out_col2.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					out_col3.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					out_col4.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					out_col5.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					out_col6.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					out_col7.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					out_col8.add(new BigDecimal(0).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
				} else {
					in_col1.add(rs.getBigDecimal(1).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					in_col2.add(rs.getBigDecimal(2).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					in_col3.add(rs.getBigDecimal(3).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					in_col4.add(rs.getBigDecimal(4).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					in_col5.add(rs.getBigDecimal(5).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					in_col6.add(rs.getBigDecimal(6).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					in_col7.add(rs.getBigDecimal(7).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					in_col8.add(rs.getBigDecimal(8).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					out_col1.add(rs.getBigDecimal(9).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					out_col2.add(rs.getBigDecimal(10).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					out_col3.add(rs.getBigDecimal(11).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					out_col4.add(rs.getBigDecimal(12).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					out_col5.add(rs.getBigDecimal(13).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					out_col6.add(rs.getBigDecimal(14).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					out_col7.add(rs.getBigDecimal(15).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
					out_col8.add(rs.getBigDecimal(16).divide(new BigDecimal(300), 4, BigDecimal.ROUND_HALF_UP));
				}
				x.add(String.valueOf(rs.getTimestamp(17)));
			}
			Map<String, Object> in_cola_1 = new HashMap<String, Object>();
			Map<String, Object> in_cola_2 = new HashMap<String, Object>();
			Map<String, Object> in_cola_3 = new HashMap<String, Object>();
			Map<String, Object> in_cola_4 = new HashMap<String, Object>();
			Map<String, Object> in_cola_5 = new HashMap<String, Object>();
			Map<String, Object> in_cola_6 = new HashMap<String, Object>();
			Map<String, Object> in_cola_7 = new HashMap<String, Object>();
			Map<String, Object> in_cola_8 = new HashMap<String, Object>();
			Map<String, Object> out_cola_1 = new HashMap<String, Object>();
			Map<String, Object> out_cola_2 = new HashMap<String, Object>();
			Map<String, Object> out_cola_3 = new HashMap<String, Object>();
			Map<String, Object> out_cola_4 = new HashMap<String, Object>();
			Map<String, Object> out_cola_5 = new HashMap<String, Object>();
			Map<String, Object> out_cola_6 = new HashMap<String, Object>();
			Map<String, Object> out_cola_7 = new HashMap<String, Object>();
			Map<String, Object> out_cola_8 = new HashMap<String, Object>();
			in_cola_1.put("y", in_col1);
			in_cola_1.put("x", x);
			in_cola_1.put("tittle", "DEFAULT");
			in_cola_2.put("y", in_col2);
			in_cola_2.put("x", x);
			in_cola_2.put("tittle", "VIDEO");
			in_cola_3.put("y", in_col3);
			in_cola_3.put("x", x);
			in_cola_3.put("tittle", "GESTI�N");
			in_cola_4.put("y", in_col4);
			in_cola_4.put("x", x);
			in_cola_4.put("tittle", "CLIENTES");
			in_cola_5.put("y", in_col5);
			in_cola_5.put("x", x);
			in_cola_5.put("tittle", "CLIENTES");
			in_cola_6.put("y", in_col6);
			in_cola_6.put("x", x);
			in_cola_6.put("tittle", "CLIENTES");
			in_cola_7.put("y", in_col7);
			in_cola_7.put("x", x);
			in_cola_7.put("tittle", "VOIP");
			in_cola_8.put("y", in_col8);
			in_cola_8.put("x", x);
			in_cola_8.put("tittle", "ROUTING CONTROL");

			out_cola_1.put("y", out_col1);
			out_cola_1.put("x", x);
			out_cola_1.put("tittle", "DEFAULT");
			out_cola_2.put("y", out_col2);
			out_cola_2.put("x", x);
			out_cola_2.put("tittle", "VIDEO");
			out_cola_3.put("y", out_col3);
			out_cola_3.put("x", x);
			out_cola_3.put("tittle", "GESTI�N");
			out_cola_4.put("y", out_col4);
			out_cola_4.put("x", x);
			out_cola_4.put("tittle", "CLIENTES");
			out_cola_5.put("y", out_col5);
			out_cola_5.put("x", x);
			out_cola_5.put("tittle", "CLIENTES");
			out_cola_6.put("y", out_col6);
			out_cola_6.put("x", x);
			out_cola_6.put("tittle", "CLIENTES");
			out_cola_7.put("y", out_col7);
			out_cola_7.put("x", x);
			out_cola_7.put("tittle", "VOIP");
			out_cola_8.put("y", out_col8);
			out_cola_8.put("x", x);
			out_cola_8.put("tittle", "ROUTING CONTROL");

			rpta3.add(in_cola_1);
			rpta3.add(in_cola_2);
			rpta3.add(in_cola_3);
			rpta3.add(in_cola_4);
			rpta3.add(in_cola_5);
			rpta3.add(in_cola_6);
			rpta3.add(in_cola_7);
			rpta3.add(in_cola_8);
			rpta3.add(out_cola_1);
			rpta3.add(out_cola_2);
			rpta3.add(out_cola_3);
			rpta3.add(out_cola_4);
			rpta3.add(out_cola_5);
			rpta3.add(out_cola_6);
			rpta3.add(out_cola_7);
			rpta3.add(out_cola_8);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rpta3;
	}

	public static List<Map<String, Object>> getReporteTraficoEnlace(Connection conn,
			Map<String, Object> payload) {
		List<Map<String, Object>> rpta = new ArrayList<Map<String, Object>>();
		String sql = "";
		if (payload.get("fecha_ini") != null) {
			sql = "SELECT *\r\n" + 
					"FROM (\r\n" + 
					"    SELECT EN.\"ID_ENLACE\", \"TIPO\", \"REGION\", \"LOCALIDAD\", \"INTERFAZ\", \"LAG_GROUP\", \"DESTINO\", \"INTERFAZ_DESTINO\", EQ.\"ID_EQUIPO\", EN.\"CAPACIDAD\","
					+ "CASE WHEN \"REDUNDANCIA\" IS NULL THEN '' ELSE \"REDUNDANCIA\" END \r\n" + 
					"    FROM dev_webtrafico.\"Enlaces\" EN\r\n" + 
					"    JOIN dev_webtrafico.\"Equipos\" EQ\r\n" + 
					"    ON EN.\"ID_EQUIPO\" = EQ.\"ID_EQUIPO\"\r\n" + 
					"    JOIN dev_webtrafico.\"Ubigeo\" UB\r\n" + 
					"    ON EQ.\"ID_EQUIPO\" = UB.\"ID_EQUIPO\"\r\n" + 
					"    JOIN dev_webtrafico.\"Interfaces\" INTF\r\n" + 
					"    ON EN.\"ID_INTERFAZ\" = INTF.\"ID_INTERFAZ\"\r\n" + 
					") T1 JOIN (\r\n" + 
					"    SELECT \"ID_ENLACE\" AS iden, MIN(\"TRAFICO\")*8/300, AVG(\"TRAFICO\")*8/300, MAX(\"TRAFICO\")*8/300, STDDEV(\"TRAFICO\")*8/300,\r\n" + 
					"    MIN(\"TRAFICO_IN\")*8/300, AVG(\"TRAFICO_IN\")*8/300, MAX(\"TRAFICO_IN\")*8/300, STDDEV(\"TRAFICO_IN\")*8/300\r\n" + 
					"    FROM dev_webtrafico.\"DataDiferenciada\" \r\n" + 
					"    WHERE \"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')\r\n" + 
					"    GROUP BY \"ID_ENLACE\"\r\n" + 
					") T2\r\n" + 
					"ON T1.\"ID_ENLACE\" = T2.iden\r\n" + 
					"JOIN (\r\n" + 
					"    select T1.\"ID_ENLACE\" as enla, case when T2.disponibilidad is null then 100 ELSE T2.disponibilidad END\r\n" + 
					"    from\r\n" + 
					"    (\r\n" + 
					"        SELECT \"ID_ENLACE\"\r\n" + 
					"        FROM dev_webtrafico.\"Enlaces\"\r\n" + 
					"    ) T1 LEFT JOIN (\r\n" + 
					"        SELECT \"ID_ENLACE\" as enla,\r\n" + 
					"        ROUND((CAST (1 - (CAST (CAST (COUNT(1) AS FLOAT) / (EXTRACT (EPOCH FROM (((to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS'))-(to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')))))/60)\r\n" + 
					"         AS FLOAT)) AS NUMERIC))*100,3) AS disponibilidad,\r\n" + 
					"        cast (COUNT(1) AS FLOAT), CAST ((1*60*24*7) AS FLOAT)\r\n" + 
					"        FROM dev_webtrafico.\"Disponibilidad\"\r\n" + 
					"        WHERE \"DISPONIBILIDAD\"= 'down' AND \r\n" + 
					"        \"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')\r\n" + 
					"        GROUP BY \"ID_ENLACE\"\r\n" + 
					"    ) T2\r\n" + 
					"    ON T1.\"ID_ENLACE\" = T2.enla\r\n" + 
					") T3\r\n" + 
					"ON T1.\"ID_ENLACE\" = T3.enla\r\n" + 
					"ORDER BY \"ID_ENLACE\"";
		} else {
			sql = "SELECT *\r\n" + 
					"FROM (\r\n" + 
					"    SELECT EN.\"ID_ENLACE\", \"TIPO\", \"REGION\", \"LOCALIDAD\", \"INTERFAZ\", \"LAG_GROUP\", \"DESTINO\", \"INTERFAZ_DESTINO\", EQ.\"ID_EQUIPO\", EN.\"CAPACIDAD\","
					+ " CASE WHEN \"REDUNDANCIA\" IS NULL THEN '' ELSE \"REDUNDANCIA\" END \r\n" + 
					"    FROM dev_webtrafico.\"Enlaces\" EN\r\n" + 
					"    JOIN dev_webtrafico.\"Equipos\" EQ\r\n" + 
					"    ON EN.\"ID_EQUIPO\" = EQ.\"ID_EQUIPO\"\r\n" + 
					"    JOIN dev_webtrafico.\"Ubigeo\" UB\r\n" + 
					"    ON EQ.\"ID_EQUIPO\" = UB.\"ID_EQUIPO\"\r\n" + 
					"    JOIN dev_webtrafico.\"Interfaces\" INTF\r\n" + 
					"    ON EN.\"ID_INTERFAZ\" = INTF.\"ID_INTERFAZ\"\r\n" + 
					") T1 JOIN (\r\n" + 
					"    SELECT \"ID_ENLACE\" AS iden, MIN(\"TRAFICO\")*8/300, AVG(\"TRAFICO\")*8/300, MAX(\"TRAFICO\")*8/300, STDDEV(\"TRAFICO\")*8/300,\r\n" + 
					"    MIN(\"TRAFICO_IN\")*8/300, AVG(\"TRAFICO_IN\")*8/300, MAX(\"TRAFICO_IN\")*8/300, STDDEV(\"TRAFICO_IN\")*8/300\r\n" + 
					"    FROM dev_webtrafico.\"DataDiferenciada\" \r\n" + 
					"    WHERE \"FECHA\" > current_timestamp - interval'7 days'\r\n" + 
					"    GROUP BY \"ID_ENLACE\"\r\n" + 
					") T2\r\n" + 
					"ON T1.\"ID_ENLACE\" = T2.iden\r\n" + 
					"JOIN (\r\n" + 
					"    select T1.\"ID_ENLACE\" as enla, case when T2.disponibilidad is null then 100 ELSE T2.disponibilidad END\r\n" + 
					"    from\r\n" + 
					"    (\r\n" + 
					"        SELECT \"ID_ENLACE\"\r\n" + 
					"        FROM dev_webtrafico.\"Enlaces\"\r\n" + 
					"    ) T1 LEFT JOIN (\r\n" + 
					"        SELECT \"ID_ENLACE\" as enla, ROUND((CAST (1 - (CAST (CAST (COUNT(1) AS FLOAT) / CAST ((1*60*24*7) AS FLOAT) AS FLOAT)) AS NUMERIC))*100,3) AS disponibilidad\r\n" + 
					"        FROM dev_webtrafico.\"Disponibilidad\"\r\n" + 
					"        WHERE \"DISPONIBILIDAD\"= 'down' AND \"FECHA\" > current_timestamp - interval '7 days'\r\n" + 
					"        GROUP BY \"ID_ENLACE\"\r\n" + 
					"    ) T2\r\n" + 
					"    ON T1.\"ID_ENLACE\" = T2.enla\r\n" + 
					") T3\r\n" + 
					"ON T1.\"ID_ENLACE\" = T3.enla\r\n" + 
					"ORDER BY \"ID_ENLACE\"";
		}
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			if (payload.get("fecha_ini") != null) {
				ps.setString(1, String.valueOf((payload.get("fecha_ini"))));
				ps.setString(2, String.valueOf((payload.get("fecha_fin"))));
				ps.setString(3, String.valueOf((payload.get("fecha_fin"))));
				ps.setString(4, String.valueOf((payload.get("fecha_ini"))));
				ps.setString(5, String.valueOf((payload.get("fecha_ini"))));
				ps.setString(6, String.valueOf((payload.get("fecha_fin"))));
			}
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Map<String, Object> m = new HashMap<String, Object>(); 
				m.put("id_enlace", rs.getInt(1));
				m.put("tipo", rs.getString(2)+" "+rs.getString(11));
				m.put("region_org", rs.getString(3));
				m.put("localidad_org", rs.getString(4));
				m.put("interfaz_org", rs.getString(5));
				m.put("lag", rs.getInt(6));
				m.put("localidad_dst", rs.getString(7));
				m.put("interfaz_dst", rs.getString(8));
				m.put("id_equipo", rs.getInt(9));
				m.put("capacidad", rs.getInt(10)/1000 + "G");
				m.put("raw_capacidad", rs.getInt(10));
				m.put("min", SIFormater(rs.getBigDecimal(13)));
				m.put("raw_min", rs.getBigDecimal(13).divide(BigDecimal.valueOf(1000000), 3, BigDecimal.ROUND_HALF_UP));
				m.put("avg", SIFormater(rs.getBigDecimal(14)));
				m.put("raw_avg", rs.getBigDecimal(14).divide(BigDecimal.valueOf(1000000), 3, BigDecimal.ROUND_HALF_UP));
				m.put("max", SIFormater(rs.getBigDecimal(15)));
				m.put("raw_max", rs.getBigDecimal(15).divide(BigDecimal.valueOf(1000000), 3, BigDecimal.ROUND_HALF_UP));
				m.put("stddev", SIFormater(rs.getBigDecimal(16)));
				m.put("raw_stddev", rs.getBigDecimal(16).divide(BigDecimal.valueOf(1000000), 3, BigDecimal.ROUND_HALF_UP));
				
				m.put("min2", SIFormater(rs.getBigDecimal(17)));
				m.put("raw_min2", rs.getBigDecimal(17).divide(BigDecimal.valueOf(1000000), 3, BigDecimal.ROUND_HALF_UP));
				m.put("avg2", SIFormater(rs.getBigDecimal(18)));
				m.put("raw_avg2", rs.getBigDecimal(18).divide(BigDecimal.valueOf(1000000), 3, BigDecimal.ROUND_HALF_UP));
				m.put("max2", SIFormater(rs.getBigDecimal(19)));
				m.put("raw_max2", rs.getBigDecimal(19).divide(BigDecimal.valueOf(1000000), 3, BigDecimal.ROUND_HALF_UP));
				m.put("stddev2", SIFormater(rs.getBigDecimal(20)));
				m.put("raw_stddev2", rs.getBigDecimal(20).divide(BigDecimal.valueOf(1000000), 3, BigDecimal.ROUND_HALF_UP));
				m.put("dispo", rs.getString(22)+"%");
				m.put("raw_dispo", rs.getFloat(22));
				BigDecimal capa = BigDecimal.valueOf(rs.getInt(10)).multiply(BigDecimal.valueOf(1000000));
				BigDecimal util = rs.getBigDecimal(14).multiply(BigDecimal.valueOf(100)).divide(capa,3,BigDecimal.ROUND_HALF_UP);
				m.put("utilizacion", util + "%");
				m.put("raw_utilizacion", util);
				BigDecimal util2 = rs.getBigDecimal(18).multiply(BigDecimal.valueOf(100)).divide(capa,3,BigDecimal.ROUND_HALF_UP);
				m.put("utilizacion2", util2 + "%");
				m.put("raw_utilizacion2", util2);
				rpta.add(m);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return rpta;
	}

	public static List<Map<String, Object>> getReporteDispoEquipos(Connection conn, Map<String, Object> payload) {
		List<Map<String, Object>> rpta = new ArrayList<Map<String, Object>>();
		String sql = "";
		if (payload.get("fecha_ini") != null) {
			sql = "SELECT *\r\n" + 
					"FROM (\r\n" + 
					"	SELECT EQ.\"ID_EQUIPO\" AS EQP, \"TIPO\","
					+ " CASE WHEN \"REDUNDANCIA\" IS NULL THEN '' ELSE \"REDUNDANCIA\" END"
					+ ", \"REGION\", \"LOCALIDAD\", \"HOSTNAME\", \"IP\"\r\n" + 
					"	FROM dev_webtrafico.\"Equipos\" EQ\r\n" + 
					"    JOIN dev_webtrafico.\"Ubigeo\" UB\r\n" + 
					"    ON EQ.\"ID_EQUIPO\" = UB.\"ID_EQUIPO\"\r\n" + 
					"    ORDER BY EQ.\"ID_EQUIPO\"\r\n" + 
					") T1 JOIN (\r\n" + 
					"	SELECT equip, CASE WHEN dispo IS NULL THEN 100 ELSE dispo END\r\n" + 
					"    FROM (\r\n" + 
					"        SELECT \"ID_EQUIPO\" AS equip\r\n" + 
					"        FROM dev_webtrafico.\"Equipos\"\r\n" + 
					"    ) T1 LEFT JOIN (\r\n" + 
					"        SELECT \"ID_EQUIPO\" AS equip2, ROUND((CAST (1 - (CAST (CAST (COUNT(1) AS FLOAT) / (EXTRACT (EPOCH FROM (((to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS'))-(to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')))))/60)\r\n" + 
					"        AS FLOAT)) AS NUMERIC))*100,3) as dispo \r\n" + 
					"        FROM dev_webtrafico.\"DisponibilidadEquipo\"\r\n" + 
					"        WHERE \"DISPONIBILIDAD\"= 4 AND \"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')\r\n" + 
					"        GROUP BY \"ID_EQUIPO\"\r\n" + 
					"    ) T2\r\n" + 
					"    ON T1.equip = T2.equip2\r\n" + 
					") T2\r\n" + 
					"ON T1.EQP = T2.equip"
					+ " ORDER BY EQP";
		} else {
			sql = "SELECT *\r\n" + 
				"FROM (\r\n" + 
				"	SELECT EQ.\"ID_EQUIPO\" AS EQP, \"TIPO\","
				+ " CASE WHEN \"REDUNDANCIA\" IS NULL THEN '' ELSE \"REDUNDANCIA\" END"
				+ ", \"REGION\", \"LOCALIDAD\", \"HOSTNAME\", \"IP\"\r\n" + 
				"	FROM dev_webtrafico.\"Equipos\" EQ\r\n" + 
				"    JOIN dev_webtrafico.\"Ubigeo\" UB\r\n" + 
				"    ON EQ.\"ID_EQUIPO\" = UB.\"ID_EQUIPO\"\r\n" + 
				"    ORDER BY EQ.\"ID_EQUIPO\"\r\n" + 
				") T1 JOIN (\r\n" + 
				"	SELECT equip, CASE WHEN dispo IS NULL THEN 100 ELSE dispo END\r\n" + 
				"    FROM (\r\n" + 
				"        SELECT \"ID_EQUIPO\" AS equip\r\n" + 
				"        FROM dev_webtrafico.\"Equipos\"\r\n" + 
				"    ) T1 LEFT JOIN (\r\n" + 
				"        SELECT \"ID_EQUIPO\" AS equip2, ROUND((CAST (1 - (CAST (CAST (COUNT(1) AS FLOAT) / CAST ((1*60*24*7) AS FLOAT) AS FLOAT)) AS NUMERIC))*100,3) as dispo\r\n" + 
				"        FROM dev_webtrafico.\"DisponibilidadEquipo\"\r\n" + 
				"        WHERE \"DISPONIBILIDAD\"= 4 AND \"FECHA\" > current_timestamp - interval '7 days'\r\n" + 
				"        GROUP BY \"ID_EQUIPO\"\r\n" + 
				"    ) T2\r\n" + 
				"    ON T1.equip = T2.equip2\r\n" + 
				") T2\r\n" + 
				"ON T1.EQP = T2.equip"
				+ " ORDER BY EQP";
		}
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			if (payload.get("fecha_ini") != null) {
				ps.setString(1, String.valueOf((payload.get("fecha_fin"))));
				ps.setString(2, String.valueOf((payload.get("fecha_ini"))));
				ps.setString(3, String.valueOf((payload.get("fecha_ini"))));
				ps.setString(4, String.valueOf((payload.get("fecha_fin"))));
			}
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Map<String, Object> m = new HashMap<String, Object>();
				m.put("id_equipo", rs.getInt(1));
				m.put("tipo", rs.getString(2)+" "+rs.getString(3));
				m.put("region", rs.getString(4));
				m.put("localidad", rs.getString(5));
				m.put("hostname", rs.getString(6));
				m.put("ip", rs.getString(7));
				m.put("dispo", rs.getString(9)+"%");
				m.put("raw_dispo", rs.getFloat(9));
				rpta.add(m);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rpta;
	}

	public static Map<String, Object> getdispoEquiposDash(Connection conn, Map<String, Object> payload) {
		Map<String, Object> rpta = new HashMap<String, Object>();
		String sql = "";
		if (payload.get("fecha_ini") != null) {
			sql = "select CONCAT(LOWER(\"TIPO\"), CASE WHEN \"REDUNDANCIA\" IS NULL THEN '' ELSE CONCAT('_', LOWER(\"REDUNDANCIA\")) END),\r\n" + 
					"(CASE WHEN MAX(MINUTOS) IS NULL THEN 100 ELSE ROUND((1 - (CAST (MAX(MINUTOS) AS NUMERIC)/\r\n" + 
					"CAST((EXTRACT (EPOCH FROM ((to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')) - (to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')))))/ 60 AS NUMERIC)))*100, 3) END),\r\n" + 
					"(CASE WHEN MAX(MINUTOS) IS NULL THEN 0 ELSE MAX(MINUTOS) END), \"TIPO\",\r\n" + 
					"CASE WHEN \"REDUNDANCIA\" IS NULL THEN '' ELSE \"REDUNDANCIA\" END\r\n" + 
					"from (\r\n" + 
					"	SELECT \"ID_EQUIPO\", \"TIPO\", \"REDUNDANCIA\"\r\n" + 
					"	FROM dev_webtrafico.\"Equipos\"\r\n" + 
					") t1 LEFT JOIN (\r\n" + 
					"	SELECT \"ID_EQUIPO\" AS id_equipo, COUNT(1) AS MINUTOS\r\n" + 
					"	FROM dev_webtrafico.\"DisponibilidadEquipo\"\r\n" + 
					"	WHERE \"DISPONIBILIDAD\"= 4 AND\r\n" + 
					"	\"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')\r\n" + 
					"	GROUP BY \"ID_EQUIPO\"\r\n" + 
					") t2 on t1.\"ID_EQUIPO\" = t2.id_equipo\r\n" + 
					"GROUP BY \"TIPO\", \"REDUNDANCIA\"\r\n" + 
					"ORDER BY concat";
		} else {
			sql = "select CONCAT(LOWER(\"TIPO\"), CASE WHEN \"REDUNDANCIA\" IS NULL THEN '' ELSE CONCAT('_', LOWER(\"REDUNDANCIA\")) END),\r\n" + 
					"(CASE WHEN MAX(MINUTOS) IS NULL THEN 100 ELSE ROUND((1 - (CAST (MAX(MINUTOS) AS NUMERIC)/(60*24*7)))*100, 3) END),\r\n" + 
					"(CASE WHEN MAX(MINUTOS) IS NULL THEN 0 ELSE MAX(MINUTOS) END), \"TIPO\",\r\n" + 
					"CASE WHEN \"REDUNDANCIA\" IS NULL THEN '' ELSE \"REDUNDANCIA\" END\r\n" + 
					"from (\r\n" + 
					"	SELECT \"ID_EQUIPO\", \"TIPO\", \"REDUNDANCIA\"\r\n" + 
					"	FROM dev_webtrafico.\"Equipos\"\r\n" + 
					") t1 LEFT JOIN (\r\n" + 
					"	SELECT \"ID_EQUIPO\" AS id_equipo, COUNT(1) AS MINUTOS\r\n" + 
					"	FROM dev_webtrafico.\"DisponibilidadEquipo\"\r\n" + 
					"	WHERE \"DISPONIBILIDAD\"= 4 AND\r\n" + 
					"	\"FECHA\" > current_timestamp - interval '7 days'\r\n" + 
					"	GROUP BY \"ID_EQUIPO\"\r\n" + 
					") t2 on t1.\"ID_EQUIPO\" = t2.id_equipo\r\n" + 
					"GROUP BY \"TIPO\", \"REDUNDANCIA\"\r\n" + 
					"ORDER BY concat";
		}
		
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(sql);
			if (payload.get("fecha_ini") != null) {
				ps.setString(1, String.valueOf((payload.get("fecha_fin"))));
				ps.setString(2, String.valueOf((payload.get("fecha_ini"))));
				ps.setString(3, String.valueOf((payload.get("fecha_ini"))));
				ps.setString(4, String.valueOf((payload.get("fecha_fin"))));
			} 
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				if (rs.getString(1).equals("conexi�n")) {
					rpta.put("conexion", rs.getFloat(2));
					rpta.put("c_conexion", DAO.getColor(rs.getFloat(2), rs.getString(4), rs.getString(5)));
					rpta.put("c_minutos_conexion", rs.getInt(3));
				} else {
					rpta.put(rs.getString(1), rs.getFloat(2));
					rpta.put("c_"+rs.getString(1), DAO.getColor(rs.getFloat(2), rs.getString(4), rs.getString(5)));
					rpta.put("c_minutos_"+rs.getString(1), rs.getInt(3));
				}
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return rpta;
	}

	public static List<Map<String, Object>> getdispoEnlasDash(Connection conn, Map<String, Object> payload) {
		List<Map<String, Object>> rpta = new ArrayList<Map<String, Object>>();
		String sql = "";
		if (payload.get("fecha_ini") != null) {
			sql = "SELECT *\r\n" + 
					"FROM (\r\n" + 
					"    SELECT EN.\"ID_ENLACE\", CONCAT('[',\"TIPO\",'] ',\"LOCALIDAD\", ' / [', CASE\r\n" + 
					"                               WHEN \"TIPO\" = 'CORE' THEN 'CORE] '\r\n" + 
					"                              WHEN \"TIPO\" = 'AGREGADOR' THEN 'CORE] '\r\n" + 
					"                              WHEN \"TIPO\" = 'DISTRIBUIDOR' THEN 'AGREGADOR] '\r\n" + 
					"                              ELSE 'DISTRIBUIDOR] ' END, \"DESTINO\")\r\n" + 
					"    FROM dev_webtrafico.\"Equipos\" EQ\r\n" + 
					"    JOIN dev_webtrafico.\"Enlaces\" EN\r\n" + 
					"    ON EQ.\"ID_EQUIPO\" = EN.\"ID_EQUIPO\"\r\n" + 
					"    JOIN dev_webtrafico.\"Ubigeo\" UB\r\n" + 
					"    ON EQ.\"ID_EQUIPO\" = UB.\"ID_EQUIPO\"\r\n" + 
					") T1 RIGHT JOIN (\r\n" + 
					"    select T1.\"ID_ENLACE\" as enla, case when T2.disponibilidad is null then 100 ELSE T2.disponibilidad END,\r\n" + 
					"CASE WHEN T2.disponibilidad is null then '#33cc33' WHEN T2.disponibilidad < 95 THEN '#ff471a'\r\n" + 
					"WHEN T2.disponibilidad >= 95 AND T2.disponibilidad < 97 THEN '#ffff33' ELSE '#33cc33' END,"
					+ " CASE WHEN minutos IS NULL THEN 0 ELSE minutos END \r\n" + 
					"from (\r\n" + 
					"	SELECT \"ID_ENLACE\" \r\n" + 
					"	FROM dev_webtrafico.\"Enlaces\"\r\n" + 
					") T1 LEFT JOIN (\r\n" + 
					"	SELECT \"ID_ENLACE\" as enla, ROUND((CAST (1 - (CAST (CAST (COUNT(1) AS FLOAT) / (EXTRACT (EPOCH FROM (((to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS'))-(to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')))))/60)\r\n" + 
					"	AS FLOAT)) AS NUMERIC))*100,3) AS disponibilidad, count(1) as minutos\r\n" + 
					"	FROM dev_webtrafico.\"Disponibilidad\"\r\n" + 
					"	WHERE \"DISPONIBILIDAD\"= 'down' AND\r\n" + 
					"	\"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')\r\n" + 
					"	GROUP BY \"ID_ENLACE\"\r\n" + 
					") T2\r\n" + 
					"ON T1.\"ID_ENLACE\" = T2.enla\r\n" + 
					"ORDER BY disponibilidad\r\n" + 
					"limit 7"+
					") T2\r\n" + 
					"ON T2.enla = T1.\"ID_ENLACE\"";
		} else {
			sql = "SELECT *\r\n" + 
					"FROM (\r\n" + 
					"	SELECT EN.\"ID_ENLACE\", CONCAT('[',\"TIPO\",'] ',\"LOCALIDAD\", ' / [', CASE\r\n" + 
					"	WHEN \"TIPO\" = 'CORE' THEN 'CORE] '\r\n" + 
					"	WHEN \"TIPO\" = 'AGREGADOR' THEN 'CORE] '\r\n" + 
					"	WHEN \"TIPO\" = 'DISTRIBUIDOR' THEN 'AGREGADOR] '\r\n" + 
					"	ELSE 'DISTRIBUIDOR] ' END, \"DESTINO\")\r\n" + 
					"	FROM dev_webtrafico.\"Equipos\" EQ\r\n" + 
					"	JOIN dev_webtrafico.\"Enlaces\" EN\r\n" + 
					"	ON EQ.\"ID_EQUIPO\" = EN.\"ID_EQUIPO\"\r\n" + 
					"	JOIN dev_webtrafico.\"Ubigeo\" UB\r\n" + 
					"	ON EQ.\"ID_EQUIPO\" = UB.\"ID_EQUIPO\"\r\n" + 
					") T1 RIGHT JOIN (\r\n" + 
					"	select T1.\"ID_ENLACE\" as enla, case when T2.disponibilidad is null then 100 ELSE T2.disponibilidad END,\r\n" + 
					"	CASE WHEN T2.disponibilidad is null then '#33cc33' WHEN T2.disponibilidad < 95 THEN '#ff471a'\r\n" + 
					"	WHEN T2.disponibilidad >= 95 AND T2.disponibilidad < 97 THEN '#ffff33' ELSE '#33cc33' END, \r\n" + 
					"    CASE WHEN minutos IS NULL THEN 0 ELSE minutos END\r\n" + 
					"	from (\r\n" + 
					"		SELECT \"ID_ENLACE\"\r\n" + 
					"		FROM dev_webtrafico.\"Enlaces\"\r\n" + 
					"	) T1 LEFT JOIN (\r\n" + 
					"		SELECT \"ID_ENLACE\" as enla, ROUND((CAST (1 - (CAST (CAST (COUNT(1) AS FLOAT) / CAST ((1*60*24*7) AS FLOAT) AS FLOAT)) AS NUMERIC))*100,3) AS disponibilidad, count(1) as minutos\r\n" + 
					"		FROM dev_webtrafico.\"Disponibilidad\"\r\n" + 
					"		WHERE \"DISPONIBILIDAD\"= 'down' AND \"FECHA\" > current_timestamp - interval '7 days'\r\n" + 
					"		GROUP BY \"ID_ENLACE\"\r\n" + 
					"	) T2 ON T1.\"ID_ENLACE\" = T2.enla\r\n" + 
					"	ORDER BY disponibilidad\r\n" + 
					"limit 7\r\n" + 
					") T2\r\n" + 
					"ON T2.enla = T1.\"ID_ENLACE\"";
		}
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			if (payload.get("fecha_ini") != null) {
				ps.setString(1, String.valueOf((payload.get("fecha_fin"))));
				ps.setString(2, String.valueOf((payload.get("fecha_ini"))));
				ps.setString(3, String.valueOf((payload.get("fecha_ini"))));
				ps.setString(4, String.valueOf((payload.get("fecha_fin"))));
			} 
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Map<String, Object> m = new HashMap<String, Object>();
				m.put("id_enlace", rs.getInt(1));
				m.put("enlace", rs.getString(2));
				m.put("dispo", rs.getFloat(4));
				m.put("color", rs.getString(5));
				m.put("minutos", rs.getInt(6));
				rpta.add(m);
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rpta;
	}

	public static List<Map<String, Object>> dispoLagsDash(Connection conn, Map<String, Object> payload) {
		List<Map<String, Object>> rpta = new ArrayList<Map<String, Object>>();
		String sql = "";
		if (payload.get("fecha_ini") != null) {
			sql = "SELECT *\r\n" + 
					"FROM (\r\n" + 
					"    SELECT EN.\"ID_LAG\", CONCAT('[',\"TIPO\",'] ',\"LOCALIDAD\", ' / LAG GROUP ', \"LAG_GROUP\")\r\n" + 
					"    FROM dev_webtrafico.\"Equipos\" EQ\r\n" + 
					"    JOIN dev_webtrafico.\"Lags\" EN\r\n" + 
					"    ON EQ.\"ID_EQUIPO\" = EN.\"ID_EQUIPO\"\r\n" + 
					"    JOIN dev_webtrafico.\"Ubigeo\" UB\r\n" + 
					"    ON EQ.\"ID_EQUIPO\" = UB.\"ID_EQUIPO\"\r\n" + 
					") T1 RIGHT JOIN (\r\n" + 
					"    select T1.\"ID_LAG\" as enla, case when T2.disponibilidad is null then 100 ELSE T2.disponibilidad END, "
					+ "CASE WHEN T2.disponibilidad is null then '#33cc33' WHEN T2.disponibilidad < 95 THEN '#ff471a'"
					+ "WHEN T2.disponibilidad >= 95 AND T2.disponibilidad < 97 THEN '#ffff33' ELSE '#33cc33' END \r\n" + 
					"    from (\r\n" + 
					"        SELECT \"ID_LAG\"\r\n" + 
					"        FROM dev_webtrafico.\"Lags\"\r\n" + 
					"    ) T1 LEFT JOIN (\r\n" + 
					"        SELECT \"ID_LAG\" as enla, ROUND((CAST (1 - (CAST (CAST (COUNT(1) AS FLOAT) / (EXTRACT (EPOCH FROM (((to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS'))-(to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')))))/60)\r\n" + 
					"	AS FLOAT)) AS NUMERIC))*100,3) AS disponibilidad\r\n" + 
					"        FROM dev_webtrafico.\"DisponibilidadLag\"\r\n" + 
					"        WHERE \"DISPONIBILIDAD\"= 4 AND \"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')"
					+ " 		AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')\r\n" + 
					"        GROUP BY \"ID_LAG\"\r\n" + 
					"    ) T2\r\n" + 
					"    ON T1.\"ID_LAG\" = T2.enla\r\n" + 
					"    ORDER BY disponibilidad\r\n" + 
					"    limit 7\r\n" + 
					") T2\r\n" + 
					"ON T2.enla = T1.\"ID_LAG\"";
		} else {
			sql = "SELECT *\r\n" + 
					"FROM (\r\n" + 
					"    SELECT EN.\"ID_LAG\", CONCAT('[',\"TIPO\",'] ',\"LOCALIDAD\", ' / LAG GROUP ', \"LAG_GROUP\")\r\n" + 
					"    FROM dev_webtrafico.\"Equipos\" EQ\r\n" + 
					"    JOIN dev_webtrafico.\"Lags\" EN\r\n" + 
					"    ON EQ.\"ID_EQUIPO\" = EN.\"ID_EQUIPO\"\r\n" + 
					"    JOIN dev_webtrafico.\"Ubigeo\" UB\r\n" + 
					"    ON EQ.\"ID_EQUIPO\" = UB.\"ID_EQUIPO\"\r\n" + 
					") T1 RIGHT JOIN (\r\n" + 
					"    select T1.\"ID_LAG\" as enla, case when T2.disponibilidad is null then 100 ELSE T2.disponibilidad END, "
					+ "CASE WHEN T2.disponibilidad is null then '#33cc33' WHEN T2.disponibilidad < 95 THEN '#ff471a'"
					+ "WHEN T2.disponibilidad >= 95 AND T2.disponibilidad < 97 THEN '#ffff33' ELSE '#33cc33' END \r\n" + 
					"    from (\r\n" + 
					"        SELECT \"ID_LAG\"\r\n" + 
					"        FROM dev_webtrafico.\"Lags\"\r\n" + 
					"    ) T1 LEFT JOIN (\r\n" + 
					"        SELECT \"ID_LAG\" as enla, ROUND((CAST (1 - (CAST (CAST (COUNT(1) AS FLOAT) / CAST ((1*60*24*7) AS FLOAT) AS FLOAT)) AS NUMERIC))*100,3) AS disponibilidad\r\n" + 
					"        FROM dev_webtrafico.\"DisponibilidadLag\"\r\n" + 
					"        WHERE \"DISPONIBILIDAD\"= 4 AND \"FECHA\" > current_timestamp - interval '7 days'\r\n" + 
					"        GROUP BY \"ID_LAG\"\r\n" + 
					"    ) T2\r\n" + 
					"    ON T1.\"ID_LAG\" = T2.enla\r\n" + 
					"    ORDER BY disponibilidad\r\n" + 
					"    limit 7\r\n" + 
					") T2\r\n" + 
					"ON T2.enla = T1.\"ID_LAG\"";
		}
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			if (payload.get("fecha_ini") != null) {
				ps.setString(1, String.valueOf((payload.get("fecha_fin"))));
				ps.setString(2, String.valueOf((payload.get("fecha_ini"))));
				ps.setString(3, String.valueOf((payload.get("fecha_ini"))));
				ps.setString(4, String.valueOf((payload.get("fecha_fin"))));
			}
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Map<String, Object> m = new HashMap<String, Object>();
				m.put("id_lag", rs.getInt(1));
				m.put("lag", rs.getString(2));
				m.put("dispo", rs.getFloat(4));
				m.put("color", rs.getString(5));
				rpta.add(m);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rpta;
	}

	public static List<Map<String, Object>> trafEnlaDash(Connection conn, Map<String, Object> payload) {
		List<Map<String, Object>> rpta = new ArrayList<Map<String, Object>>();
		String sql = "";
		if (payload.get("fecha_ini") != null) {
			sql = "select \"ID_ENLACE\", ENLA, ROUND( (traffic*100) / (CAST(\"CAPACIDAD\" AS NUMERIC ) *1000000),3) AS UTIL,\r\n" + 
					"CASE\r\n" + 
					"	WHEN ROUND( (traffic*100) / (CAST(\"CAPACIDAD\" AS NUMERIC ) *1000000),3) < 50 THEN '#33cc33'\r\n" + 
					"    WHEN ROUND( (traffic*100) / (CAST(\"CAPACIDAD\" AS NUMERIC ) *1000000),3) >= 50 AND ROUND( (traffic*100) / (CAST(\"CAPACIDAD\" AS NUMERIC ) *1000000),3) < 75 THEN '#ffff33'\r\n" + 
					"    WHEN ROUND( (traffic*100) / (CAST(\"CAPACIDAD\" AS NUMERIC ) *1000000),3) >= 75 THEN '#ff471a' END, traffic\r\n" + 
					"\r\n" + 
					"from (\r\n" + 
					"SELECT EN.\"ID_ENLACE\", CONCAT('[',\"TIPO\",'] ',\"LOCALIDAD\", ' / [', CASE\r\n" + 
					"WHEN \"TIPO\" = 'CORE' THEN 'CORE] '\r\n" + 
					"WHEN \"TIPO\" = 'AGREGADOR' THEN 'CORE] '\r\n" + 
					"WHEN \"TIPO\" = 'DISTRIBUIDOR' THEN 'AGREGADOR] '\r\n" + 
					"ELSE 'DISTRIBUIDOR] ' END, \"DESTINO\") AS ENLA, \"CAPACIDAD\"\r\n" + 
					"FROM dev_webtrafico.\"Equipos\" EQ\r\n" + 
					"JOIN dev_webtrafico.\"Enlaces\" EN\r\n" + 
					"ON EQ.\"ID_EQUIPO\" = EN.\"ID_EQUIPO\"\r\n" + 
					"JOIN dev_webtrafico.\"Ubigeo\" UB\r\n" + 
					"ON EQ.\"ID_EQUIPO\" = UB.\"ID_EQUIPO\"\r\n" + 
					") t1 RIGHT join (\r\n" + 
					"SELECT DD.\"ID_ENLACE\" AS iden, AVG(\"TRAFICO\")*8/300 as traffic\r\n" + 
					"FROM dev_webtrafico.\"DataDiferenciada\" DD\r\n" + 
					"JOIN dev_webtrafico.\"Enlaces\" EN\r\n" + 
					"ON EN.\"ID_ENLACE\" = DD.\"ID_ENLACE\"\r\n" + 
					"JOIN dev_webtrafico.\"Equipos\" EQ\r\n" + 
					"ON EQ.\"ID_EQUIPO\" = EN.\"ID_EQUIPO\"\r\n" + 
					"WHERE \"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"TIPO\" = 'CORE'\r\n" + 
					"GROUP BY DD.\"ID_ENLACE\"\r\n" + 
					") t2 \r\n" + 
					"on T2.iden = T1.\"ID_ENLACE\"\r\n" + 
					"ORDER BY util DESC\r\n" + 
					"LIMIT 7";
		} else {
			sql = "select \"ID_ENLACE\", ENLA, ROUND( (traffic*100) / (CAST(\"CAPACIDAD\" AS NUMERIC ) *1000000),3) AS UTIL,\r\n" + 
					"CASE\r\n" + 
					"	WHEN ROUND( (traffic*100) / (CAST(\"CAPACIDAD\" AS NUMERIC ) *1000000),3) < 50 THEN '#33cc33'\r\n" + 
					"	WHEN ROUND( (traffic*100) / (CAST(\"CAPACIDAD\" AS NUMERIC ) *1000000),3) >= 50 AND ROUND( (traffic*100) / (CAST(\"CAPACIDAD\" AS NUMERIC ) *1000000),3) < 75 THEN '#ffff33'\r\n" + 
					"	WHEN ROUND( (traffic*100) / (CAST(\"CAPACIDAD\" AS NUMERIC ) *1000000),3) >= 75 THEN '#ff471a' END, traffic\r\n" + 
					"from (\r\n" + 
					"    SELECT EN.\"ID_ENLACE\", CONCAT('[',\"TIPO\",'] ',\"LOCALIDAD\", ' / [', CASE\r\n" + 
					"    WHEN \"TIPO\" = 'CORE' THEN 'CORE] '\r\n" + 
					"    WHEN \"TIPO\" = 'AGREGADOR' THEN 'CORE] '\r\n" + 
					"    WHEN \"TIPO\" = 'DISTRIBUIDOR' THEN 'AGREGADOR] '\r\n" + 
					"    ELSE 'DISTRIBUIDOR] ' END, \"DESTINO\") AS ENLA, \"CAPACIDAD\"\r\n" + 
					"    FROM dev_webtrafico.\"Equipos\" EQ\r\n" + 
					"    JOIN dev_webtrafico.\"Enlaces\" EN\r\n" + 
					"    ON EQ.\"ID_EQUIPO\" = EN.\"ID_EQUIPO\"\r\n" + 
					"    JOIN dev_webtrafico.\"Ubigeo\" UB\r\n" + 
					"	ON EQ.\"ID_EQUIPO\" = UB.\"ID_EQUIPO\"\r\n" + 
					") t1 RIGHT join (\r\n" + 
					"	SELECT DD.\"ID_ENLACE\" AS iden, AVG(\"TRAFICO\")*8/300 as traffic\r\n" + 
					"	FROM dev_webtrafico.\"DataDiferenciada\" DD\r\n" + 
					"	JOIN dev_webtrafico.\"Enlaces\" EN\r\n" + 
					"	ON EN.\"ID_ENLACE\" = DD.\"ID_ENLACE\"\r\n" + 
					"	JOIN dev_webtrafico.\"Equipos\" EQ\r\n" + 
					"	ON EQ.\"ID_EQUIPO\" = EN.\"ID_EQUIPO\"\r\n" + 
					"	WHERE \"FECHA\" > current_timestamp - interval'7 days' and \"TIPO\" = 'CORE'\r\n" + 
					"	GROUP BY DD.\"ID_ENLACE\"\r\n" + 
					") t2\r\n" + 
					"on T2.iden = T1.\"ID_ENLACE\"\r\n" + 
					"ORDER BY util DESC\r\n" + 
					"LIMIT 7";
		}
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			if (payload.get("fecha_ini") != null) {
				ps.setString(1, String.valueOf((payload.get("fecha_ini"))));
				ps.setString(2, String.valueOf((payload.get("fecha_fin"))));
			}
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Map<String, Object> m = new HashMap<String, Object>();
				m.put("id_enlace", rs.getInt(1));
				m.put("enlace", rs.getString(2));
				m.put("utilizacion", rs.getBigDecimal(3));
				m.put("color", rs.getString(4));
				m.put("trafico", DAO.SIFormater(rs.getBigDecimal(5)));
				rpta.add(m);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rpta;
	}

	public static List<Map<String, Object>> trafAgregaDash(Connection conn, Map<String, Object> payload) {
		List<Map<String, Object>> rpta = new ArrayList<Map<String, Object>>();
		String sql = "";
		if (payload.get("fecha_ini") != null) {
			sql = "select \"ID_ENLACE\", ENLA, ROUND( (traffic*100) / (CAST(\"CAPACIDAD\" AS NUMERIC ) *1000000),3) AS UTIL,\r\n" + 
					"CASE\r\n" + 
					"	WHEN ROUND( (traffic*100) / (CAST(\"CAPACIDAD\" AS NUMERIC ) *1000000),3) < 50 THEN '#33cc33'\r\n" + 
					"    WHEN ROUND( (traffic*100) / (CAST(\"CAPACIDAD\" AS NUMERIC ) *1000000),3) >= 50 AND ROUND( (traffic*100) / (CAST(\"CAPACIDAD\" AS NUMERIC ) *1000000),3) < 75 THEN '#ffff33'\r\n" + 
					"    WHEN ROUND( (traffic*100) / (CAST(\"CAPACIDAD\" AS NUMERIC ) *1000000),3) >= 75 THEN '#ff471a' END, traffic\r\n" + 
					"\r\n" + 
					"from (\r\n" + 
					"SELECT EN.\"ID_ENLACE\", CONCAT('[',\"TIPO\",'] ',\"LOCALIDAD\", ' / [', CASE\r\n" + 
					"WHEN \"TIPO\" = 'CORE' THEN 'CORE] '\r\n" + 
					"WHEN \"TIPO\" = 'AGREGADOR' THEN 'CORE] '\r\n" + 
					"WHEN \"TIPO\" = 'DISTRIBUIDOR' THEN 'AGREGADOR] '\r\n" + 
					"ELSE 'DISTRIBUIDOR] ' END, \"DESTINO\") AS ENLA, \"CAPACIDAD\"\r\n" + 
					"FROM dev_webtrafico.\"Equipos\" EQ\r\n" + 
					"JOIN dev_webtrafico.\"Enlaces\" EN\r\n" + 
					"ON EQ.\"ID_EQUIPO\" = EN.\"ID_EQUIPO\"\r\n" + 
					"JOIN dev_webtrafico.\"Ubigeo\" UB\r\n" + 
					"ON EQ.\"ID_EQUIPO\" = UB.\"ID_EQUIPO\"\r\n" + 
					") t1 RIGHT join (\r\n" + 
					"SELECT DD.\"ID_ENLACE\" AS iden, AVG(\"TRAFICO\")*8/300 as traffic\r\n" + 
					"FROM dev_webtrafico.\"DataDiferenciada\" DD\r\n" + 
					"JOIN dev_webtrafico.\"Enlaces\" EN\r\n" + 
					"ON EN.\"ID_ENLACE\" = DD.\"ID_ENLACE\"\r\n" + 
					"JOIN dev_webtrafico.\"Equipos\" EQ\r\n" + 
					"ON EQ.\"ID_EQUIPO\" = EN.\"ID_EQUIPO\"\r\n" + 
					"WHERE\"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"TIPO\" = 'AGREGADOR'\r\n" + 
					"GROUP BY DD.\"ID_ENLACE\"\r\n" + 
					") t2 \r\n" + 
					"on T2.iden = T1.\"ID_ENLACE\"\r\n" + 
					"ORDER BY util DESC\r\n" + 
					"LIMIT 7";
		} else {
			sql = "select \"ID_ENLACE\", ENLA, ROUND( (traffic*100) / (CAST(\"CAPACIDAD\" AS NUMERIC ) *1000000),3) AS UTIL,\r\n" + 
					"CASE\r\n" + 
					"	WHEN ROUND( (traffic*100) / (CAST(\"CAPACIDAD\" AS NUMERIC ) *1000000),3) < 50 THEN '#33cc33'\r\n" + 
					"    WHEN ROUND( (traffic*100) / (CAST(\"CAPACIDAD\" AS NUMERIC ) *1000000),3) >= 50 AND ROUND( (traffic*100) / (CAST(\"CAPACIDAD\" AS NUMERIC ) *1000000),3) < 75 THEN '#ffff33'\r\n" + 
					"    WHEN ROUND( (traffic*100) / (CAST(\"CAPACIDAD\" AS NUMERIC ) *1000000),3) >= 75 THEN '#ff471a' END, traffic\r\n" + 
					"\r\n" + 
					"from (\r\n" + 
					"SELECT EN.\"ID_ENLACE\", CONCAT('[',\"TIPO\",'] ',\"LOCALIDAD\", ' / [', CASE\r\n" + 
					"WHEN \"TIPO\" = 'CORE' THEN 'CORE] '\r\n" + 
					"WHEN \"TIPO\" = 'AGREGADOR' THEN 'CORE] '\r\n" + 
					"WHEN \"TIPO\" = 'DISTRIBUIDOR' THEN 'AGREGADOR] '\r\n" + 
					"ELSE 'DISTRIBUIDOR] ' END, \"DESTINO\") AS ENLA, \"CAPACIDAD\"\r\n" + 
					"FROM dev_webtrafico.\"Equipos\" EQ\r\n" + 
					"JOIN dev_webtrafico.\"Enlaces\" EN\r\n" + 
					"ON EQ.\"ID_EQUIPO\" = EN.\"ID_EQUIPO\"\r\n" + 
					"JOIN dev_webtrafico.\"Ubigeo\" UB\r\n" + 
					"ON EQ.\"ID_EQUIPO\" = UB.\"ID_EQUIPO\"\r\n" + 
					") t1 RIGHT join (\r\n" + 
					"SELECT DD.\"ID_ENLACE\" AS iden, AVG(\"TRAFICO\")*8/300 as traffic\r\n" + 
					"FROM dev_webtrafico.\"DataDiferenciada\" DD\r\n" + 
					"JOIN dev_webtrafico.\"Enlaces\" EN\r\n" + 
					"ON EN.\"ID_ENLACE\" = DD.\"ID_ENLACE\"\r\n" + 
					"JOIN dev_webtrafico.\"Equipos\" EQ\r\n" + 
					"ON EQ.\"ID_EQUIPO\" = EN.\"ID_EQUIPO\"\r\n" + 
					"WHERE \"FECHA\" > current_timestamp - interval'7 days' and \"TIPO\" = 'AGREGADOR'\r\n" + 
					"GROUP BY DD.\"ID_ENLACE\"\r\n" + 
					") t2 \r\n" + 
					"on T2.iden = T1.\"ID_ENLACE\"\r\n" + 
					"ORDER BY util DESC\r\n" + 
					"LIMIT 7";
		}
		
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			if (payload.get("fecha_ini") != null) {
				ps.setString(1, String.valueOf((payload.get("fecha_ini"))));
				ps.setString(2, String.valueOf((payload.get("fecha_fin"))));
			}
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Map<String, Object> m = new HashMap<String, Object>();
				m.put("id_enlace", rs.getInt(1));
				m.put("enlace", rs.getString(2));
				m.put("utilizacion", rs.getFloat(3));
				m.put("color", rs.getString(4));
				m.put("trafico", DAO.SIFormater(rs.getBigDecimal(5)));
				rpta.add(m);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rpta;
	}

	public static List<Map<String, Object>> getReporteTraficoLag(Connection conn, Map<String, Object> payload) {
			List<Map<String, Object>> rpta = new ArrayList<Map<String, Object>>();
			String sql = "";
			if (payload.get("fecha_ini") != null) {
				sql = "SELECT *\r\n" + 
						"FROM (\r\n" + 
						"	SELECT LG.\"ID_LAG\", EQ.\"TIPO\", UB.\"REGION\", UB.\"LOCALIDAD\", LG.\"LAG_GROUP\", EQ.\"ID_EQUIPO\",\r\n" + 
						"	CASE WHEN \"REDUNDANCIA\" IS NULL THEN '' ELSE \"REDUNDANCIA\" END\r\n" + 
						"    FROM dev_webtrafico.\"Lags\" LG\r\n" + 
						"	JOIN dev_webtrafico.\"Equipos\" EQ\r\n" + 
						"	ON LG.\"ID_EQUIPO\" = EQ.\"ID_EQUIPO\"\r\n" + 
						"	JOIN dev_webtrafico.\"Ubigeo\" UB\r\n" + 
						"	ON EQ.\"ID_EQUIPO\" = UB.\"ID_EQUIPO\"\r\n" + 
						") T1 JOIN (\r\n" + 
						"	SELECT \"ID_LAG\" AS iden, MIN(\"TRAFICO\")*8/300, AVG(\"TRAFICO\")*8/300, MAX(\"TRAFICO\")*8/300, STDDEV(\"TRAFICO\")*8/300,\r\n" + 
						"	MIN(\"TRAFICO_IN\")*8/300, AVG(\"TRAFICO_IN\")*8/300, MAX(\"TRAFICO_IN\")*8/300, STDDEV(\"TRAFICO_IN\")*8/300\r\n" + 
						"	FROM dev_webtrafico.\"DataLagsDiferenciada\"\r\n" + 
						"	WHERE \"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')\r\n" + 
						"	GROUP BY \"ID_LAG\"\r\n" + 
						") T2\r\n" + 
						"ON T1.\"ID_LAG\" = T2.iden\r\n" + 
						"JOIN (\r\n" + 
						"	select T1.\"ID_LAG\" as enla, case when T2.disponibilidad is null then 100 ELSE T2.disponibilidad END\r\n" + 
						"	from\r\n" + 
						"	(\r\n" + 
						"		SELECT \"ID_LAG\"\r\n" + 
						"		FROM dev_webtrafico.\"Lags\"\r\n" + 
						"	) T1 LEFT JOIN (\r\n" + 
						"		SELECT \"ID_LAG\" as enla,\r\n" + 
						"        ROUND((CAST (1 - (CAST (CAST (COUNT(1) AS FLOAT) / \r\n" + 
						"		(EXTRACT (EPOCH FROM (((to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS'))-(to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')))))/60)\r\n" + 
						"		AS FLOAT)) AS NUMERIC))*100,3) AS disponibilidad\r\n" + 
						"		FROM dev_webtrafico.\"DisponibilidadLag\"\r\n" + 
						"		WHERE \"DISPONIBILIDAD\"= 4 AND \r\n" + 
						"        \"FECHA\" >= to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS') AND \"FECHA\" < to_timestamp(?, 'YYYY-mm-dd HH24:MI:SS')\r\n" + 
						"		GROUP BY \"ID_LAG\"\r\n" + 
						"	) T2\r\n" + 
						"	ON T1.\"ID_LAG\" = T2.enla\r\n" + 
						") T3\r\n" + 
						"ON T1.\"ID_LAG\" = T3.enla\r\n" + 
						"ORDER BY \"ID_LAG\"";
			} else {
				sql = "SELECT *\r\n" + 
						"FROM (\r\n" + 
						"	SELECT LG.\"ID_LAG\", EQ.\"TIPO\", UB.\"REGION\", UB.\"LOCALIDAD\", LG.\"LAG_GROUP\", EQ.\"ID_EQUIPO\",\r\n" + 
						"	CASE WHEN \"REDUNDANCIA\" IS NULL THEN '' ELSE \"REDUNDANCIA\" END\r\n" + 
						"    FROM dev_webtrafico.\"Lags\" LG\r\n" + 
						"	JOIN dev_webtrafico.\"Equipos\" EQ\r\n" + 
						"	ON LG.\"ID_EQUIPO\" = EQ.\"ID_EQUIPO\"\r\n" + 
						"	JOIN dev_webtrafico.\"Ubigeo\" UB\r\n" + 
						"	ON EQ.\"ID_EQUIPO\" = UB.\"ID_EQUIPO\"\r\n" + 
						") T1 JOIN (\r\n" + 
						"	SELECT \"ID_LAG\" AS iden, MIN(\"TRAFICO\")*8/300, AVG(\"TRAFICO\")*8/300, MAX(\"TRAFICO\")*8/300, STDDEV(\"TRAFICO\")*8/300,\r\n" + 
						"	MIN(\"TRAFICO_IN\")*8/300, AVG(\"TRAFICO_IN\")*8/300, MAX(\"TRAFICO_IN\")*8/300, STDDEV(\"TRAFICO_IN\")*8/300\r\n" + 
						"	FROM dev_webtrafico.\"DataLagsDiferenciada\"\r\n" + 
						"	WHERE \"FECHA\" > current_timestamp - interval'7 days'\r\n" + 
						"	GROUP BY \"ID_LAG\"\r\n" + 
						") T2\r\n" + 
						"ON T1.\"ID_LAG\" = T2.iden\r\n" + 
						"JOIN (\r\n" + 
						"	select T1.\"ID_LAG\" as enla, case when T2.disponibilidad is null then 100 ELSE T2.disponibilidad END\r\n" + 
						"	from\r\n" + 
						"	(\r\n" + 
						"		SELECT \"ID_LAG\"\r\n" + 
						"		FROM dev_webtrafico.\"Lags\"\r\n" + 
						"	) T1 LEFT JOIN (\r\n" + 
						"		SELECT \"ID_LAG\" as enla, ROUND((CAST (1 - (CAST (CAST (COUNT(1) AS FLOAT) / CAST ((1*60*24*7) AS FLOAT) AS FLOAT)) AS NUMERIC))*100,3) AS disponibilidad\r\n" + 
						"		FROM dev_webtrafico.\"DisponibilidadLag\"\r\n" + 
						"		WHERE \"DISPONIBILIDAD\"= 4 AND \"FECHA\" > current_timestamp - interval '7 days'\r\n" + 
						"		GROUP BY \"ID_LAG\"\r\n" + 
						"	) T2\r\n" + 
						"	ON T1.\"ID_LAG\" = T2.enla\r\n" + 
						") T3\r\n" + 
						"ON T1.\"ID_LAG\" = T3.enla\r\n" + 
						"ORDER BY \"ID_LAG\"";
			}
			try {
				PreparedStatement ps = conn.prepareStatement(sql);
				if (payload.get("fecha_ini") != null) {
					ps.setString(1, String.valueOf((payload.get("fecha_ini"))));
					ps.setString(2, String.valueOf((payload.get("fecha_fin"))));
					ps.setString(3, String.valueOf((payload.get("fecha_fin"))));
					ps.setString(4, String.valueOf((payload.get("fecha_ini"))));
					ps.setString(5, String.valueOf((payload.get("fecha_ini"))));
					ps.setString(6, String.valueOf((payload.get("fecha_fin"))));
				}
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					Map<String, Object> m = new HashMap<String, Object>(); 
					m.put("id_lag", rs.getInt(1));
					m.put("tipo", rs.getString(2)+" "+rs.getString(7));
					m.put("region_org", rs.getString(3));
					m.put("localidad_org", rs.getString(4));
					m.put("lag_group", rs.getString(5));
					m.put("id_equipo", rs.getInt(6));
					m.put("min", SIFormater(rs.getBigDecimal(9)));
					m.put("raw_min", rs.getBigDecimal(9).divide(BigDecimal.valueOf(1000000), 3, BigDecimal.ROUND_HALF_UP));
					m.put("avg", SIFormater(rs.getBigDecimal(10)));
					m.put("raw_avg", rs.getBigDecimal(10).divide(BigDecimal.valueOf(1000000), 3, BigDecimal.ROUND_HALF_UP));
					m.put("max", SIFormater(rs.getBigDecimal(11)));
					m.put("raw_max", rs.getBigDecimal(11).divide(BigDecimal.valueOf(1000000), 3, BigDecimal.ROUND_HALF_UP));
					m.put("stddev", SIFormater(rs.getBigDecimal(12)));
					m.put("raw_stddev", rs.getBigDecimal(12).divide(BigDecimal.valueOf(1000000), 3, BigDecimal.ROUND_HALF_UP));
					
					m.put("min2", SIFormater(rs.getBigDecimal(13)));
					m.put("raw_min2", rs.getBigDecimal(13).divide(BigDecimal.valueOf(1000000), 3, BigDecimal.ROUND_HALF_UP));
					m.put("avg2", SIFormater(rs.getBigDecimal(14)));
					m.put("raw_avg2", rs.getBigDecimal(14).divide(BigDecimal.valueOf(1000000), 3, BigDecimal.ROUND_HALF_UP));
					m.put("max2", SIFormater(rs.getBigDecimal(15)));
					m.put("raw_max2", rs.getBigDecimal(15).divide(BigDecimal.valueOf(1000000), 3, BigDecimal.ROUND_HALF_UP));
					m.put("stddev2", SIFormater(rs.getBigDecimal(16)));
					m.put("raw_stddev2", rs.getBigDecimal(16).divide(BigDecimal.valueOf(1000000), 3, BigDecimal.ROUND_HALF_UP));
					m.put("dispo", rs.getString(18)+"%");
					m.put("raw_dispo", rs.getFloat(18));
					rpta.add(m);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			return rpta;
	}
}